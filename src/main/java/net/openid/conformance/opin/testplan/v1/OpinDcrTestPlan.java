package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.dcr.OpinDCRWithAllScopesNoUnregistrationTestModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Open Insurance Brasil Functional DCR Tests",
	profile = OBBProfile.OBB_PROFILE_OPIN_DCR,
	displayName = PlanNames.OPIN_DCR_TEST_PLAN,
	summary = "Open Insurance Brasil Functional DCR Tests"
)

public class OpinDcrTestPlan implements TestPlan{
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinDCRWithAllScopesNoUnregistrationTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openinsurance_brazil"),
					new Variant(FAPIResponseMode.class, "plain_response")
				)
			)
		);
	}
}
