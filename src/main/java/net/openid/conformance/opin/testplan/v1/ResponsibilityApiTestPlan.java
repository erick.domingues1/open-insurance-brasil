package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.opin.testmodule.v1.responsibility.OpinResponsibilityResourcesApiTestModule;
import net.openid.conformance.opin.testmodule.v1.responsibility.OpinResponsibilityWrongPermissionsTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIResponseMode;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance responsibility api test",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.RESPONSIBILITY_API_PHASE_2_TEST_PLAN,
	summary = "Structural and logical tests for OpenInsurance Brasil-conformant responsibility API"
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
		"vp_fapi_response_mode.plain_response",
		//"resource.brazilCpf",
		//"resource.brazilCnpj",
		//"client.org_jwks"
})

public class ResponsibilityApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinResponsibilityResourcesApiTestModule.class,
					OpinResponsibilityWrongPermissionsTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, "plain_response")
				)
			)
		);
	}
}
