package net.openid.conformance.opin.testplan.v1;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.opin.testmodule.v1.financialRisk.OpinFinancialRiskApiTestModule;
import net.openid.conformance.opin.testmodule.v1.financialRisk.OpinFinancialRiskResourcesApiTestModule;
import net.openid.conformance.opin.testmodule.v1.financialRisk.OpinFinancialRiskWrongPermissionsTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance financial risks api test",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.FINANCIAL_RISKS_API_PHASE_2_TEST_PLAN,
	summary = "Structural and logical tests for OpenInsurance Brasil-conformant financial risks API"
)

public class FinancialRisksApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinFinancialRiskApiTestModule.class,
					OpinFinancialRiskResourcesApiTestModule.class,
					OpinFinancialRiskWrongPermissionsTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, "plain_response")
				)
			)
		);
	}
}
