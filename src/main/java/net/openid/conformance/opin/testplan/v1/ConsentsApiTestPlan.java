package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.consents.*;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance consents api test",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.CONSENTS_API_TEST_PLAN,
	summary = "Functional tests for Open Insurance Brasil -  consents API"
)

public class ConsentsApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
						OpinPreFlightCheckModule.class,
						OpinConsentApiTestModule.class,
						OpinConsentsApiConsentStatusTestModule.class,
						OpinConsentApiNegativeTests.class,
						OpinConsentsApiPermissionGroupsTestModule.class,
						OpinConsentsApiCrossClientTestModule.class,
						OpinConsentsApiConsentStatusIfDeclinedTestModule.class,
						OpinConsentsApiConsentExpiredTestModule.class,
						OpinConsentsApiDeleteTestModule.class,
						OpinConsentInvalidUser.class

				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, "plain_response")
				)
			)
		);
	}
}
