package net.openid.conformance.opin.testplan.phase1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.phase1.privateGuarantee.PrivateGuaranteeApiTestModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.ClientAuthType;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Open Insurance - ProductsServices - Private Guarantee API test plan",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1,
	displayName = PlanNames.PRIVATE_GUARANTEE_API_TEST_PLAN,
	summary = "Structural and logical tests for Private Guarantee API"
)
public class PrivateGuaranteeApiTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(PrivateGuaranteeApiTestModule.class),
				List.of(new Variant(ClientAuthType.class, "none"))
			)
		);
	}

}
