package net.openid.conformance.opin.testplan.v1;


import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.opin.testmodule.v1.rural.OpinRuralApiTestModule;
import net.openid.conformance.opin.testmodule.v1.rural.OpinRuralResourcesApiTestModule;
import net.openid.conformance.opin.testmodule.v1.rural.OpinRuralWrongPermissionsTestModule;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
        testPlanName = "Insurance rural api test",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        displayName = PlanNames.RURAL_API_TEST_PLAN,
        summary = "Functional Tests for Rural API - based on Swagger Version 1.0.0 (WIP)"
)

public class RuralApiTestPlan implements TestPlan {
    public static List<ModuleListEntry> testModulesWithVariants() {
        return List.of(
                new ModuleListEntry(
                        List.of(
                                OpinPreFlightCheckModule.class,
                                OpinRuralApiTestModule.class,
                                OpinRuralResourcesApiTestModule.class,
                                OpinRuralWrongPermissionsTestModule.class
                        ),
                        List.of(
                                new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
                                new Variant(FAPIResponseMode.class, "plain_response")
                        )
                )
        );
    }
}
