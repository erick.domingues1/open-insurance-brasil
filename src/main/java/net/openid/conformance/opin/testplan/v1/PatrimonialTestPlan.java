package net.openid.conformance.opin.testplan.v1;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.v1.consents.OpinPreFlightCheckModule;
import net.openid.conformance.opin.testmodule.v1.patrimonial.*;
import net.openid.conformance.opin.testplan.utils.PlanNames;
import net.openid.conformance.plan.PublishTestPlan;
import net.openid.conformance.plan.TestPlan;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.FAPIResponseMode;

import java.util.List;

@PublishTestPlan(
	testPlanName = "Insurance patrimonial api test",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	displayName = PlanNames.PATRIMONIAL_API_TEST_PLAN,
	summary = "Functional tests for Open Insurance Brasil -  Patrimonial API"
)

public class PatrimonialTestPlan implements TestPlan {
	public static List<ModuleListEntry> testModulesWithVariants() {
		return List.of(
			new ModuleListEntry(
				List.of(
					OpinPreFlightCheckModule.class,
					OpinPatrimonialApiTestModule.class,
					OpinPatrimonialWrongPermissionsTestModule.class,
					OpinPatrimonialResourcesApiTestModule.class,
					OpinPatrimonialResidencialBranchTestModule.class,
					OpinPatrimonialCondominioBranchTestModule.class,
					OpinPatrimonialEmpresarialBranchTestModule.class,
					OpinPatrimonialRnRoBranchTestModule.class,
					OpinPatrimonialGlobalBancosBranchTestModule.class,
					OpinPatrimonialLucrosCessantesBranchTestModule.class,
					OpinPatrimonialRiscosDeEngenhariaBranchTestModule.class,
					OpinPatrimonialAssistenciaBranchTestModule.class,
					OpinPatrimonialRiscosDiversosBranchTestModule.class,
					OpinPatrimonialGarantiaEstendidaBranchTestModule.class
				),
				List.of(
					new Variant(FAPI1FinalOPProfile.class, "openbanking_brazil"),
					new Variant(FAPIResponseMode.class, "plain_response")
				)
			)
		);
	}
}
