package net.openid.conformance.opin.testplan.utils;

public class PlanNames {
	/** VERSION 1 **/
	/* Phase 1 - Open Data */
	public static final String PRODUCTS_N_SERVICES_PERSON_API_TEST_PLAN  = "Functional tests for ProductsNServices - Person API - based on Swagger version: 1.3.0"; // OPIN Person
	public static final String PERSON_PENSION_PLAN_API_TEST_PLAN = "Functional tests for ProductsNServices - Pension Plan API - based on Swagger version: 1.1.0"; // OPIN Pension Plan

	public static final String AUTO_INSURANCE_PLAN_API_TEST_PLAN = "Functional tests for ProductsNServices - Auto Insurance API - based on Swagger version: 1.3.0"; // OPIN Auto Insurance
	public static final String HOME_INSURANCE_PLAN_API_TEST_PLAN = "Functional tests for ProductsNServices - Home Insurance API - based on Swagger version: 1.2.0"; // OPIN Home Insurance
	public static final String CAPITALIZATION_TITLE_PLAN_API_TEST_PLAN = "Functional tests for ProductsNServices - Capitalization Title API - based on Swagger version: 1.1.2"; // OPIN Capitalization Title
	public static final String LIFE_PENSION_PLAN_API_TEST_PLAN = "Functional tests for ProductsNServices - Life Pension API - based on Swagger version: 1.3.0"; // OPIN Life Pension
	public static final String OPIN_CHANNELS_BRANCHES_API_TEST_PLAN = "Functional tests for Channels - Branches API - based on Swagger version: 1.2.0"; // OPIN Channel Branches
	public static final String OPIN_ELECTRONIC_CHANNELS_API_TEST_PLAN = "Functional tests for Channels - Electronic Channels API - based on Swagger version: 1.2.0"; // OPIN Electronic Channel
	public static final String OPIN_PHONE_CHANNELS_API_TEST_PLAN = "Functional tests for Channels - Phone Channels API - based on Swagger version: 1.0.2"; // OPIN Phone Channels
	public static final String OPIN_ADMIN_API_TEST_PLAN = "Functional tests for Admin API - based on Swagger version: 1.3.0"; // OPIN Admin
	public static final String OPIN_DISCOVERY_STATUS_TEST_PLAN = "Functional tests for Discovery Status API - based on Swagger version: 1.1.0"; // OPIN Discovery
	public static final String OPIN_DISCOVERY_OUTAGES_TEST_PLAN = "Functional tests for Discovery  Outages API - based on Swagger version: 1.1.0"; // OPIN Discovery
	public static final String BUSINESS_API_TEST_PLAN = "Functional tests for ProductsServices - Business API - based on Swagger version: 1.1.0";
	public static final String AUTO_EXTENDED_WARRANTY_API_TEST_PLAN = "Functional tests for ProductsServices - Auto Extended Warranty API - based on Swagger version: 1.1.0";
	public static final String ASSISTANCE_GENERAL_ASSETS_API_TEST_PLAN = "Functional tests for ProductsServices - Assistance General Assets API - based on Swagger version: 1.1.0";
	public static final String GENERAL_LIABILITY_API_TEST_PLAN = "Functional tests for ProductsServices - General Liability API - based on Swagger version: 1.1.0";
	public static final String EQUIPMENT_BREAKDOWN_API_TEST_PLAN = "Functional tests for ProductsServices - Equipment Breakdown API - based on Swagger version: 1.1.0";
	public static final String LOST_PROFIT_API_TEST_PLAN = "Functional tests for ProductsServices - Lost Profit API - based on Swagger version: 1.1.0";
	public static final String ENGINEERING_API_TEST_PLAN = "Functional tests for ProductsServices - Engineering API - based on Swagger version: 1.1.0";
	public static final String PRIVATE_GUARANTEE_API_TEST_PLAN = "Functional tests for ProductsServices - Private Guarantee API - based on Swagger version: 1.1.0";
	public static final String DOMESTIC_CREDIT_API_TEST_PLAN = "Functional tests for ProductsServices - Domestic Credit API - based on Swagger version: 1.1.0";
	public static final String EXTENDED_WARRANTY_API_TEST_PLAN = "Functional tests for ProductsServices - Extended Warranty API - based on Swagger version: 1.1.0";
	public static final String CYBER_RISK_API_TEST_PLAN = "Functional tests for ProductsServices - Cyber Risk Plan API - based on Swagger version: 1.1.0";
	public static final String EXPORT_CREDIT_API_TEST_PLAN = "Functional tests for ProductsServices - Export Credit API - based on Swagger version: 1.1.0";
	public static final String PUBLIC_GUARANTEE_API_TEST_PLAN = "Functional tests for ProductsServices - Public Guarantee API - based on Swagger version: 1.1.0";
	public static final String NAMED_OPERATIONAL_RISKS_API_TEST_PLAN = "Functional tests for ProductsServices - Named Operational Risks API - based on Swagger version: 1.1.0";
	public static final String FINANCIAL_RISK_API_TEST_PLAN = "Functional tests for ProductsServices - Financial Risk API - based on Swagger version: 1.1.0";
	public static final String GLOBAL_BANKING_API_TEST_PLAN = "Functional tests for ProductsServices - Global Banking API - based on Swagger version: 1.1.0";
	public static final String STOP_LOSS_API_TEST_PLAN = "Functional tests for ProductsServices - Stop Loss API - based on Swagger version: 1.1.0";
	public static final String RENT_GUARANTEE_API_TEST_PLAN = "Functional tests for ProductsServices - Rent Guarantee API - based on Swagger version: 1.1.0";
	public static final String ENVIRONMENTAL_LIABILITY_API_TEST_PLAN = "Functional tests for ProductsServices - Environmental Liability API - based on Swagger version: 1.1.0";
	public static final String CONDOMINIUM_API_TEST_PLAN =  "Functional tests for ProductsServices - Condominium API - based on Swagger version: 1.1.0";
	public static final String ERRORS_OMISSIONS_LIABILITY_API_TEST_PLAN = "Functional tests for ProductsServices - Errors Omissions Liability API - based on Swagger version: 1.1.0";
	public static final String DIRECTORS_OFFICERS_LIABILITY_API_TEST_PLAN = "Functional tests for ProductsServices - Directors Officers Liability API - based on Swagger version: 1.1.0";
	public static final String OTHERS_SCOPES_API_TEST_PLAN = "Functional tests for ProductsServices - Others Scopes API - based on Swagger version: 1.0.0";

	public static final String INTERMEDIARY_API_TEST_PLAN = "Functional tests for Channels - Intermediary API - based on Swagger version: 1.2.0";
	public static final String REFERENCED_NETWORK_API_TEST_PLAN = "Functional tests for Channels - Referenced Network API - based on Swagger version: 1.2.0";


	/* Phase 2 - Customer Data */
	public static final String CONSENTS_API_TEST_PLAN = "Functional Tests for Consents API - based on Swagger Version 1.1.0";
	public static final String RESOURCES_API_TEST_PLAN = "Functional Tests for Resources API - based on Swagger Version 1.2.0";
	public static final String CUSTOMERS_BUSINESS_API_TEST_PLAN = "Functional Tests for Customer Business API - based on swagger Version 1.2.0";
	public static final String CUSTOMERS_PERSONAL_API_TEST_PLAN = "Functional Tests for Customer Personal API - based on swagger Version 1.2.0";
	public static final String PATRIMONIAL_API_TEST_PLAN = "Functional Tests for Patrimonial API - based on swagger Version 1.3.0";

	public static final String RESPONSIBILITY_API_PHASE_2_TEST_PLAN = "Functional Tests for Responsibility API - based on Swagger Version 1.2.0 (WIP)";
	public static final String FINANCIAL_RISKS_API_PHASE_2_TEST_PLAN = "Functional Tests for Financial Risks API - based on Swagger Version 1.2.0 (WIP)";
	public static final String ACCEPTANCE_AND_BRANCHES_ABROAD_PHASE_2_TEST_PLAN = "Functional Tests for Acceptance and Branches Abroad API - based on Swagger Version 1.2.0 (WIP)";

	public static final String RURAL_API_TEST_PLAN = "Functional tests for Rural API - based on Swagger version: 1.2.0 (WIP)";
	public static final String AUTO_API_TEST_PLAN = "Functional Tests for Auto API - based on swagger Version 1.2.0 (WIP)";
	public static final String TRANSPORT_API_TEST_PLAN = " Functional Tests for Transport API - based on Swagger Version 1.1.0 (WIP)";

	public static final String HOUSING_API_TEST_PLAN = "Functional tests for Housing API - based on Swagger version: 1.2.0 (WIP)";

	public static final String STRUCTURAL_TEST_PLAN = "Open Insurance Brasil Structural Tests for Phase 2 - Version 1";
	public static final String OPIN_DCR_TEST_PLAN = "Opin Brazil DCR Test";
	/* Phase 3 - Payment Initiation */


}
