package net.openid.conformance.opin.testmodule.support;

import com.google.common.base.Strings;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.Optional;

public class OpinInsertMtlsCa extends AbstractCondition {

	private static String MTLS_ROOT = "http://crl.pki.opinbrasil.com.br/root-ca.pem";
	private static String MTLS_ISSUER = "http://crl.pki.opinbrasil.com.br/issuer-ca.pem";

	@Override
	public Environment evaluate(Environment env) {

		String mtlsCa;
		try {
			RestTemplate restTemplate = createRestTemplate(env);
			ResponseEntity<String> response = restTemplate.exchange(MTLS_ROOT, HttpMethod.GET, null, String.class);
			String rootMtls = response.getBody();
			if (Strings.isNullOrEmpty(rootMtls)) {
				throw  error(String.format("MTLS_ROOT returned by the endpoint %s was null", MTLS_ROOT));
			}

			restTemplate = createRestTemplate(env);
			response = restTemplate.exchange(MTLS_ISSUER, HttpMethod.GET, null, String.class);
			String issuerMtls = response.getBody();
			if (Strings.isNullOrEmpty(issuerMtls)) {
				throw  error(String.format("MTLS_ISSUER returned by the endpoint %s was null", MTLS_ISSUER));
			}

			mtlsCa = issuerMtls + rootMtls;
		} catch (UnrecoverableKeyException | KeyManagementException | CertificateException |
				 InvalidKeySpecException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
			throw error("Error creating HTTP client", e);
		} catch (RestClientException e) {
			String msg = "Unable to fetch server configuration";
			if (e.getCause() != null) {
				msg += " - " +e.getCause().getMessage();
			}
			throw error(msg, e);
		}

		if (Strings.isNullOrEmpty(mtlsCa)) {
			throw error("Verify the test behavior, mtls.ca returned was null.");
		}

		env.putString("config", "mtls.ca", mtlsCa);

		Optional<String> mtls2Cert = Optional.ofNullable(env.getString("config", "mtls2.cert"));
		if (mtls2Cert.isPresent()) {
			log("Second client found, copying mtls.ca to mtls2.ca");
			env.putString("config", "mtls2.ca", mtlsCa);
		}

		logSuccess("the mtls.ca was correctly configured", args("mtlsCA", mtlsCa));

		return env;
	}
}
