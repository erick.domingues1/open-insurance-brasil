package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.validator.insuranceHousing.v1.*;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
    testName = "opin-housing-api-structural-test",
    displayName = "Validate structure of Housing API Endpoint 200 response",
    summary = "Validate structure of Housing API Endpoint 200 response \n"+
            "• Call the “/\" endpoint - Expect 200 and validate response\n" +
            "• Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
            "• Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
            "• Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
    profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
    configurationFields = {
            "server.discoveryUrl",
            "resource.resourceUrl",
})

public class OpinHousingStructuralTestModule extends AbstractOpinDataStructuralTestModule {
    private static final String API = "insurance-housing";

    @Override
    public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {
        setApi(API);
        setRootValidator(OpinInsuranceHousingListValidatorV1.class);
        setPolicyInfoValidator(OpinInsuranceHousingPolicyInfoValidatorV1.class);
        setPremiumValidator(OpinInsuranceHousingPremiumValidatorV1.class);
        setClaimValidator(OpinInsuranceHousingClaimValidatorV1.class);

        super.configure(config, baseUrl, externalUrlOverride);
    }
}
