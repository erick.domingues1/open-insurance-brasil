package net.openid.conformance.opin.testmodule;


import net.openid.conformance.openbanking_brasil.testmodules.AbstractOBBrasilFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.OpinInsertMtlsCa;
import net.openid.conformance.opin.testmodule.support.OpinPreAuthorizationConsentApi;
import net.openid.conformance.sequence.ConditionSequence;

public abstract  class AbstractOpinFunctionalTestModule extends AbstractOBBrasilFunctionalTestModule {

	@Override
	protected void configureClient() {
		callAndStopOnFailure(OpinInsertMtlsCa.class);
		super.configureClient();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		OpinPreAuthorizationConsentApi steps = new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication);
		return steps;
	}

}
