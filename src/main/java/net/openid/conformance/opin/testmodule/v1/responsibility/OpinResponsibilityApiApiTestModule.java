package net.openid.conformance.opin.testmodule.v1.responsibility;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModule;
import net.openid.conformance.opin.testmodule.support.AddResponsibilityScope;
import net.openid.conformance.opin.testmodule.support.BuildResponsibilityConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.validator.insuranceResponsibility.v1.OpinInsuranceResponsibilityClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceResponsibility.v1.OpinInsuranceResponsibilityListValidatorV1;
import net.openid.conformance.opin.validator.insuranceResponsibility.v1.OpinInsuranceResponsibilityPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceResponsibility.v1.OpinInsuranceResponsibilityPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-responsibility-api-test",
	displayName = "Validates the structure of all responsibility API resources",
	summary = "Validates the structure of all responsibility API resources\n" +
		"\u2022 Creates a consent with all the permissions needed to access the responsibility API (“DAMAGES_AND_PEOPLE_RESPONSIBILITY_READ”, “DAMAGES_AND_PEOPLE_RESPONSIBILITY_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_RESPONSIBILITY_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_RESPONSIBILITY_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET responsibility “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
		"\u2022 Calls GET responsibility policy-Info API \n" +
		"\u2022 Expects 200 - Validate all the fields\n" +
		"\u2022 Calls GET responsibility premium API \n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET responsibility claim API \n" +
		"\u2022 Expects 200- Validate all the fields",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"mtls.ca",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinResponsibilityApiApiTestModule extends AbstractOpinApiTestModule {

	private static final String API = "insurance-responsibility";
	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddResponsibilityScope.class);
		callAndStopOnFailure(BuildResponsibilityConfigResourceUrlFromConsentUrl.class);
		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_RESPONSIBILITY).build();

		setApi(API);
		setRootValidator(OpinInsuranceResponsibilityListValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceResponsibilityPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceResponsibilityPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceResponsibilityClaimValidatorV1.class);
	}
}
