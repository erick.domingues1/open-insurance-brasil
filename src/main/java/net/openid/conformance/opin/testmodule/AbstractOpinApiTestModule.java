package net.openid.conformance.opin.testmodule;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseHasLinks;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateResponseMetaData;
import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateSelfEndpoint;
import net.openid.conformance.opin.testmodule.support.*;

public abstract class AbstractOpinApiTestModule extends AbstractOpinFunctionalTestModule {
	protected String api;

	protected Class<? extends Condition> rootValidator;
	protected Class<? extends Condition> policyInfoValidator;
	protected Class<? extends Condition> premiumValidator;
	protected Class<? extends Condition> claimValidator;

	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(rootValidator, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(PolicyIDSelector.class);

		validate(PrepareUrlForFetchingPolicyInfo.class, policyInfoValidator, String.format("Fetch %s policy info", api));
		validate(PrepareUrlForFetchingPremium.class, premiumValidator, String.format("Fetch %s premium", api));
		validate(PrepareUrlForFetchingClaim.class, claimValidator, String.format("Fetch %s claim", api));

		callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(OpinValidateMetaOnlyPagesRecords.class, Condition.ConditionResult.FAILURE);

		call(new ValidateSelfEndpoint().replace(ValidateResponseMetaData.class, condition(OpinValidateMetaOnlyPagesRecords.class)));

	}

	protected void validate(Class<? extends Condition> prepareUrlCondition, Class<? extends Condition> validator, String logMsg) {
		callAndStopOnFailure(prepareUrlCondition);
		preCallProtectedResource(logMsg);
		callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
	}

	protected void setApi(String api) {
		this.api = api;
		env.putString("api", api);
	}

	public void setRootValidator(Class<? extends Condition> rootValidator) {
		this.rootValidator = rootValidator;
	}

	public void setPolicyInfoValidator(Class<? extends Condition> policyInfoValidator) {
		this.policyInfoValidator = policyInfoValidator;
	}

	public void setPremiumValidator(Class<? extends Condition> premiumValidator) {
		this.premiumValidator = premiumValidator;
	}

	public void setClaimValidator(Class<? extends Condition> claimValidator) {
		this.claimValidator = claimValidator;
	}



}
