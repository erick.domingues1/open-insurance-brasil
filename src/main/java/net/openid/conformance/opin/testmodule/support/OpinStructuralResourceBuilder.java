package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.testmodule.Environment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class OpinStructuralResourceBuilder extends AbstractCondition {

	protected String api;
	@Override
	public Environment evaluate(Environment env) {

		String url = env.getString("config", "resource.resourceUrl");

		Pattern p = Pattern.compile("v[0-9]");
		Matcher m = p.matcher(url);
		String apiVersion;
		if(!m.find()) {
			throw error("Information about the API version not found in the resourceUrl", args("resourceUrl", url));
		}
		apiVersion = m.group(0);

		String regex = "^(https://)(.*?)(/open-insurance)";
		p = Pattern.compile(regex);
		m = p.matcher(url);
		String baseUrl;
		if(!m.find()) {
			throw error(String.format("Base URL provided does not follow regex %s ", regex), args("resourceUrl", url));
		}
		baseUrl = m.group(0);

		String protectedUrl = String.format("%s/%s/%s/", baseUrl, api, apiVersion);

		env.putString("protected_resource_url", protectedUrl);
		env.putString("config", "resource.resourceUrl", protectedUrl);

		logSuccess(String.format("protected_resource_url and resource.resourceUrl set to %s", protectedUrl), args("endpoint", protectedUrl));
		return env;
	}

	public void setApi(String api) {
		this.api = api;
	}
}
