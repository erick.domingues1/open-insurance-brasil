package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;

import java.util.Map;

public class CompareResourceIdWithPolicyId extends AbstractCondition {

	@Override
	@PreEnvironment(required = {"extracted_resource_id"})
	public Environment evaluate(Environment env) {

		String policiesString = env.getString("all_policies");

		JsonArray extractedPolicyIds = JsonParser.parseString(policiesString).getAsJsonArray();
		JsonArray extractedResourcesIds = env.getElementFromObject("extracted_resource_id", "extractedResourceIds").getAsJsonArray();

		if (extractedPolicyIds.isEmpty()) {
			throw error("Extracted API IDs array is empty");
		}

		if (extractedResourcesIds.isEmpty()) {
			throw error("Extracted Resources IDs array is empty");
		}

		if (extractedPolicyIds.size() == extractedResourcesIds.size()) {
			log("Sizes are equal", Map.of("PolicyIdsSize", extractedPolicyIds.size(), "ResourceIdsSize", extractedResourcesIds.size()));
			for (JsonElement extractedResourcesId : extractedResourcesIds) {
				if (!extractedPolicyIds.contains(extractedResourcesId)) {
					throw error("API resources do not have a resource fetched from the resource endpoint response",
						Map.of("Missing ID", extractedResourcesId,
							"extractedPolicyIds", extractedPolicyIds,
							"extractedResourceIds", extractedResourcesIds)
					);
				}
			}

			for (JsonElement extractedPolicyId : extractedPolicyIds) {
				if (!extractedResourcesIds.contains(extractedPolicyId)) {
					throw error("Resource endpoint response resources do not have a resource fetched from the API",
						Map.of("Missing ID", extractedPolicyId,
							"extractedPolicyIds", extractedPolicyIds,
							"extractedResourceIds", extractedResourcesIds)
					);
				}
			}
		} else {
			throw error("Sizes of two resource lists are not equal", Map.of(
				"PolicyIdsSize", extractedPolicyIds.size(),
				"ResourceIdsSize", extractedResourcesIds.size(),
				"extractedPolicyIds", extractedPolicyIds,
				"extractedResourceIds", extractedResourcesIds)
			);
		}

		logSuccess("resourceId values are identical");
		return env;
	}
}
