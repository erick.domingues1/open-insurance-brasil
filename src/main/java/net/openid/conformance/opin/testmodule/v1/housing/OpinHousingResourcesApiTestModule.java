package net.openid.conformance.opin.testmodule.v1.housing;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.opin.testmodule.AbstractOpinDataResourceTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.validator.insuranceHousing.v1.OpinInsuranceHousingListValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-housing-resources-api-test",
	displayName = "Makes sure that the Resource API and the Housing API are returning the same available IDs",
	summary = "Makes sure that the Resource API and the Housing API are returning the same available IDs\n" +
			"• Creates a consent with all the permissions needed to access the housing API (“DAMAGES_AND_PEOPLE_HOUSING_READ”, “DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ”,  “RESOURCES_READ”)\n" +
			"• Expects the server to create the consent with 201\n" +
			"• Redirect the user to authorize at the financial institution\n" +
			"• Call the Housing “/” API\n" +
			"• Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the policy id provided by this API\n" +
			"• Call the resources API\n" +
			"• Expect a success - Validate the fields of the response that are marked as AVAILABLE are exactly the ones that have been returned by the tested API",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
public class OpinHousingResourcesApiTestModule extends AbstractOpinDataResourceTestModule {

	private static final String RESOURCE_TYPE = EnumOpinResourcesType.DAMAGES_AND_PEOPLE_HOUSING.name();
	private static final String RESOURCE_STATUS = EnumResourcesStatus.AVAILABLE.name();

	@Override
	protected void configureClient() {
		callAndStopOnFailure(BuildHousingConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {

		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env, getId(), eventLog, testInfo, executionManager);
		permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_HOUSING);
		permissionsBuilder.build();

		callAndStopOnFailure(AddHousingScope.class);
		setResourcesType(RESOURCE_TYPE);
		setResourceStatus(RESOURCE_STATUS);
		setRootValidator(OpinInsuranceHousingListValidatorV1.class);

		super.onConfigure(config, baseUrl);
	}
}
