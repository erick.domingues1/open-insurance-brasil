package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskClaimValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskListValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-financial-risks-api-structural-test",
	displayName = "Validate structure of Financial Risks API Endpoints 200 response",
	summary = "Validate structure of Financial Risks API Endpoints 200 response \n"+
	"\u2022 Call the “/\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
			"server.discoveryUrl",
			"resource.resourceUrl"
			})

public class OpinFinancialRisksStructuralTestModule extends AbstractOpinDataStructuralTestModule {

	private final String API = "insurance-financial-risk";

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {

		setApi(API);
		setRootValidator(OpinInsuranceFinancialRiskListValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceFinancialRiskPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceFinancialRiskPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceFinancialRiskClaimValidatorV1.class);

		super.configure(config, baseUrl, externalUrlOverride);
	}

}

