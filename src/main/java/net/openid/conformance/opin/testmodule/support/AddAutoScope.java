package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.AbstractScopeAddingCondition;

public class AddAutoScope extends AbstractScopeAddingCondition {

    protected String newScope() {
        return "insurance-auto";
    }
}
