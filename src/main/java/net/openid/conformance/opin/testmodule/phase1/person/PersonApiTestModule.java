package net.openid.conformance.opin.testmodule.phase1.person;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.validator.productsNServices.GetPersonValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Open Insurance - ProductsNServices - Person API test",
	displayName = "Validate structure of ProductsNServices - Person API Api resources",
	summary = "Validate structure of ProductsNServices - Person Api resources",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1)
public class PersonApiTestModule extends AbstractNoAuthFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validate ProductsNServices - Person response", () -> {
			callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class);
			callAndContinueOnFailure(GetPersonValidator.class, Condition.ConditionResult.FAILURE);
		});
	}
}
