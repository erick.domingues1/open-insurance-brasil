package net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModule;
import net.openid.conformance.opin.testmodule.support.AddAcceptanceBranchesAbroadScope;
import net.openid.conformance.opin.testmodule.support.BuildAcceptanceAndBranchesAbroadConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadListValidatorV1;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-acceptance-and-branches-abroad-api-test",
	displayName = "Validates the structure of all financial Acceptance and Branches Abroad API resources",
	summary = "Validates the structure of all financial Acceptance and Branches Abroad API resources\n" +
		"\u2022 (“DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad policy-Info API \n" +
		"\u2022 Expects 200 - Validate all the fields\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad premium API \n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad claim API \n" +
		"\u2022 Expects 200- Validate all the fields",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinAcceptanceAndBranchesAbroadApiTestModule extends AbstractOpinApiTestModule {

	private static final String API = "insurance-acceptance-and-branches-abroad";

	@Override
	protected void configureClient() {
		super.configureClient();
		callAndStopOnFailure(AddAcceptanceBranchesAbroadScope.class);
		callAndStopOnFailure(BuildAcceptanceAndBranchesAbroadConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD).build();

		setApi(API);
		setRootValidator(OpinInsuranceAcceptanceAndBranchesAbroadListValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceAcceptanceAndBranchesAbroadPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceAcceptanceAndBranchesAbroadPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceAcceptanceAndBranchesAbroadClaimValidatorV1.class);
	}
}
