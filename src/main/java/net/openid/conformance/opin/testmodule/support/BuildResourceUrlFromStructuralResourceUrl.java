package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.opin.testmodule.support.OpinStructuralResourceBuilder;
import net.openid.conformance.testmodule.Environment;



public class BuildResourceUrlFromStructuralResourceUrl extends OpinStructuralResourceBuilder {

	@Override
	@PreEnvironment(strings = "api_base")
	public Environment evaluate(Environment env) {

		setApi(env.getString("api_base"));

		return super.evaluate(env);
	}

}
