package net.openid.conformance.opin.testmodule.v1.structural;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;

public abstract class AbstractOpinDataStructuralTestModule extends AbstractNoAuthFunctionalTestModule{

	protected String api;

	protected Class<? extends Condition> rootValidator;
	protected Class<? extends Condition> policyInfoValidator;
	protected Class<? extends Condition> premiumValidator;
	protected Class<? extends Condition> claimValidator;

	@Override
	protected void runTests() {

		call(new ValidateOpinWellKnownUriSteps());

		env.putString("api_base", api);
		callAndStopOnFailure(BuildResourceUrlFromStructuralResourceUrl.class);

		runInBlock(String.format("Validate %s - Root", api), () -> {
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class);
			callAndContinueOnFailure(rootValidator, Condition.ConditionResult.FAILURE);
		});

		callAndContinueOnFailure(PolicyIDSelector.class);

		runInBlock(String.format("Validate %s - Policy-info", api), () -> {
			validate(PrepareUrlForFetchingPolicyInfo.class, policyInfoValidator);
		});

		runInBlock(String.format("Validate %s - Premium", api), () -> {
			validate(PrepareUrlForFetchingPremium.class, premiumValidator);
		});

		runInBlock(String.format("Validate %s - Claim", api), () -> {
			validate(PrepareUrlForFetchingClaim.class, claimValidator);
		});
	}

	protected void validate(Class<? extends Condition> prepareUrlCondition, Class<? extends Condition> validator) {

		callAndStopOnFailure(prepareUrlCondition);
		callAndStopOnFailure(CallNoCacheResource.class);
		callAndContinueOnFailure(DoNotStopOnFailure.class);
		callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
	}

	protected void setApi(String api) {
		this.api = api;
		env.putString("api", api);
	}

	public void setRootValidator(Class<? extends Condition> rootValidator) {
		this.rootValidator = rootValidator;
	}

	public void setPolicyInfoValidator(Class<? extends Condition> policyInfoValidator) {
		this.policyInfoValidator = policyInfoValidator;
	}

	public void setPremiumValidator(Class<? extends Condition> premiumValidator) {
		this.premiumValidator = premiumValidator;
	}

	public void setClaimValidator(Class<? extends Condition> claimValidator) {
		this.claimValidator = claimValidator;
	}
}

