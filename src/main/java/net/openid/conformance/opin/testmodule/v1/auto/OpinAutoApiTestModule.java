package net.openid.conformance.opin.testmodule.v1.auto;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModule;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoListValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoPremiumValidatorV1;
import net.openid.conformance.opin.testmodule.support.AddAutoScope;
import net.openid.conformance.opin.testmodule.support.BuildAutoConfigResourceUrlFromConsentUrl;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-auto-api-test",
        displayName = "Validates the structure of all auto API resources",
        summary ="Validates the structure of all auto API resources\n" +
                "\u2022 Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_AUTO_READ”, “DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API\n" +
                "\u2022 Calls GET auto “/” API \n" +
                "\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
                "\u2022 Calls GET auto policy-Info API \n" +
                "\u2022 Expects 200 - Validate all the fields \n" +
                "\u2022 Calls GET auto premium API \n" +
                "\u2022 Expects 200- Validate all the fields \n" +
                "\u2022 Calls GET auto claim API \n" +
                "\u2022 Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }

)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})

public class OpinAutoApiTestModule extends AbstractOpinApiTestModule {

    private static final String API = "insurance-auto";

    @Override
    protected void configureClient() {
        super.configureClient();
        callAndStopOnFailure(AddAutoScope.class);
        callAndStopOnFailure(BuildAutoConfigResourceUrlFromConsentUrl.class);
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
        permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_AUTO).build();

        setApi(API);
        setRootValidator(OpinInsuranceAutoListValidatorV1.class);
        setPolicyInfoValidator(OpinInsuranceAutoPolicyInfoValidatorV1.class);
        setPremiumValidator(OpinInsuranceAutoPremiumValidatorV1.class);
        setClaimValidator(OpinInsuranceAutoClaimValidatorV1.class);
    }
}
