package net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.resourcesAPI.EnumResourcesStatus;
import net.openid.conformance.opin.testmodule.AbstractOpinDataResourceTestModule;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadListValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-acceptance-and-branches-abroad-resources-api-test",
	displayName = "Makes sure that the Resource API and the acceptance and branches abroad API are returning the same available IDs",
	summary = "Makes sure that the Resource API and the cceptance and branches abroad API are returning the same available IDs\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Acceptance and Branches Abroad API (“DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects server to create the consent with 201\n" +
		"\u2022 Redirect the user to authorize at the financial institution\n" +
		"\u2022 Call the acceptance and branches abroad“/” API\n" +
		"\u2022 Expect a success - Validate the fields of the response and Make sure that a policy id is returned - Fetch the policy id provided by this API\n" +
		"\u2022 Call the resources API \n" +
		"\u2022 Expect a success - Validate the fields of the response and Make sure that an id is returned - Fetch the policy id provided by this API",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
public class OpinAcceptanceBranchesAbroadResourcesApiTestModule extends AbstractOpinDataResourceTestModule {

	private static final String RESOURCE_TYPE = EnumOpinResourcesType.DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD.name();
	private static final String RESOURCE_STATUS = EnumResourcesStatus.AVAILABLE.name();
	private OpinConsentPermissionsBuilder permissionsBuilder;

	@Override
	protected void configureClient() {
		callAndStopOnFailure(BuildAcceptanceAndBranchesAbroadConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {

		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD);
		permissionsBuilder.build();

		callAndStopOnFailure(AddAcceptanceBranchesAbroadScope.class);
		setResourcesType(RESOURCE_TYPE);
		setResourceStatus(RESOURCE_STATUS);
		setRootValidator(OpinInsuranceAcceptanceAndBranchesAbroadListValidatorV1.class);

		super.onConfigure(config, baseUrl);
	}
}
