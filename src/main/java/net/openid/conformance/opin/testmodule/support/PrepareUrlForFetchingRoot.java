package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingRoot extends ResourceBuilder {
	@Override
	@PreEnvironment(strings = {"api"})
	public Environment evaluate(Environment env) {

		String api = env.getString("api");
		setApi(api);
		setEndpoint("/"+api);

		return super.evaluate(env);
	}
}
