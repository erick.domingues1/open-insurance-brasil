package net.openid.conformance.opin.testmodule.phase1.intermediary;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.validator.channels.GetIntermediaryValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Open Insurance - Channels - Intermediary API test",
	displayName = "Validate structure of Channels - Intermediary response",
	summary = "Validate structure of Channels - Intermediary response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class IntermediaryTestModule extends AbstractNoAuthFunctionalTestModule {
	@Override
	protected void runTests() {
		runInBlock("Validate Channels - Intermediary response", () -> {
			callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class);
			callAndContinueOnFailure(GetIntermediaryValidator.class, Condition.ConditionResult.FAILURE);
		});
	}
}
