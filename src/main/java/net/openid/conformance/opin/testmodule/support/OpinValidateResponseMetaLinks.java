package net.openid.conformance.opin.testmodule.support;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators.LinksValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators.MetaValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;

import java.text.ParseException;
import java.util.Objects;

public class OpinValidateResponseMetaLinks extends AbstractJsonAssertingCondition {
	private final MetaValidator metaValidator;
	private final LinksValidator linksValidator;
	private static final String RESPONSE_ENV_KEY = "consent_endpoint_response_full";

	public OpinValidateResponseMetaLinks() {
		metaValidator = new MetaValidator(this);
		linksValidator = new LinksValidator(this);
	}

	@Override
	@PreEnvironment(required = RESPONSE_ENV_KEY)
	public Environment evaluate(Environment env) {
		JsonElement body = bodyFrom(env);
		String requestUri = env.getString("config", "resource.consentUrl");
		if (Strings.isNullOrEmpty(requestUri)) {
			throw error("consent url missing from configuration");
		}
		int expectedVersion = 1;
		metaValidator.assertMetaObject(body);
		linksValidator.assertLinksObject(body, requestUri, expectedVersion);
		String errorMessage = metaValidator.getErrorMessage();
		if (!Objects.equals(errorMessage, "")) {
			throw error(errorMessage, metaValidator.getArgs());
		}
		return env;
	}

	@Override
	protected JsonElement bodyFrom(Environment env) {
		try {
			return BodyExtractor.bodyFrom(env, RESPONSE_ENV_KEY)
					.orElseThrow(() -> error("Could not extract body from response"));
		} catch (ParseException e) {
			throw error("Could not parse the body");
		}
	}

}
