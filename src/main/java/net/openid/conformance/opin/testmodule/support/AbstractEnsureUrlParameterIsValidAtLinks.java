package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.paymentsv2.BodyExtractor;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.ParseException;

public abstract class AbstractEnsureUrlParameterIsValidAtLinks extends AbstractCondition {


    protected abstract String getUrlParameter();
    protected abstract boolean urlParameterIsValid(String urlParameter);

    public static final String RESPONSE_ENV_KEY = "resource_endpoint_response_full";

    protected String getUrlParameterFrom(String resourceUrl, String link) {

        if (!resourceUrl.contains(getUrlParameter()+"=")) {
            log(String.format("URL Parameter \"%s\" not found at %s link", getUrlParameter(), link));
            return null;
        }
        return  UriComponentsBuilder.fromUriString(resourceUrl).build().getQueryParams().get(getUrlParameter()).get(0);
    }

    protected JsonElement bodyFrom(Environment environment) {
        try {
            return BodyExtractor.bodyFrom(environment, RESPONSE_ENV_KEY)
                    .orElseThrow(() -> error("Could not extract body from response"));
        } catch (ParseException e) {
            throw error("Error parsing JWT response");
        }
    }

    @Override
    @PreEnvironment(required = RESPONSE_ENV_KEY)
    public Environment evaluate(Environment env) {
        JsonObject body = bodyFrom(env).getAsJsonObject();

        if (body.get("links") == null) {
            log("The object \"link\" was not found on the response", args("links", body.get("links")));
            return env;
        }

        JsonObject links = body.get("links").getAsJsonObject();

        for (String key : links.keySet()) {
            String linkUrl = links.get(key).getAsString();
            String urlParameter = getUrlParameterFrom(linkUrl, key);

            if (urlParameterIsValid(urlParameter)) {
                this.logSuccess(String.format("URL Parameter \"%s\" is valid", getUrlParameter()));
            } else {
                throw error("URL Parameter is not valid", args(getUrlParameter(),urlParameter ));
            }
        }
        return env;
    }
}
