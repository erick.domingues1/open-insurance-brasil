package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import java.util.Optional;
import com.google.gson.JsonObject;

public class DeleteCnpjFromConfig extends AbstractCondition {
    @Override
    @PreEnvironment(required = "config")
    public Environment evaluate(Environment env) {

        JsonObject config = Optional.ofNullable(env.getElementFromObject("config", "resource"))
                .orElseThrow(() -> error("Could not find config.brazilCnpj"))
                .getAsJsonObject();

        config.remove("brazilCnpj");
        log("BrazilCnpj was deleted");

        return env;
    }
}

