package net.openid.conformance.opin.testmodule.v1.resources;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CallProtectedResource;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.customerAPI.OpinAddScopesForAll;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.validator.resources.v1.OpinResourcesListValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;


@PublishTestModule(
	testName = "opin-resources-api-pagination-test",
	displayName = "Validates pagination rules on the resources API ",
	summary = "There should be at least 3 resources on the Resources API and less than 25, regardless of the status of it\n" +
		"\u2022 Creates consents with all the permissions, choosing between customer data or business, depending on what was selected for confi \n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well \n" +
		"\u2022 Calls GET Resources API \n" +
		"\u2022 Expects a 200 response - Fetch “totalRecords” from Meta and assure it has at least 3 records \n" +
		"\u2022 Calls GET Resources API with page size=1000 \n" +
		"\u2022 Expects a 200 response and expect that the links and meta attributes don’t display the next page nor prev \n" +
		"\u2022 Calls GET Resources API with page size=1 \n" +
		"\u2022 Expects a 200 response and expect that the links and meta attributes display the next page, but don’t display prev page \n" +
		"\u2022 Calls Next Page URI with page size=1 \n" +
		"\u2022 Expects a 200 response and expect that the links and meta attributes display the next page and the prev page, and that the data array has only 1 item \n" +
		"\u2022 Calls GET Resources API with page size=1001 \n" +
		"\u2022 Expects a 422 response \n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinResourcesApiPaginationTestModule extends AbstractOBBrasilFunctionalTestModuleOptionalErrors {

	private final int EXPECTED_TOTAL_RECORDS = 3;
	@Override
	protected void configureClient(){
		callAndStopOnFailure(OpinInsertMtlsCa.class);
		callAndStopOnFailure(BuildOpinResourcesConfigResourceUrlFromConsentUrl.class);
		callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		callAndStopOnFailure(OpinAddScopesForAll.class);
		callAndStopOnFailure(OpinSetPermissionsBuilderForAllPermissions.class);
		permissionsBuilder.buildFromEnv();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps(){
		OpinPreAuthorizationConsentApi steps = new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication);
		return steps;
	}

	@Override
	protected void validateResponse() {

		runInBlock("Validate Resources API request", () -> {

			callAndStopOnFailure(OpinResourcesListValidatorV1.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResponseHasLinks.class);

			env.putInteger("expected_total_records", EXPECTED_TOTAL_RECORDS);
			callAndContinueOnFailure(EnsureTotalRecordsNumber.class);
			callAndStopOnFailure(EnsurePageParameterIsIntegerAtLinks.class);
		});

		runInBlock("Call Resources API page size = 1000", () -> {

			callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1000.class);
			preCallProtectedResource();
			callAndStopOnFailure(EnsureResponseHasLinks.class);
			callAndStopOnFailure(EnsurePageParameterIsIntegerAtLinks.class);
		});

		runInBlock("Call Resources API page size = 1", () -> {

			callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1.class);
			preCallProtectedResource();
			callAndStopOnFailure(EnsureResponseHasLinks.class);
			callAndStopOnFailure(EnsurePageParameterIsIntegerAtLinks.class);
		});

		runInBlock("Call Next Endpoint page size = 1", () -> {

			callAndStopOnFailure(SetProtectedResourceUrlToNextEndpoint.class);
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1.class);
			callAndStopOnFailure(SetResourceMethodToGet.class);
			callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
			callAndStopOnFailure(CallProtectedResource.class);
			callAndStopOnFailure(EnsureResponseCodeWas200.class);
			callAndContinueOnFailure(EnsureDataArrayHasOnlyOneElement.class);
			callAndStopOnFailure(EnsureResponseHasLinks.class);
			callAndStopOnFailure(EnsurePageParameterIsIntegerAtLinks.class);
		});

		runInBlock("Call Resources API with page size = 1001", () -> {

			callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
			callAndStopOnFailure(ClearRequestObjectFromEnvironment.class);
			callAndStopOnFailure(SetProtectedResourceUrlPageSize1001.class);
			callAndStopOnFailure(SetResourceMethodToGet.class);
			callAndStopOnFailure(ClearContentTypeHeaderForResourceEndpointRequest.class);
			callAndStopOnFailure(CallProtectedResource.class);
			callAndStopOnFailure(EnsureResponseCodeWas422.class);
		});
	}
}
