package net.openid.conformance.opin.testmodule;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.validator.resources.v1.OpinResourcesListValidatorV1;

public abstract class AbstractOpinDataResourceTestModule extends AbstractOpinFunctionalTestModule {

	protected Class<? extends Condition> rootValidator;
	private String resourcesType;
	private String resourceStatus;

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddResourcesScope.class);
	}
	@Override
	protected void validateResponse() {
		callAndContinueOnFailure(rootValidator, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(PolicyIDAllSelector.class);

		callAndStopOnFailure(BuildOpinResourcesConfigResourceUrlFromConsentUrl.class);
		callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
		preCallProtectedResource("Call Resources API");

		runInBlock("Validate Resources response", () -> {
			callAndStopOnFailure(OpinResourcesListValidatorV1.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResponseHasLinks.class);
			callAndContinueOnFailure(OpinValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
			call(new ValidateSelfEndpoint().replace(ValidateResponseMetaData.class, condition(OpinValidateResponseMetaData.class)));
		});

		eventLog.startBlock("Compare active resourceId's with API resources");
		env.putString("resource_type", resourcesType);
		env.putString("resource_status", resourceStatus);
		callAndStopOnFailure(ExtractResourceIdOfResourcesWithSpecifiedTypeAndStatus.class);
		callAndStopOnFailure(CompareResourceIdWithPolicyId.class);
		eventLog.endBlock();
	}


	public void setResourcesType(String resourcesType) {
		this.resourcesType = resourcesType;
	}

	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}

	public void setRootValidator(Class<? extends Condition> rootValidator) {
		this.rootValidator = rootValidator;
	}

}
