package net.openid.conformance.opin.testmodule.v1.financialRisk;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.AbstractOpinWrongPermissionsTestModule;
import net.openid.conformance.opin.testmodule.support.AddFinancialRiskScope;
import net.openid.conformance.opin.testmodule.support.BuildFinancialRiskConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskClaimValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskListValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;



@PublishTestModule(
	testName = "opin-financial-risk-api-wrong-permissions-test",
	displayName = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Financial Risk API (“DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API \n" +
		"\u2022 Calls GET Financial Risk “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
		"\u2022 Calls GET Financial Risk policy-Info API specifying n Policy ID\n" +
		"\u2022 Expects 200 - Validate all the fields \n" +
		"\u2022 Calls GET Financial Risk premium API specifying n Policy ID\n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET Financial Risk claim API specifying n Policy ID\n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Call the POST Consents API with only either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well\n" +
		"\u2022 Calls GET Financial Risk “/” API\n"+
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Financial Risk policy-Info API specifying n Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Financial Risk premium API specifying an Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Financial Risk claim API specifying an Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
public class OpinFinancialRiskWrongPermissionsTestModule extends AbstractOpinWrongPermissionsTestModule {

	private static final String API = "insurance-financial-risk";

	@Override
	protected void configureClient() {
		callAndStopOnFailure(BuildFinancialRiskConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}
	@Override
	protected void prepareCorrectConsents(){
		callAndStopOnFailure(AddFinancialRiskScope.class);
		setApi(API);

		setRootValidator(OpinInsuranceFinancialRiskListValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceFinancialRiskPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceFinancialRiskPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceFinancialRiskClaimValidatorV1.class);

		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_FINANCIAL_RISKS).build();
	}
}
