package net.openid.conformance.opin.testmodule.phase1.assistanceGeneralAssets;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.validator.productsServices.GetAssistanceGeneralAssetsValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "Open Insurance - ProductsServices - Assistance General Assets API test",
        displayName = "Validate structure of ProductsServices - Assistance General Assets API Api resources",
        summary = "Validate structure of ProductsServices - Assistance General Assets Api resources",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1)

public class AssistanceGeneralAssetsApiTestModule extends AbstractNoAuthFunctionalTestModule {

    @Override
    protected void runTests() {
        runInBlock("Validate ProductsServices - Assistance General Assets response", () -> {
            callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class);
            callAndContinueOnFailure(GetAssistanceGeneralAssetsValidator.class, Condition.ConditionResult.FAILURE);
        });
    }
}