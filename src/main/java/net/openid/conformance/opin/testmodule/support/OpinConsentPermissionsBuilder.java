package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.ConditionSequenceRepeater;
import net.openid.conformance.fapi1advancedfinal.AbstractFAPI1AdvancedFinalServerTestModule;
import net.openid.conformance.info.TestInfoService;
import net.openid.conformance.logging.TestInstanceEventLog;
import net.openid.conformance.runner.TestExecutionManager;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.PublishTestModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@PublishTestModule(
	//Even though this class is not a test module per se, this annotation is necessary to ensure the logging. Will be changed
	//once the upstream project is refactored.
	testName = "opin-consent-permissions-builder",
	displayName = "",
	summary = "",
	profile = ""
)
public class OpinConsentPermissionsBuilder extends AbstractFAPI1AdvancedFinalServerTestModule {

	private final Logger logger = LoggerFactory.getLogger(ConditionSequenceRepeater.class);
	private Set<String> permissionsSet;
	private Set<net.openid.conformance.opin.testmodule.support.PermissionsGroup> includedPermissionsGroups;
	private boolean setsSet = false;

	public OpinConsentPermissionsBuilder(Environment env, String id, TestInstanceEventLog eventLog,
                                         TestInfoService testInfo, TestExecutionManager executionManager) {

		super.setProperties(id, null, eventLog , null, testInfo, executionManager, null);
		this.env = env;

		logger.info("OpinConsentPermissionsBuilder was correctly instantiated");
	}

	public OpinConsentPermissionsBuilder resetPermissions() {

		this.permissionsSet = new HashSet<>();
		this.includedPermissionsGroups = new HashSet<>();
		setsSet = true;
		logger.info("permissionsSet and includedPermissionsGroup sets were correctly instantiated");
		return this;
	}

	private boolean isSetsSet() {
		return setsSet;
	}

	public OpinConsentPermissionsBuilder set(String... permissions) {

		resetPermissions();
		permissionsSet.addAll(List.of(permissions));
		return this;
	}

	public OpinConsentPermissionsBuilder addPermissionsGroup(PermissionsGroup... permissionsGroups) {

		if(!isSetsSet()) {
			resetPermissions();
		}

		for (PermissionsGroup permissionsGroup : permissionsGroups) {
			permissionsSet.addAll(List.of(permissionsGroup.getPermissions()));
			includedPermissionsGroups.add(permissionsGroup);
		}
		return  this;
	}
	public OpinConsentPermissionsBuilder removePermissionsGroups(net.openid.conformance.opin.testmodule.support.PermissionsGroup... permissionGroups) {

		for (net.openid.conformance.opin.testmodule.support.PermissionsGroup permissionsGroup : permissionGroups) {
			List<String> permissions = new ArrayList<>(List.of(permissionsGroup.getPermissions()));
			permissions.remove("RESOURCES_READ");
			permissionsSet.removeAll(permissions);
		}
		return this;
	}

	public OpinConsentPermissionsBuilder removePermission(String... permissions) {

		for (String permission : permissions) {
			includedPermissionsGroups.removeIf(permissionsGroup -> permissionsGroup.name().startsWith(permission));
		}
		permissionsSet.removeAll(List.of(permissions));
		return this;
	}


	public String getLogMessage() {

		String result;
		if (includedPermissionsGroups.isEmpty()) {
			result = "CUSTOM";
		} else {
			result = String.join(" ", includedPermissionsGroups.toString()) ;
			if (includedPermissionsGroups.contains(net.openid.conformance.opin.testmodule.support.PermissionsGroup.ALL
)) {
				result = "ALL PERMISSIONS";
			}
		}

		String msg = String.format("Providing permissions %s\n", result);

		return msg;
	}

	public void build() {
		env.putString("consent_permissions", String.join(" ", permissionsSet));
		env.putString("consent_permissions_log", getLogMessage());
		call(condition(OpinLogConsentPermissions.class));
	}

	public void buildFromEnv() {

		call(condition(OpinEnsurePermissionsBuilderInEnv.class).dontStopOnFailure());

		String permissionGroupString = env.getString("permissions_builder");
		net.openid.conformance.opin.testmodule.support.PermissionsGroup permissionsGroup = null;
		for (net.openid.conformance.opin.testmodule.support.PermissionsGroup pg : PermissionsGroup.values()) {
			if (pg.name().equals(permissionGroupString)) {
				permissionsGroup = pg;
			}
		}

		this.resetPermissions().addPermissionsGroup(permissionsGroup).build();
	}
}
