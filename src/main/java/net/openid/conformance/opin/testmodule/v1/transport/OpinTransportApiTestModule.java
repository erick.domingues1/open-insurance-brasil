package net.openid.conformance.opin.testmodule.v1.transport;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModule;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoListValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoPremiumValidatorV1;
import net.openid.conformance.opin.testmodule.support.AddAutoScope;
import net.openid.conformance.opin.testmodule.support.AddTransportScope;
import net.openid.conformance.opin.testmodule.support.BuildAutoConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.BuildTransportConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportListValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-transport-api-test",
        displayName = "Validates the structure of all Transport API resources",
        summary ="Validates the structure of all Transport API resources\n" +
                "\u2022 Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_TRANSPORT_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API\n" +
                "\u2022 Calls GET transport “/” API \n" +
                "\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
                "\u2022 Calls GET transport policy-Info API \n" +
                "\u2022 Expects 200 - Validate all the fields \n" +
                "\u2022 Calls GET transport premium API \n" +
                "\u2022 Expects 200- Validate all the fields \n" +
                "\u2022 Calls GET transport claim API \n" +
                "\u2022 Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }

)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})

public class OpinTransportApiTestModule extends AbstractOpinApiTestModule {

    private static final String API = "insurance-transport";

    @Override
    protected void configureClient() {
        super.configureClient();
        callAndStopOnFailure(AddTransportScope.class);
        callAndStopOnFailure(BuildTransportConfigResourceUrlFromConsentUrl.class);
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
        permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_TRANSPORT).build();

        setApi(API);

        setRootValidator(OpinInsuranceTransportListValidatorV1.class);
        setPolicyInfoValidator(OpinInsuranceTransportPolicyInfoValidatorV1.class);
        setPremiumValidator(OpinInsuranceTransportPremiumValidatorV1.class);
        setClaimValidator(OpinInsuranceTransportClaimValidatorV1.class);
    }
}
