package net.openid.conformance.opin.testmodule.v1.transport;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.testmodule.AbstractOpinWrongPermissionsTestModule;
import net.openid.conformance.opin.testmodule.support.AddAutoScope;
import net.openid.conformance.opin.testmodule.support.AddTransportScope;
import net.openid.conformance.opin.testmodule.support.BuildAutoConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.BuildTransportConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoPremiumValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-transport-api-wrong-permissions-test",
	displayName = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a consent with all the permissions needed to access the transport API (“DAMAGES_AND_PEOPLE_TRANSPORT_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_TRANSPORT_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API \n" +
		"\u2022 Calls GET transport “/” API \n" +
		"\u2022 Expects 201 - Fetches one of the Policy IDs returned \n" +
		"\u2022 Calls GET transport policy-Info API specifying an Policy ID \n" +
		"\u2022 Expects 200 - Validate all the fields \n" +
		"\u2022 Calls GET transport premium API specifying an Policy ID \n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Calls GET transport claim API specifying an Policy ID \n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Call the POST Consents API with either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field " +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well \n" +
		"\u2022 Calls GET transport “/” API \n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET transport policy-Info API specifying an Policy ID \n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET transport premium API specifying an Policy ID \n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET transport claim API specifying an Policy ID \n" +
		"\u2022 Expects a 403 response  - Validate error response\n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
			"server.discoveryUrl",
			"client.client_id",
			"client.jwks",
			"mtls.key",
			"mtls.cert",
			"resource.consentUrl",
			"resource.brazilCpf",
			"consent.productType"
	}
)
public class OpinTransportWrongPermissionsTestModule extends AbstractOpinWrongPermissionsTestModule {

	private static final String API = "insurance-transport";

	@Override
	protected void configureClient() {
		callAndStopOnFailure(BuildTransportConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}
	@Override
	protected void prepareCorrectConsents(){
		callAndStopOnFailure(AddTransportScope.class);
		setApi(API);
		setRootValidator(OpinInsuranceTransportClaimValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceTransportPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceTransportPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceTransportClaimValidatorV1.class);

		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_TRANSPORT).build();
	}
}
