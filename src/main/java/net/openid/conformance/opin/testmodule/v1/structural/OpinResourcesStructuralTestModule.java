package net.openid.conformance.opin.testmodule.v1.structural;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.BuildResourceUrlFromStructuralResourceUrl;
import net.openid.conformance.opin.testmodule.support.PrepareUrlForOpinResourcesRoot;
import net.openid.conformance.opin.testmodule.support.ValidateOpinWellKnownUriSteps;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.validator.resources.v1.OpinResourcesListValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-resources-api-structural-test",
	displayName = "Validate structure of Resources API Endpoint 200 response",
	summary = "Validate structure of Resources API Endpoint 200 response \n"+
	"\u2022 Call the “/\" endpoint - Expect 200 and validate the response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
			"server.discoveryUrl",
			"resource.resourceUrl"
			})

public class OpinResourcesStructuralTestModule extends AbstractNoAuthFunctionalTestModule{

	private final String API = "resources";
	@Override
	protected void runTests() {

		call(new ValidateOpinWellKnownUriSteps());

		env.putString("api_base", API);
		callAndStopOnFailure(BuildResourceUrlFromStructuralResourceUrl.class);

		runInBlock("Validate Resources - Root", () -> {
			callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class);
			callAndContinueOnFailure(OpinResourcesListValidatorV1.class, Condition.ConditionResult.FAILURE);
		});
	}
}

