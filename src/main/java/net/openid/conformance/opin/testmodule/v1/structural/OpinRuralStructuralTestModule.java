package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralClaimListValidatorV1;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralListValidatorV1;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-rural-api-structural-test",
	displayName = "Validate structure of Rural API Endpoint 200 response",
	summary = "Validate structure of Rural API Endpoint 200 response \n"+
		"\u2022 Call the “/\" endpoint - Expect 200 and validate response\n" +
		"\u2022 Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
		"\u2022 Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
		"\u2022 Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"resource.resourceUrl"
	})

public class OpinRuralStructuralTestModule extends AbstractOpinDataStructuralTestModule {

	private final String API = "insurance-rural";
	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {

		setApi(API);
		setRootValidator(OpinInsuranceRuralListValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceRuralPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceRuralPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceRuralClaimListValidatorV1.class);

		super.configure(config, baseUrl, externalUrlOverride);
	}
}

