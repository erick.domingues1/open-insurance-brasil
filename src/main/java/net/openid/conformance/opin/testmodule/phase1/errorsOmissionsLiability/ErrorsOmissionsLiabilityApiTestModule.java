package net.openid.conformance.opin.testmodule.phase1.errorsOmissionsLiability;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.validator.productsServices.GetErrorsOmissionsLiabilityValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "Open Insurance - Errors Omissions Liability API test",
        displayName = "Validate structure of Errors Omissions Liability response",
        summary = "Validate structure of Errors Omissions Liability response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class ErrorsOmissionsLiabilityApiTestModule extends AbstractNoAuthFunctionalTestModule {
    @Override
    protected void runTests() {
        runInBlock("Validate ProductsServices Errors Omissions Liability response", () -> {
            callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
            callAndStopOnFailure(CallNoCacheResource.class);
            callAndContinueOnFailure(DoNotStopOnFailure.class);
            callAndContinueOnFailure(GetErrorsOmissionsLiabilityValidator.class, Condition.ConditionResult.FAILURE);
        });
    }
}