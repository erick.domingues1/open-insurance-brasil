package net.openid.conformance.opin.testmodule.v1.resources;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.validator.resources.v1.OpinResourcesListValidatorV1;
import net.openid.conformance.sequence.ConditionSequence;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;


@PublishTestModule(
	testName = "opin-resources-api-test",
	displayName = "Validate structure of all resources API resources",
	summary = "Validates the structure of all resources API resources\n" +
		"• Creates a Consent will all of the existing permissions \n" +
		"• Checks all of the fields sent on the consent API are specification compliant\n" +
		"• Calls the GET resources API\n" +
		"• Expects a 200 - Validate Reponse",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks", "directory.keystore"
})
public class OpinResourcesApiTestModule extends AbstractOBBrasilFunctionalTestModuleOptionalErrors {

	@Override
	protected void configureClient(){
		callAndStopOnFailure(OpinInsertMtlsCa.class);
		callAndStopOnFailure(BuildOpinResourcesConfigResourceUrlFromConsentUrl.class);
		callAndStopOnFailure(PrepareUrlForOpinResourcesRoot.class);
		super.configureClient();
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps(){
		OpinPreAuthorizationConsentApi steps = new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication);
		return steps;
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		callAndStopOnFailure(OpinSetPermissionsBuilderForAllPermissions.class);
		permissionsBuilder.buildFromEnv();

		callAndStopOnFailure(IgnoreResponseError.class);
		callAndStopOnFailure(AddResourcesScope.class);
	}

	@Override
	protected void validateResponse() {

		runInBlock("Validate resources api request", () -> {
			callAndStopOnFailure(EnsureResponseCodeWas200.class);
			callAndStopOnFailure(OpinResourcesListValidatorV1.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResponseHasLinks.class);
			callAndContinueOnFailure(OpinValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
			call(new ValidateSelfEndpoint().replace(ValidateResponseMetaData.class, condition(OpinValidateResponseMetaData.class)));
		});

	}
}
