package net.openid.conformance.opin.testmodule.v1.patrimonial;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddPatrimonialScope;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModule;
import net.openid.conformance.opin.testmodule.support.BuildPatrimonialConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialClaimValidatorV1;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialListValidatorV1;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-patrimonial-api-test",
	displayName = "Validates the structure of all Patrimonial API resources",
	summary = "Validates the structure of all Patrimonial API resources\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Patrimonial API (“DAMAGES_AND_PEOPLE_PATRIMONIAL_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_PATRIMONIAL_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Patrimonial “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
		"\u2022 Calls GET Patrimonial policy-Info API \n" +
		"\u2022 Expects 200 - Validate all the fields\n" +
		"\u2022 Calls GET patrimonial premium API \n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET patrimonial claim API \n" +
		"\u2022 Expects 200- Validate all the fields",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinPatrimonialApiTestModule extends AbstractOpinApiTestModule {

	private static final String API = "insurance-patrimonial";

	@Override
	protected void configureClient() {
		super.configureClient();
		callAndStopOnFailure(AddPatrimonialScope.class);
		callAndStopOnFailure(BuildPatrimonialConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_PATRIMONIAL).build();

		setApi(API);
		setRootValidator(OpinInsurancePatrimonialListValidatorV1.class);
		setPolicyInfoValidator(OpinInsurancePatrimonialPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsurancePatrimonialPremiumValidatorV1.class);
		setClaimValidator(OpinInsurancePatrimonialClaimValidatorV1.class);
	}
}
