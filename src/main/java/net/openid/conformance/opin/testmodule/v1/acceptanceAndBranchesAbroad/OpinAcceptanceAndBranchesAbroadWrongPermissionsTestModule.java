package net.openid.conformance.opin.testmodule.v1.acceptanceAndBranchesAbroad;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.AbstractOpinWrongPermissionsTestModule;
import net.openid.conformance.opin.testmodule.support.AddAcceptanceBranchesAbroadScope;
import net.openid.conformance.opin.testmodule.support.BuildAcceptanceAndBranchesAbroadConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadListValidatorV1;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-acceptance-and-branches-abroad-api-wrong-permissions-test",
	displayName = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
		"\u2022 Creates a consent with all the permissions needed to access the Acceptance and Branches Abroad API (“DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad policy-Info API specifying n Policy ID\n" +
		"\u2022 Expects 200 - Validate all the fields \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad premium API specifying n Policy ID\n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad claim API specifying n Policy ID\n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Call the POST Consents API with only either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field\n" +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well\n" +
		"\u2022 Calls GET Acceptance and Branches Abroad “/” API\n"+
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad policy-Info API specifying n Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad premium API specifying an Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET Acceptance and Branches Abroad claim API specifying an Policy ID\n" +
		"\u2022 Expects a 403 response  - Validate error response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
public class OpinAcceptanceAndBranchesAbroadWrongPermissionsTestModule extends AbstractOpinWrongPermissionsTestModule {
	private static final String API = "insurance-acceptance-and-branches-abroad";

	@Override
	protected void configureClient() {
		callAndStopOnFailure(BuildAcceptanceAndBranchesAbroadConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}
	@Override
	protected void prepareCorrectConsents(){
		callAndStopOnFailure(AddAcceptanceBranchesAbroadScope.class);
		setApi(API);

		setRootValidator(OpinInsuranceAcceptanceAndBranchesAbroadListValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceAcceptanceAndBranchesAbroadPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceAcceptanceAndBranchesAbroadPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceAcceptanceAndBranchesAbroadClaimValidatorV1.class);

		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD).build();
	}
}
