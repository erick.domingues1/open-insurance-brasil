package net.openid.conformance.opin.testmodule.phase1.pension;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.validator.productsNServices.GetPensionPlanValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Open Insurance - ProductsNServices - Pension Plan API test",
	displayName = "Validate structure of ProductsNServices - Pension Plan API Api resources",
	summary = "Validate structure of ProductsNServices - Pension Plan Api resources",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1)
public class PensionPlanApiTestModule extends AbstractNoAuthFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validate ProductsNServices - Pension Plan response", () -> {
			callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class);
			callAndContinueOnFailure(GetPensionPlanValidator.class, Condition.ConditionResult.FAILURE);
		});
	}
}
