package net.openid.conformance.opin.testmodule.v1.housing;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModule;
import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.opin.validator.insuranceHousing.v1.*;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-housing-api-test",
        displayName = "Validates the structure of all housing API resources",
        summary = "Validates the structure of all housing API resources\n" +
                "• Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_HOUSING_READ”, “DAMAGES_AND_PEOPLE_HOUSING_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_HOUSING_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_HOUSING_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "• Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API\n" +
                "• Calls GET housing “/” API\n" +
                "• Expects 200 - Fetches one of the Policy IDs returned\n" +
                "• Calls GET housing policy-Info API \n" +
                "• Expects 200 - Validate all the fields\n" +
                "• Calls GET housing premium API \n" +
                "• Expects 200- Validate all the fields\n" +
                "• Calls GET housing claim API \n" +
                "• Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
        "client.org_jwks"
})

public class OpinHousingApiTestModule extends AbstractOpinApiTestModule {
    private static final String API = "insurance-housing";

    @Override
    protected void configureClient() {
        super.configureClient();
        callAndStopOnFailure(AddHousingScope.class);
        callAndStopOnFailure(BuildHousingConfigResourceUrlFromConsentUrl.class);
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
        permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_HOUSING).build();

        setApi(API);

        setRootValidator(OpinInsuranceHousingListValidatorV1.class);
        setPolicyInfoValidator(OpinInsuranceHousingPolicyInfoValidatorV1.class);
        setPremiumValidator(OpinInsuranceHousingPremiumValidatorV1.class);
        setClaimValidator(OpinInsuranceHousingClaimValidatorV1.class);
    }
}