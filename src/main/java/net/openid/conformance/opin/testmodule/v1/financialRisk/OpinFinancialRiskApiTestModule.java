package net.openid.conformance.opin.testmodule.v1.financialRisk;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModule;
import net.openid.conformance.opin.testmodule.support.AddFinancialRiskScope;
import net.openid.conformance.opin.testmodule.support.BuildFinancialRiskConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskClaimValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskListValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-financial-risk-api-test",
	displayName = "Validates the structure of all financial risk API resources",
	summary = "Validates the structure of all financial risk API resources\n" +
		"\u2022 Creates a consent with all the permissions needed to access the financial risk API (“DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_FINANCIAL_RISKS_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consents API\n" +
		"\u2022 Calls GET Financial Risk “/” API\n" +
		"\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
		"\u2022 Calls GET Financial Risk policy-Info API \n" +
		"\u2022 Expects 200 - Validate all the fields\n" +
		"\u2022 Calls GET Financial Risk premium API \n" +
		"\u2022 Expects 200- Validate all the fields\n" +
		"\u2022 Calls GET Financial Risk claim API \n" +
		"\u2022 Expects 200- Validate all the fields",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
		"consent.productType"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinFinancialRiskApiTestModule extends AbstractOpinApiTestModule {

	private static final String API = "insurance-financial-risk";

	@Override
	protected void configureClient() {
		super.configureClient();
		callAndStopOnFailure(AddFinancialRiskScope.class);
		callAndStopOnFailure(BuildFinancialRiskConfigResourceUrlFromConsentUrl.class);
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_FINANCIAL_RISKS).build();

		setApi(API);
		setRootValidator(OpinInsuranceFinancialRiskListValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceFinancialRiskPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceFinancialRiskPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceFinancialRiskClaimValidatorV1.class);
	}
}
