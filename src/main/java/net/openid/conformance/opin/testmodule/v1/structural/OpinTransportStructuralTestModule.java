package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportListValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-transport-api-structural-test",
	displayName = "Validate structure of Transport API Endpoints 200 response",
	summary = "Validate structure of Transport API Endpoints 200 response \n"+
	"\u2022 Call the “/\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
			"server.discoveryUrl",
			"resource.resourceUrl"
			})

public class OpinTransportStructuralTestModule extends AbstractOpinDataStructuralTestModule {

	private final String API = "insurance-transport";

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {

		setApi(API);

		setRootValidator(OpinInsuranceTransportListValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceTransportPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceTransportPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceTransportClaimValidatorV1.class);

		super.configure(config, baseUrl, externalUrlOverride);
	}

}

