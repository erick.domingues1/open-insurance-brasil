package net.openid.conformance.opin.testmodule;

import com.google.common.base.Strings;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.generic.ErrorValidator;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractPermissionsCheckingFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.CallProtectedResourceExpectingFailureSequence;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas403;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.testmodule.support.*;
import net.openid.conformance.sequence.ConditionSequence;

public abstract class AbstractOpinWrongPermissionsTestModule extends AbstractPermissionsCheckingFunctionalTestModule {

	protected String api;
	protected OpinConsentPermissionsBuilder permissionsBuilder;
	protected Class<? extends Condition> rootValidator;
	protected Class<? extends Condition> policyInfoValidator;
	protected Class<? extends Condition> premiumValidator;
	protected Class<? extends Condition> claimValidator;

	@Override
	protected void configureClient() {
		callAndStopOnFailure(OpinInsertMtlsCa.class);
		super.configureClient();
	}

	@Override
	protected void preFetchResources() {
		callAndContinueOnFailure(rootValidator, Condition.ConditionResult.FAILURE);
		callAndStopOnFailure(PolicyIDSelector.class);

		validate(PrepareUrlForFetchingPolicyInfo.class, policyInfoValidator, String.format("Fetch %s policy info", api));
		validate(PrepareUrlForFetchingPremium.class, premiumValidator, String.format("Fetch %s premium", api));
		validate(PrepareUrlForFetchingClaim.class, claimValidator, String.format("Fetch %s claim", api));
	}

	@Override
	protected ConditionSequence createOBBPreauthSteps() {
		OpinPreAuthorizationConsentApi steps = new OpinPreAuthorizationConsentApi(addTokenEndpointClientAuthentication);
		return steps;
	}

	@Override
	protected void prepareIncorrectPermissions() {

		String productType = env.getString("config", "consent.productType");

		if(!Strings.isNullOrEmpty(productType) && productType.equals("business")) {
			permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS).build();
		}else if(!Strings.isNullOrEmpty(productType) && productType.equals("personal")){
			permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL).build();
		}

	}

	@Override
	protected void requestResourcesWithIncorrectPermissions() {

		runInBlock("Ensure we cannot call the root API", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingRoot.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(ErrorValidator.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the policy-Info API specifying a Policy ID", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingPolicyInfo.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(ErrorValidator.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the premium API specifying an Policy ID", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingPremium.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(ErrorValidator.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResponseCodeWas403.class);
		});

		runInBlock("Ensure we cannot call the claim API specifying an Policy ID", () -> {
			callAndStopOnFailure(PrepareUrlForFetchingClaim.class);
			call(sequence(CallProtectedResourceExpectingFailureSequence.class));
			callAndContinueOnFailure(ErrorValidator.class, Condition.ConditionResult.FAILURE);
			callAndStopOnFailure(EnsureResponseCodeWas403.class);
		});
	}

	protected void setApi(String api) {
		this.api = api;
		env.putString("api", api);
	}

	protected void validate(Class<? extends Condition> prepareUrlCondition, Class<? extends Condition> validator, String logMsg) {
		callAndStopOnFailure(prepareUrlCondition);
		preCallProtectedResource(logMsg);
		callAndContinueOnFailure(validator, Condition.ConditionResult.FAILURE);
	}

	public void setRootValidator(Class<? extends Condition> rootValidator) {
		this.rootValidator = rootValidator;
	}
	public void setPolicyInfoValidator(Class<? extends Condition> policyInfoValidator) {
		this.policyInfoValidator = policyInfoValidator;
	}
	public void setPremiumValidator(Class<? extends Condition> premiumValidator) {
		this.premiumValidator = premiumValidator;
	}
	public void setClaimValidator(Class<? extends Condition> claimValidator) {
		this.claimValidator = claimValidator;
	}

	@Override
	protected abstract void prepareCorrectConsents();

}
