package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateResponseMetaData;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OpinValidateResponseMetaData extends ValidateResponseMetaData {
	@Override
	protected void validateSelfLink(String selfLink, String consentIdField) {
		final Pattern consentRegex = Pattern.compile(String.format("^(https://)(.*?)(/open-insurance/consents/v\\d/%s)$", consentIdField), Pattern.CASE_INSENSITIVE);
		Matcher matcher = consentRegex.matcher(selfLink);
		if (matcher.find()) {
			logSuccess("Consent ID in self link matches the consent ID in the returned object");
		} else {
			throw error("Invalid 'self' link URI. URI: " + selfLink);
		}
	}
}
