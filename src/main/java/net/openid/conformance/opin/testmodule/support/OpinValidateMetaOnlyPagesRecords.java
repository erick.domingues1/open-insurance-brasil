package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.ValidateResponseMetaData;

/**
 * Validate Opin Meta only with totalPages and totalRecords field
 **/
public class OpinValidateMetaOnlyPagesRecords extends ValidateResponseMetaData {
	@Override
	protected void validateMetaRequestDateTime() {
		//Not needed for this condition
	}

}
