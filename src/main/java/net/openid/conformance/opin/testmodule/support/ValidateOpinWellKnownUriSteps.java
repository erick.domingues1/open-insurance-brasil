package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.CheckDiscEndpointDiscoveryUrl;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureResponseCodeWas200;
import net.openid.conformance.openbanking_brasil.testmodules.support.EnsureWellKnownUriIsRegistered;
import net.openid.conformance.opin.testmodule.support.CallOpinDirectoryParticipantsEndpoint;
import net.openid.conformance.sequence.AbstractConditionSequence;

public class ValidateOpinWellKnownUriSteps extends AbstractConditionSequence {

	@Override
	public void evaluate() {
		call(exec().startBlock("Validating Well-Known URI"));
		callAndContinueOnFailure(CheckDiscEndpointDiscoveryUrl.class, Condition.ConditionResult.FAILURE);
		callAndContinueOnFailure(CallOpinDirectoryParticipantsEndpoint.class, Condition.ConditionResult.FAILURE);
		call(exec().mapKey("resource_endpoint_response_full", "directory_participants_response_full"));
		callAndContinueOnFailure(EnsureResponseCodeWas200.class, Condition.ConditionResult.FAILURE);
		call(exec().unmapKey("resource_endpoint_response_full"));
		callAndContinueOnFailure(EnsureWellKnownUriIsRegistered.class, Condition.ConditionResult.FAILURE);
		call(exec().endBlock());
	}
}
