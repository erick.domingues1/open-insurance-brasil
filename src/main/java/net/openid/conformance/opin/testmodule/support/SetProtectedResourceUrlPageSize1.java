package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PostEnvironment;
import net.openid.conformance.opin.testmodule.support.AbstractPageSizeCondition;
import net.openid.conformance.testmodule.Environment;

public class SetProtectedResourceUrlPageSize1 extends AbstractPageSizeCondition {

	@Override
	protected int getPageSize() {
		return 1;
	}
}

