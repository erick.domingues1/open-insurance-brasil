package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.AbstractCondition;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.testmodule.Environment;
import org.apache.commons.lang3.StringUtils;

public class PrepareToGetHomeInsuranceApi extends AbstractCondition {

	@Override
	@PreEnvironment(required = "config")
	public Environment evaluate(Environment env) {
		String baseURL = env.getString("config", "resource.resourceUrl");
		baseURL = StringUtils.removeEnd(baseURL, "/");
		String commercializationArea =  env.getString("config", "resource.commercializationArea");
		String protectedUrl = baseURL;
		if (commercializationArea != null){
			protectedUrl = String.format("%s/%s", baseURL, commercializationArea);
		}
		env.putString("protected_resource_url", protectedUrl);
		return env;
	}
}
