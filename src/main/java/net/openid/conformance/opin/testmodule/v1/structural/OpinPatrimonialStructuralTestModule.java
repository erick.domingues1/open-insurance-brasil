package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialClaimValidatorV1;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialListValidatorV1;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-patrimonial-api-structural-test",
	displayName = "Validate structure of Patrimonial API Endpoints 200 response",
	summary = "Validate structure of Patrimonial API Endpoints 200 response \n"+
		"\u2022 Call the “/\" endpoint - Expect 200 and validate response\n" +
		"\u2022 Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
		"\u2022 Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
		"\u2022 Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
			"resource.resourceUrl"
			})

public class OpinPatrimonialStructuralTestModule extends AbstractOpinDataStructuralTestModule {

	private final String API = "insurance-patrimonial";

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {

		setApi(API);
		setRootValidator(OpinInsurancePatrimonialListValidatorV1.class);
		setPolicyInfoValidator(OpinInsurancePatrimonialPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsurancePatrimonialPremiumValidatorV1.class);
		setClaimValidator(OpinInsurancePatrimonialClaimValidatorV1.class);

		super.configure(config, baseUrl, externalUrlOverride);
	}

}

