package net.openid.conformance.opin.testmodule.v1.consents;

import com.google.gson.JsonObject;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.*;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.generic.ErrorValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddConsentScope;
import net.openid.conformance.openbanking_brasil.testmodules.support.AddDummyPersonalProductTypeToConfig;
import net.openid.conformance.openbanking_brasil.testmodules.support.CheckAuthorizationEndpointHasError;
import net.openid.conformance.opin.testmodule.AbstractOpinFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.BuildFinancialRiskConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.OpinInsertMtlsCa;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.testmodule.support.OpinAddDummyCpfToConfig;
import net.openid.conformance.opin.testmodule.support.DeleteCnpjFromConfig;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
	testName = "opin-consent-invalid-user-test",
	displayName = "This test will use a dummy, but well-formated payload to make sure that the server will accept the POST Consents request, as mandated by the security guidelines, however, will not be able to complete the authorization code flow as no user with this CPF exists on the financial institution",
	summary = "This test will use a dummy, but well-formated payload to make sure that the server will accept the POST Consents request, as mandated by the security guidelines, however, will not be able to complete the authorization code flow as no user with this CPF exists on the financial institution\n" +
		"• Call the POST Consents API with the dummy payload\n" +
		"• Expect the server to accept the message and return the 201 as the financial institution should not validate the CPF at that stage of the process.\n" +
		"• Redirect the user to authorize the consent\n" +
		"• Expect a failure an error on the authorization redirect",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf",
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinConsentInvalidUser extends AbstractOpinFunctionalTestModule {

	OpinConsentPermissionsBuilder permissionsBuilder;
	@Override
	protected void configureClient(){
		callAndStopOnFailure(OpinInsertMtlsCa.class);
		//Arbitrary resource
		callAndStopOnFailure(BuildFinancialRiskConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}

	@Override
	protected void onConfigure(JsonObject config, String baseUrl) {
		callAndStopOnFailure(AddConsentScope.class);
		callAndStopOnFailure(DeleteCnpjFromConfig.class);
		callAndStopOnFailure(OpinAddDummyCpfToConfig.class);
		callAndStopOnFailure(AddDummyPersonalProductTypeToConfig.class);
		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.resetPermissions()
			.addPermissionsGroup(PermissionsGroup.ALL)
			.removePermissionsGroups(PermissionsGroup.CUSTOMERS_BUSINESS)
			.build();
	}

	@Override
	protected void onAuthorizationCallbackResponse() {

		callAndContinueOnFailure(CheckMatchingCallbackParameters.class, Condition.ConditionResult.FAILURE);

		callAndContinueOnFailure(RejectStateInUrlQueryForHybridFlow.class, Condition.ConditionResult.FAILURE, "OIDCC-3.3.2.5");

		callAndStopOnFailure(CheckAuthorizationEndpointHasError.class);

		callAndContinueOnFailure(ErrorValidator.class, Condition.ConditionResult.FAILURE);

		callAndContinueOnFailure(CheckForUnexpectedParametersInErrorResponseFromAuthorizationEndpoint.class, Condition.ConditionResult.WARNING, "OIDCC-3.1.2.6");

		callAndContinueOnFailure(CheckStateInAuthorizationResponse.class, Condition.ConditionResult.FAILURE, "OIDCC-3.2.2.5", "JARM-4.4-2");

		callAndContinueOnFailure(ValidateIssInAuthorizationResponse.class, Condition.ConditionResult.WARNING, "OAuth2-iss-2");

		fireTestFinished();
	}


	@Override
	protected void validateResponse() {

	}
}
