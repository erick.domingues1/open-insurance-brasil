package net.openid.conformance.opin.testmodule.phase1.electronic;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.openbanking_brasil.testmodules.support.LogOnlyFailure;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.testplan.utils.PrepareToGetOpenInsuranceApi;
import net.openid.conformance.opin.validator.channels.v1.ElectronicChannelsValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Open Insurance - Channels - Electronic Channels API test",
	displayName = "Validate structure of Channels - Electronic Channels Api resources",
	summary = "Validate structure of Channels - Electronic Channels Api resources",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1
)
public class ElectronicChannelsApiTestModule extends AbstractNoAuthFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validate Channels - Electronic Channels response", () -> {
			callAndStopOnFailure(PrepareToGetOpenInsuranceApi.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class);
			callAndContinueOnFailure(LogOnlyFailure.class);
			callAndContinueOnFailure(ElectronicChannelsValidator.class, Condition.ConditionResult.FAILURE);
		});
	}
}
