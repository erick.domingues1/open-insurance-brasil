package net.openid.conformance.opin.testmodule.v1.structural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.validator.insuranceResponsibility.v1.OpinInsuranceResponsibilityClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceResponsibility.v1.OpinInsuranceResponsibilityListValidatorV1;
import net.openid.conformance.opin.validator.insuranceResponsibility.v1.OpinInsuranceResponsibilityPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceResponsibility.v1.OpinInsuranceResponsibilityPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-responsibilities-api-structural-test",
	displayName = "Validate structure of Responsibility API Endpoints 200 response",
	summary = "Validate structure of Responsibility API Endpoints 200 response \n"+
	"\u2022 Call the “/\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/policy-info\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/premium\" endpoint - Expect 200 and validate response\n" +
	"\u2022 Call the “/{policyId}/claim\" endpoint - Expect 200 and validate response",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
			"server.discoveryUrl",
			"resource.resourceUrl"
			})

public class OpinResponsibilitiesStructuralTestModule extends AbstractOpinDataStructuralTestModule {

	private final String API = "insurance-responsibility";

	@Override
	public void configure(JsonObject config, String baseUrl, String externalUrlOverride) {

		setApi(API);
		setRootValidator(OpinInsuranceResponsibilityListValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceResponsibilityPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceResponsibilityPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceResponsibilityClaimValidatorV1.class);

		super.configure(config, baseUrl, externalUrlOverride);
	}

}

