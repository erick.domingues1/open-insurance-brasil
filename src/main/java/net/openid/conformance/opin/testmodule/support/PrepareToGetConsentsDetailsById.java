package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class PrepareToGetConsentsDetailsById extends ResourceBuilder {
	@Override
	@PreEnvironment(strings = "consent_id")
	public Environment evaluate(Environment env) {
		String consentID = env.getString("consent_id");
		setApi("consents");
		setEndpoint(String.format("/%s", consentID));

		return super.evaluate(env);
	}
}
