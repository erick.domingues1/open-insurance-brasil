package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.openbanking_brasil.testmodules.support.CallDirectoryParticipantsEndpoint;

public class CallOpinDirectoryParticipantsEndpoint extends CallDirectoryParticipantsEndpoint {

	private static final String OPIN_PARTICIPANTS_URI = "https://data.sandbox.directory.opinbrasil.com.br/participants";
	@Override
	protected String getParticipantsUri() {
		return String.format(OPIN_PARTICIPANTS_URI);
	}

}
