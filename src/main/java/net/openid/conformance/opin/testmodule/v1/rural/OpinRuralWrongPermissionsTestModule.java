package net.openid.conformance.opin.testmodule.v1.rural;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.AbstractOpinWrongPermissionsTestModule;
import net.openid.conformance.opin.testmodule.support.AddFinancialRiskScope;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.testmodule.support.AddRuralScope;
import net.openid.conformance.opin.testmodule.support.BuildRuralConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralClaimListValidatorV1;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralListValidatorV1;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
        testName = "opin-rural-api-wrong-permissions-test",
        displayName = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test",
        summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
                "\u2022 Creates a consent with all the permissions needed to access the Rural API (“DAMAGES_AND_PEOPLE_RURAL_READ”, “DAMAGES_AND_PEOPLE_RURAL_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API \n" +
                "\u2022 Calls GET Rural “/” API\n" +
                "\u2022 Expects 200 - Fetches one of the Policy IDs returned \n" +
                "\u2022 Calls GET Rural policy-Info API specifying n Policy ID\n" +
                "\u2022 Expects 200 - Validate all the fields \n" +
                "\u2022 Calls GET Rural premium API specifying n Policy ID\n" +
                "\u2022 Expects 200- Validate all the fields\n" +
                "\u2022 Calls GET Rural claim API specifying n Policy ID\n" +
                "\u2022 Expects 200- Validate all the fields \n" +
                "\u2022 Call the POST Consents API with only either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field\n" +
                "\u2022 Expects a success 201 - Expects a success on Redirect as well\n" +
                "\u2022 Calls GET Rural “/” API\n"+
                "\u2022 Expects a 403 response  - Validate error response \n" +
                "\u2022 Calls GET Rural policy-Info API specifying n Policy ID\n" +
                "\u2022 Expects a 403 response  - Validate error response \n" +
                "\u2022 Calls GET Rural premium API specifying an Policy ID\n" +
                "\u2022 Expects a 403 response  - Validate error response \n" +
                "\u2022 Calls GET Rural claim API specifying an Policy ID\n" +
                "\u2022 Expects a 403 response  - Validate error response",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
public class OpinRuralWrongPermissionsTestModule extends AbstractOpinWrongPermissionsTestModule {

    private static final String API = "insurance-rural";

    @Override
    protected void configureClient() {
        callAndStopOnFailure(BuildRuralConfigResourceUrlFromConsentUrl.class);
        super.configureClient();
    }
    @Override
    protected void prepareCorrectConsents(){
        callAndStopOnFailure(AddRuralScope.class);
        setApi(API);

        setRootValidator(OpinInsuranceRuralListValidatorV1.class);
        setPolicyInfoValidator(OpinInsuranceRuralPolicyInfoValidatorV1.class);
        setPremiumValidator(OpinInsuranceRuralPremiumValidatorV1.class);
        setClaimValidator(OpinInsuranceRuralClaimListValidatorV1.class);

        permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
        permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_RURAL).build();
    }
}
