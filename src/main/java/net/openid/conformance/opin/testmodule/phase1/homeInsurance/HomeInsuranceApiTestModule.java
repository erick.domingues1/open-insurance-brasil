package net.openid.conformance.opin.testmodule.phase1.homeInsurance;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.PrepareToGetHomeInsuranceApi;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.validator.productsNServices.GetHomeInsuranceValidator;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "Open Insurance - ProductsNServices - Home Insurance API test",
	displayName = "Validate structure of ProductsNServices - Home Insurance API Api resources",
	summary = "Validate structure of ProductsNServices - Home Insurance Api resources",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE1,
	configurationFields = {
		"resource.commercializationArea"
	}
)
public class HomeInsuranceApiTestModule extends AbstractNoAuthFunctionalTestModule {

	@Override
	protected void runTests() {
		runInBlock("Validate ProductsNServices - Home Insurance response", () -> {
			callAndStopOnFailure(PrepareToGetHomeInsuranceApi.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class);
			callAndContinueOnFailure(GetHomeInsuranceValidator.class, Condition.ConditionResult.FAILURE);
		});
	}
}
