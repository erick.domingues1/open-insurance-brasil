package net.openid.conformance.opin.testmodule.support;

import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.openbanking_brasil.testmodules.support.resource.ResourceBuilder;
import net.openid.conformance.testmodule.Environment;

public class PrepareUrlForFetchingPatrimonialPolicyInfo extends ResourceBuilder {
    public PrepareUrlForFetchingPatrimonialPolicyInfo() {
    }

    @PreEnvironment(
            strings = {"policyId"}
    )
    public Environment evaluate(Environment env) {
        String policyId = env.getString("policyId");
        String api = "insurance-patrimonial";
        setApi(api);
        this.setEndpoint(String.format("/%s/%s/policy-info", api, policyId));
        return super.evaluate(env);
    }
}