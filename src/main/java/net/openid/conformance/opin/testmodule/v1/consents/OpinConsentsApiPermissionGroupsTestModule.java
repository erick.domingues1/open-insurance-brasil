package net.openid.conformance.opin.testmodule.v1.consents;

import com.google.common.base.Strings;
import net.openid.conformance.condition.Condition;
import net.openid.conformance.condition.client.FAPIBrazilAddExpirationToConsentRequest;
import net.openid.conformance.condition.client.FAPIBrazilOpenInsuranceCreateConsentRequest;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.support.*;
import net.openid.conformance.opin.testmodule.support.OpinAbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.OpinValidateResponseMetaData;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.validator.consents.v1.OpinCreateNewConsentValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.testmodule.TestFailureException;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;
import org.springframework.http.HttpStatus;

@PublishTestModule(
	testName = "opin-consent-api-test-permission-groups",
	displayName = "Validate that consent API accepts the consent groups",
	summary = "Validates that consent API accepts the consent groups\n" +
		"\u2022 Creates a series of consent requests with valid permissions group and expect for each of them a 201 to be returned by the server\n" +
		"\u2022 Validates consent API request for 'Personal Registration Data' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Personal Additional Information' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Personal Qualification' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Business Registration Data' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Business Additional Information' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Business Qualification' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Capitalization Titles' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Pension Risk' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Patrimonial' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Aeronautical' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Nautical' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Nuclear' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Oil' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Responsability' permission group(s)\n"+
		"\u2022 Validates consent API request for 'Damages and People Transport' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Financial Risks' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Rural' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Auto' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Housing' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Acceptance and Branches Abroad' permission group(s)\n" +
		"\u2022 Validates consent API request for 'Damages and People Person' permission group(s)\n" +
		"\u2022 Expect a 201 or 422 - Unprocessable Entity in case the server does not support the permission group mentioned\n" +
		"\u2022 Validate Response\n" +
		"\u2022 For 201 responses, ensure that the permissions are not widened\n",
	profile = OBBProfile.OBB_PROFILE,
	configurationFields = {
		"server.discoveryUrl",
		"client.client_id",
		"client.jwks",
		"mtls.key",
		"mtls.cert",
		"resource.consentUrl",
		"resource.brazilCpf"
	}
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
	"client.org_jwks"
})
public class OpinConsentsApiPermissionGroupsTestModule extends OpinAbstractClientCredentialsGrantFunctionalTestModule {

	private OpinConsentPermissionsBuilder permissionsBuilder;
	private boolean passed = false;

	@Override
	protected void runTests() {
		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		passed = false;


		String productType = env.getString("config", "consent.productType");
		if (!Strings.isNullOrEmpty(productType) && productType.equals("business")) {
			permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_BUSINESS).build();
			validatePermissions(PermissionsGroup.CUSTOMERS_BUSINESS);
		}
		if (!Strings.isNullOrEmpty(productType) && productType.equals("personal")) {
			permissionsBuilder.resetPermissions().addPermissionsGroup(PermissionsGroup.CUSTOMERS_PERSONAL).build();
			validatePermissions(PermissionsGroup.CUSTOMERS_PERSONAL);
		}


		for (PermissionsGroup permissionsGroup : PermissionsGroup.values()) {
			if (permissionsGroup.equals(PermissionsGroup.CUSTOMERS_BUSINESS) ||
					permissionsGroup.equals(PermissionsGroup.CUSTOMERS_PERSONAL) ||
					permissionsGroup.equals(PermissionsGroup.RESOURCES) ||
					permissionsGroup.equals(PermissionsGroup.ALL) ||
					permissionsGroup.equals(PermissionsGroup.ALL_PERSONAL) ||
			        permissionsGroup.equals(PermissionsGroup.ALL_BUSINESS)) {
				continue;
			}
			permissionsBuilder.resetPermissions().addPermissionsGroup(permissionsGroup).build();
			validatePermissions(permissionsGroup);
		}

		//If all validates returned a 422
		if (!passed) {
			throw new TestFailureException(getId(), "All resources returned a 422 when at least one set of permissions should have passed");
		}
	}


	private void validatePermissions(PermissionsGroup permissionsGroup) {
		String logMessage = String.format("Validate consent api request for '%s' permission group(s)", permissionsGroup.name());
		runInBlock(logMessage, () -> {

			callAndStopOnFailure(PrepareToPostConsentRequest.class);
			callAndStopOnFailure(SetContentTypeApplicationJson.class);
			callAndStopOnFailure(FAPIBrazilOpenInsuranceCreateConsentRequest.class);
			callAndStopOnFailure(FAPIBrazilAddExpirationToConsentRequest.class);

			callAndContinueOnFailure(CallConsentEndpointWithBearerTokenAnyHttpMethod.class, Condition.ConditionResult.SUCCESS);
			call(exec().mapKey("resource_endpoint_response_full", "consent_endpoint_response_full"));
			callAndContinueOnFailure(ResourceEndpointResponseFromFullResponse.class);

			//if resource_endpoint_response isn't empty we also need to check if error 422 wasn't given for other reason
			if (!env.getString("resource_endpoint_response").equals("{}") && env.getInteger("resource_endpoint_response_full", "status") != HttpStatus.UNPROCESSABLE_ENTITY.value()) {
				passed = true;
				callAndContinueOnFailure(EnsureResponseCodeWas201.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(OpinCreateNewConsentValidatorV1.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(EnsureResponseHasLinks.class, Condition.ConditionResult.REVIEW);
				callAndContinueOnFailure(OpinValidateResponseMetaData.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(ValidateRequestedPermissionsAreNotWidened.class, Condition.ConditionResult.FAILURE);
			} else {
				callAndContinueOnFailure(EnsureResponseCodeWas422.class, Condition.ConditionResult.FAILURE);
				callAndContinueOnFailure(ConsentErrorMetaValidator.class, Condition.ConditionResult.FAILURE);
			}

		});
	}
}
