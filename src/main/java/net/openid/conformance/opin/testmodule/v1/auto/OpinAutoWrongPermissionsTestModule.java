package net.openid.conformance.opin.testmodule.v1.auto;

import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.testmodule.AbstractOpinWrongPermissionsTestModule;
import net.openid.conformance.opin.testmodule.support.AddAutoScope;
import net.openid.conformance.opin.testmodule.support.BuildAutoConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-auto-api-wrong-permissions-test",
	displayName = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test",
	summary = "Ensures API resource cannot be called with wrong permissions - there will be two browser interactions with this test\n" +
			"Creates a consent with all the permissions needed to access the auto API (“DAMAGES_AND_PEOPLE_AUTO_READ”, “DAMAGES_AND_PEOPLE_AUTO_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_AUTO_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_AUTO_CLAIM_READ”,  “RESOURCES_READ”)\n" +
		"\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API \n" +
		"\u2022 Calls GET auto “/” API \n" +
		"\u2022 Expects 201 - Fetches one of the Policy IDs returned \n" +
		"\u2022 Calls GET auto policy-Info API specifying an Policy ID \n" +
		"\u2022 Expects 200 - Validate all the fields \n" +
		"\u2022 Calls GET auto premium API specifying an Policy ID \n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Calls GET auto claim API specifying an Policy ID \n" +
		"\u2022 Expects 200- Validate all the fields \n" +
		"\u2022 Call the POST Consents API with either the customer’s business or the customer’s personal PERMISSIONS, depending on what option has been selected by the user on the configuration field " +
		"\u2022 Expects a success 201 - Expects a success on Redirect as well \n" +
		"\u2022 Calls GET auto “/” API \n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET auto policy-Info API specifying an Policy ID \n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET auto premium API specifying an Policy ID \n" +
		"\u2022 Expects a 403 response  - Validate error response \n" +
		"\u2022 Calls GET auto claim API specifying an Policy ID \n" +
		"\u2022 Expects a 403 response  - Validate error response\n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
			"server.discoveryUrl",
			"client.client_id",
			"client.jwks",
			"mtls.key",
			"mtls.cert",
			"resource.consentUrl",
			"resource.brazilCpf",
			"consent.productType"
	}
)
public class OpinAutoWrongPermissionsTestModule extends AbstractOpinWrongPermissionsTestModule {

	private static final String API = "insurance-auto";

	@Override
	protected void configureClient() {
		callAndStopOnFailure(BuildAutoConfigResourceUrlFromConsentUrl.class);
		super.configureClient();
	}
	@Override
	protected void prepareCorrectConsents(){
		callAndStopOnFailure(AddAutoScope.class);
		setApi(API);

		setRootValidator(OpinInsuranceAutoClaimValidatorV1.class);
		setPolicyInfoValidator(OpinInsuranceAutoPolicyInfoValidatorV1.class);
		setPremiumValidator(OpinInsuranceAutoPremiumValidatorV1.class);
		setClaimValidator(OpinInsuranceAutoClaimValidatorV1.class);

		permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
		permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_AUTO).build();
	}
}
