package net.openid.conformance.opin.testmodule.v1.rural;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.opin.testmodule.AbstractOpinApiTestModule;
import net.openid.conformance.opin.testmodule.support.AddFinancialRiskScope;
import net.openid.conformance.opin.testmodule.support.AddRuralScope;
import net.openid.conformance.opin.testmodule.support.BuildFinancialRiskConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.testmodule.support.OpinConsentPermissionsBuilder;
import net.openid.conformance.opin.testmodule.support.PermissionsGroup;

import net.openid.conformance.opin.testmodule.support.BuildRuralConfigResourceUrlFromConsentUrl;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralClaimListValidatorV1;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralListValidatorV1;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralPremiumValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;
import net.openid.conformance.variant.FAPI1FinalOPProfile;
import net.openid.conformance.variant.VariantHidesConfigurationFields;

@PublishTestModule(
        testName = "opin-rural-api-test",
        displayName = "Validates the structure of all rural API resources",
        summary = "Validates the structure of all rural API resources\n" +
                "\u2022 Creates a consent with all the permissions needed to access the rural API (“DAMAGES_AND_PEOPLE_RURAL_READ”, “DAMAGES_AND_PEOPLE_RURAL_POLICYINFO_READ”, “DAMAGES_AND_PEOPLE_RURAL_PREMIUM_READ”, “DAMAGES_AND_PEOPLE_RURAL_CLAIM_READ”,  “RESOURCES_READ”)\n" +
                "\u2022 Expects 201 - Expects Success on Redirect - Validates all of the fields sent on the consent API\n" +
                "\u2022 Calls GET rural “/” API\n" +
                "\u2022 Expects 200 - Fetches one of the Policy IDs returned\n" +
                "\u2022 Calls GET rural policy-Info API \n" +
                "\u2022 Expects 200 - Validate all the fields\n" +
                "\u2022 Calls GET rural premium API \n" +
                "\u2022 Expects 200- Validate all the fields\n" +
                "\u2022 Calls GET rural claim API \n" +
                "\u2022 Expects 200- Validate all the fields",
        profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
        configurationFields = {
                "server.discoveryUrl",
                "client.client_id",
                "client.jwks",
                "mtls.key",
                "mtls.cert",
                "resource.consentUrl",
                "resource.brazilCpf",
                "consent.productType"
        }
)
@VariantHidesConfigurationFields(parameter = FAPI1FinalOPProfile.class, value = "openbanking_brazil", configurationFields = {
        "client.org_jwks"
})
public class OpinRuralApiTestModule extends AbstractOpinApiTestModule {

    private static final String API = "insurance-rural";

    @Override
    protected void configureClient() {
        super.configureClient();
        callAndStopOnFailure(AddRuralScope.class);
        callAndStopOnFailure(BuildRuralConfigResourceUrlFromConsentUrl.class);
    }

    @Override
    protected void onConfigure(JsonObject config, String baseUrl) {
        OpinConsentPermissionsBuilder permissionsBuilder = new OpinConsentPermissionsBuilder(env,getId(),eventLog,testInfo,executionManager);
        permissionsBuilder.addPermissionsGroup(PermissionsGroup.DAMAGES_AND_PEOPLE_RURAL).build();

        setApi(API);
        setRootValidator(OpinInsuranceRuralListValidatorV1.class);
        setPolicyInfoValidator(OpinInsuranceRuralPolicyInfoValidatorV1.class);
        setPremiumValidator(OpinInsuranceRuralPremiumValidatorV1.class);
        setClaimValidator(OpinInsuranceRuralClaimListValidatorV1.class);
    }
}
