package net.openid.conformance.opin.testmodule.v1.structural;

import net.openid.conformance.condition.Condition;
import net.openid.conformance.openbanking_brasil.OBBProfile;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractNoAuthFunctionalTestModule;
import net.openid.conformance.openbanking_brasil.testmodules.support.ConsentIdExtractor;
import net.openid.conformance.openbanking_brasil.testmodules.support.DoNotStopOnFailure;
import net.openid.conformance.opin.testmodule.support.BuildResourceUrlFromStructuralResourceUrl;
import net.openid.conformance.opin.testmodule.support.PrepareToGetConsentsDetailsById;
import net.openid.conformance.opin.testmodule.support.PrepareToGetConsentsRoot;
import net.openid.conformance.opin.testmodule.support.ValidateOpinWellKnownUriSteps;
import net.openid.conformance.opin.testplan.utils.CallNoCacheResource;
import net.openid.conformance.opin.validator.consents.v1.OpinConsentDetailsIdentifiedByConsentIdValidatorV1;
import net.openid.conformance.opin.validator.consents.v1.OpinCreateNewConsentValidatorV1;
import net.openid.conformance.testmodule.PublishTestModule;

@PublishTestModule(
	testName = "opin-consents-api-structural-test",
	displayName = "Validate structure of Consents API Endpoint 200 response",
	summary = "\u2022 Call the “/\" endpoint - Expect 200 and validate the response \n"+
		"\u2022 Call the “/{consentId}\" endpoint - Expect 200 and validate response \n",
	profile = OBBProfile.OBB_PROFILE_OPEN_INSURANCE_PHASE2,
	configurationFields = {
		"server.discoveryUrl",
		"resource.resourceUrl"
	})
public class OpinConsentsStructuralTestModule extends AbstractNoAuthFunctionalTestModule {

	private final String API = "consents";

	@Override
	protected void runTests() {

		call(new ValidateOpinWellKnownUriSteps());

		env.putString("api_base", API);
		callAndStopOnFailure(BuildResourceUrlFromStructuralResourceUrl.class);

		runInBlock("Validate Consents - Root", () -> {
			callAndStopOnFailure(PrepareToGetConsentsRoot.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class);
			callAndContinueOnFailure(OpinCreateNewConsentValidatorV1.class, Condition.ConditionResult.FAILURE);
		});
		runInBlock("Validate Consents - Id", () -> {
			callAndContinueOnFailure(ConsentIdExtractor.class);
			callAndStopOnFailure(PrepareToGetConsentsDetailsById.class);
			callAndStopOnFailure(CallNoCacheResource.class);
			callAndContinueOnFailure(DoNotStopOnFailure.class);
			callAndContinueOnFailure(OpinConsentDetailsIdentifiedByConsentIdValidatorV1.class, Condition.ConditionResult.FAILURE);
		});
	}
}
