package net.openid.conformance.opin.testmodule.support;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.openbanking_brasil.testmodules.support.JsonHelper;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.testmodule.OIDFJSON;

public class EnsureTotalRecordsNumber extends AbstractJsonAssertingCondition {

	@Override
	public Environment evaluate(Environment env) {
		JsonElement apiResponse;

		String resourceEndpointResponse = env.getString("resource_endpoint_response");
		JsonObject consentEndpointResponse = env.getObject("consent_endpoint_response");
		Boolean isConsentsForced = env.getBoolean("force_consents_response");

		if (isConsentsForced != null && isConsentsForced) {
			apiResponse = consentEndpointResponse;
			env.putBoolean("force_consents_response", false);
		} else {
			if (!Strings.isNullOrEmpty(resourceEndpointResponse) && JsonHelper.ifExists(bodyFrom(env), "$.data")) {
				apiResponse = bodyFrom(env);
			} else {
				apiResponse = consentEndpointResponse;
			}
		}

		if (apiResponse == null) {
			throw error("Could not find API response in the environment");
		}

		Integer expectedTotalRecords = env.getInteger("expected_total_records");
		if (expectedTotalRecords == null) {
			throw error("expected_total_records not specified, condition could not proceed. Verify the development flow.");
		}

		int metaTotalRecords = 1;

		if (!JsonHelper.ifExists(apiResponse, "$.meta")) {
			throw error("meta field is missing in data");
		}

		if (!JsonHelper.ifExists(apiResponse, "$.meta.totalRecords")) {
			throw error("totalRecords is missing in meta");
		}

		metaTotalRecords = OIDFJSON.getInt(findByPath(apiResponse, "$.meta.totalRecords"));
		if (metaTotalRecords < expectedTotalRecords) {
			throw error(String.format("totalRecords should contain at least %d.", expectedTotalRecords),
				args("totaRecords", metaTotalRecords));
		}

		logSuccess(String.format("totalRecords has at least the specified amount of records of %d", expectedTotalRecords),
			args("totaRecords", metaTotalRecords));

		return env;
	}
}
