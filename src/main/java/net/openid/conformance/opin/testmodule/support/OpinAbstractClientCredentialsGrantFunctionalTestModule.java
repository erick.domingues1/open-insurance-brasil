package net.openid.conformance.opin.testmodule.support;

import com.google.gson.JsonObject;
import net.openid.conformance.openbanking_brasil.testmodules.AbstractClientCredentialsGrantFunctionalTestModule;
import net.openid.conformance.opin.testmodule.support.OpinInsertMtlsCa;

public abstract class OpinAbstractClientCredentialsGrantFunctionalTestModule extends AbstractClientCredentialsGrantFunctionalTestModule {

	@Override
	protected void preConfigure(JsonObject config, String baseUrl, String externalUrlOverride) {
		callAndContinueOnFailure(OpinInsertMtlsCa.class);
	}
}
