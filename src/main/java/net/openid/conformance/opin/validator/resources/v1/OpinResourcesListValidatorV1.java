package net.openid.conformance.opin.validator.resources.v1;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators.LinksValidator;
import net.openid.conformance.openbanking_brasil.testmodules.support.generalValidators.MetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.IntField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api Source: swagger/openinsurance/resources/v1/swagger-resources.yaml
 * Api endpoint: /
 * Api version: 1.2.0
 */

@ApiName("Resources List V1")
public class OpinResourcesListValidatorV1 extends AbstractJsonAssertingCondition {
	private static final Set<String> TYPE = SetUtils.createSet("CUSTOMERS_PERSONAL_IDENTIFICATIONS, CUSTOMERS_PERSONAL_QUALIFICATION, CUSTOMERS_PERSONAL_ADITTIONALINFO, CUSTOMERS_BUSINESS_IDENTIFICATIONS, CUSTOMERS_BUSINESS_QUALIFICATION, CUSTOMERS_BUSINESS_ADITTIONALINFO, CAPITALIZATION_TITLES, PENSION_RISK, DAMAGES_AND_PEOPLE_PATRIMONIAL, DAMAGES_AND_PEOPLE_RESPONSIBILITY, DAMAGES_AND_PEOPLE_TRANSPORT, DAMAGES_AND_PEOPLE_FINANCIAL_RISKS, DAMAGES_AND_PEOPLE_RURAL, DAMAGES_AND_PEOPLE_AUTO, DAMAGES_AND_PEOPLE_HOUSING, DAMAGES_AND_PEOPLE_PERSON, DAMAGES_AND_PEOPLE_ACCEPTANCE_AND_BRANCHES_ABROAD");
	private static final Set<String> STATUS = SetUtils.createSet("AVAILABLE, UNAVAILABLE, TEMPORARILY_UNAVAILABLE, PENDING_AUTHORISATION");
	private static final String PATTERN = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/resources\\/v\\d+)(\\/resources.*)?$";

	private final MetaValidator metaValidator;
	private final LinksValidator linksValidator;

	public OpinResourcesListValidatorV1() {
		metaValidator = new MetaValidator(this, true, true, false);
		linksValidator = new LinksValidator(this, PATTERN);
	}

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		assertField(body,
			new ObjectArrayField
				.Builder("data")
				.setValidator(this::assertInnerFields)
				.build());

		String requestUri = environment.getString("resource_endpoint_response");
		int expectedVersion = 1;
		metaValidator.assertMetaObject(body);
		linksValidator.assertLinksObject(body, requestUri, expectedVersion);
		logFinalStatus();
		return environment;
	}

	private void assertInnerFields(JsonObject identification) {
		assertField(identification,
			new StringField
				.Builder("resourceId")
				.setMinLength(1)
				.setMaxLength(100)
				.setPattern("^[a-zA-Z0-9][a-zA-Z0-9\\-]{0,99}$")
				.build());

		assertField(identification,
			new StringField
				.Builder("type")
				.setEnums(TYPE)
				.build());

		assertField(identification,
			new StringField
				.Builder("status")
				.setEnums(STATUS)
				.build());
	}
}
