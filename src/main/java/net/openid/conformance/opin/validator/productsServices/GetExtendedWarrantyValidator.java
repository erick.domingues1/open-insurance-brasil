package net.openid.conformance.opin.validator.productsServices;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.productsNServices.ProductNServicesCommonFields;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.*;

import java.util.Set;

/**
 * Api Source: swagger/openinsurance/productsServices/extended-warranty.yaml
 * Api endpoint: /extended-warranty/
 * Api version: 1.1.0
 * Git hash:
 */

@ApiName("ProductsServices Extended Warranty")
public class GetExtendedWarrantyValidator extends AbstractJsonAssertingCondition {
	private static class Fields extends ProductNServicesCommonFields {
	}

	public static final Set<String> COVERAGE = SetUtils.createSet("GARANTIA_ESTENDIDA_ORIGINAL, GARANTIA_ESTENDIDA_AMPLIADA, GARANTIA_ESTENDIDA_REDUZIDA, COMPLEMENTACAO_DE_GARANTIA, OUTRAS");
	public static final Set<String> TERM = SetUtils.createSet("ANUAL, ANUAL_INTERMITENTE, PLURIANUAL, PLURIANUAL_INTERMITENTE, MENSAL, MENSAL_INTERMITENTE, DIARIO, DIARIO_INTERMITENTE, OUTROS");
	public static final Set<String> TARGET_AUDIENCE = SetUtils.createSet("PESSOA_NATURAL, PESSOA_JURIDICA");
	public static final Set<String> SECURITY_TYPE = SetUtils.createSet("SMARTPHONE, NOTEBOOK, TABLET, EQUIPAMENTOS_PORTATEIS, ELETRODOMESTICOS_–_LINHA_BRANCA, ELETRODOMESTICOS_–_LINHA_MARROM, AUTOMOVEL, BICICLETA, BICICLETA_ELETRICA, EMPRESA, RESIDENCIA, OUTROS");
	public static final Set<String> SERVICES_PACKAGE = SetUtils.createSet("ATE_10_SERVICOS, ATE_20_SERVICOS, ACIMA_DE_20_SERVICOS, CUSTOMIZAVEL");
	public static final Set<String> TYPE_SIGNALING = SetUtils.createSet("GRATUITO, PAGO");
	public static final Set<String> CUSTOMER_SERVICES = SetUtils.createSet("REDE_REFERENCIADA, LIVRE_ESCOLHA");
	public static final Set<String> PAYMENT_METHOD = SetUtils.createSet("CARTAO_DE_CREDITO, CARTAO_DE_DEBITO, DEBITO_EM_CONTA_CORRENTE, DEBITO_EM_CONTA_POUPANCA, BOLETO_BANCARIO, PIX, CONSIGNACAO_EM_FOLHA_DE_PAGAMENTO, PONTOS_DE_PROGRAMA_DE_BENEFICIO, OUTROS");
	public static final Set<String> PAYMENT_TYPE = SetUtils.createSet(" A_VISTA, PARCELADO");
	public static final Set<String> CONTRACT_TYPE = SetUtils.createSet("COLETIVO, INDIVIDUAL");
	public static final Set<String> INSURED_PARTICIPATION = SetUtils.createSet( "FRANQUIA, POS, NAO_SE_APLICA");

	@Override
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body, new ObjectField
			.Builder("data")
			.setValidator(data -> assertField(data, new ObjectField
				.Builder("brand")
				.setValidator(brand -> {
					assertField(brand, Fields.name().setMaxLength(80).build());
					assertField(brand,
						new ObjectArrayField
							.Builder("companies")
							.setValidator(this::assertCompanies)
							.build());
				})
				.build())).build());

		String linksPattern = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/products-services\\/v\\d+)(\\/extended-warranty.*)?$";
		new OpenInsuranceLinksAndMetaValidator(this).assertMetaAndLinksWithPattern(body, linksPattern);
		logFinalStatus();
		return environment;
	}

	private void assertCompanies(JsonObject companies) {
		assertField(companies, Fields.name().setMaxLength(80).build());
		assertField(companies, Fields.cnpjNumber().setMaxLength(14).build());

		assertField(companies,
			new ObjectArrayField
				.Builder("products")
				.setValidator(this::assertProducts)
				.build());
	}

	private void assertProducts(JsonObject products) {
		assertField(products, Fields.name().setMaxLength(80).build());
		assertField(products, Fields.code().setMaxLength(100).build());

		assertField(products,
			new ObjectArrayField
				.Builder("coverages")
				.setValidator(this::assertCoverages)
				.build());

		assertField(products,
			new StringArrayField
				.Builder("securityType")
				.setEnums(SECURITY_TYPE)
				.build());

		assertField(products,
			new StringField
				.Builder("securityTypeOthers")
				.setOptional()
				.build());

		assertField(products,
			new ObjectField
				.Builder("assistanceServices")
				.setValidator(this::assertAssistanceServices)
				.build());

		assertField(products,
			new StringArrayField
				.Builder("customerServices")
				.setEnums(CUSTOMER_SERVICES)
				.build());

		assertField(products,
			new BooleanField
				.Builder("microinsurance")
				.build());

		assertField(products,
			new BooleanField
				.Builder("traits")
				.build());

		assertField(products,
			new ObjectArrayField.Builder("validity")
				.setValidator(validity -> {
					assertField(validity,
						new StringArrayField
							.Builder("term")
							.setEnums(TERM)
							.build());

					assertField(validity,
						new StringField
							.Builder("termOthers")
							.setMaxLength(100)
							.setOptional()
							.build());
				}).build());

		assertField(products,
			new ObjectArrayField
				.Builder("premiumPayment")
				.setValidator(this::assertPremiumPayment)
				.build());

		assertField(products,
			new ObjectField.Builder("termsAndConditions")
				.setValidator(termsAndConditions -> {
					assertField(termsAndConditions,
						new StringField
							.Builder("susepProcessNumber")
							.setOptional()
							.setMaxLength(20)
							.build());

					assertField(termsAndConditions,
						new StringField
							.Builder("definition")
							.setMaxLength(1024)
							.build());
				}).build());

		assertField(products,
			new ObjectField.Builder("minimumRequirements")
				.setValidator(minimumRequirements -> {
					assertField(minimumRequirements,
						new StringArrayField
							.Builder("contractType")
							.setEnums(CONTRACT_TYPE)
							.build());

					assertField(minimumRequirements,
						new StringField
							.Builder("minimumRequirementDetails")
							.setMaxLength(1024)
							.build());

					assertField(minimumRequirements,
						new StringArrayField
							.Builder("targetAudiences")
							.setEnums(TARGET_AUDIENCE)
							.build());
				}).build());
	}

	private void assertCoverages(JsonObject coverages) {
		assertField(coverages,
			new StringField
				.Builder("coverage")
				.setEnums(COVERAGE)
				.build());

		assertField(coverages,
			new StringField
				.Builder("coverageDescription")
				.setMaxLength(3000)
				.build());

		assertField(coverages,
			new ObjectField
				.Builder("coverageAttributes")
				.setValidator(this::assertCoverageAttributes)
				.setOptional()
				.build());

		assertField(coverages,
			new BooleanField
				.Builder("allowApartPurchase")
				.build());
	}

	private void assertAssistanceServices(JsonObject assistanceServices) {
		assertField(assistanceServices,
			new BooleanField
				.Builder("assistanceServices")
				.build());

		assertField(assistanceServices,
			new StringArrayField
				.Builder("assistanceServicesPackage")
				.setEnums(SERVICES_PACKAGE)
				.build());

		assertField(assistanceServices,
			new StringField
				.Builder("complementaryAssistanceServicesDetail")
				.setMaxLength(1000)
				.build());

		assertField(assistanceServices,
			new StringField
				.Builder("chargeTypeSignaling")
				.setEnums(TYPE_SIGNALING)
				.build());
	}

	private void assertPremiumPayment(JsonObject premiumPayment) {
		assertField(premiumPayment,
			new StringField
				.Builder("paymentMethod")
				.setEnums(PAYMENT_METHOD)
				.build());

		assertField(premiumPayment,
			new StringField
				.Builder("paymentDetail")
				.setMaxLength(1024)
				.setOptional()
				.build());

		assertField(premiumPayment,
			new StringArrayField
				.Builder("paymentType")
				.setEnums(PAYMENT_TYPE)
				.build());

		assertField(premiumPayment,
			new StringField
				.Builder("premiumRates")
				.setMaxLength(1024)
				.setOptional()
				.build());
	}

	private void assertCoverageAttributes(JsonObject coverageAttributes) {
		assertField(coverageAttributes,
				new ObjectField
						.Builder("maxLMI")
						.setValidator(assertValue -> {
							assertField(assertValue,
									new StringField
											.Builder("type")
											.setEnums(SetUtils.createSet("FINANCEIRO, PERCENTUAL"))
											.build());

							assertField(assertValue,
									new ObjectField
											.Builder("amount")
											.setValidator(this::assertAmount)
											.build());

							assertField(assertValue,
									new StringField
											.Builder("index")
											.setEnums(SetUtils.createSet("LMG, FINANCEIRO_COBERTURA, OUTRO"))
											.setOptional()
											.build());

							assertField(assertValue,
									new ObjectField
											.Builder("maxIndexAmount")
											.setValidator(this::assertAmount)
											.setOptional()
											.build());

						})
						.build());

		assertField(coverageAttributes,
			new StringArrayField
				.Builder("insuredParticipation")
				.setEnums(INSURED_PARTICIPATION)
				.build());

		assertField(coverageAttributes,
			new StringField
				.Builder("insuredParticipationDescription")
				.setMaxLength(1024)
				.setOptional()
				.build());
	}

	private void assertAmount(JsonObject minValue) {
		assertField(minValue,
			new NumberField
				.Builder("amount")
				.build());

		assertField(minValue,
			new ObjectField
				.Builder("unit")
				.setValidator(this::assertUnit)
				.build());
	}

	private void assertUnit(JsonObject unit) {
		assertField(unit,
			new StringField
				.Builder("code")
				.build());

		assertField(unit,
			new StringField
				.Builder("description")
				.build());
	}
}
