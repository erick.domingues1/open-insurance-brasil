package net.openid.conformance.opin.validator.consents.v1;

import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.validator.consents.v1.OpinConsentDetailsIdentifiedByConsentIdValidatorV1;

/**
 * This class corresponds to {@link net.openid.conformance.opin.validator.consents.v1.OpinConsentDetailsIdentifiedByConsentIdValidatorV1}
 * Api url: swagger/openinsurance/consents/v1/swagger-consents-api-v1.yaml
 * Api endpoint: /
 * Api version: 1.04
 **/
@ApiName("Create New Consent V1")
public class OpinCreateNewConsentValidatorV1 extends OpinConsentDetailsIdentifiedByConsentIdValidatorV1 {

}
