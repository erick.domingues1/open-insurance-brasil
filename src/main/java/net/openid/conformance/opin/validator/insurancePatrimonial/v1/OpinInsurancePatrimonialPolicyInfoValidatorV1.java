package net.openid.conformance.opin.validator.insurancePatrimonial.v1;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.*;

import java.util.Set;

/**
 * Api Source: swagger/openinsurance/insurancePatrimonial/v1/swagger-insurance-patrimonial.yaml
 * Api endpoint: /{policyId}/policy-info
 * Api version: 1.3.0
 */

@ApiName("Insurance Patrimonial PolicyInfo V1")
public class OpinInsurancePatrimonialPolicyInfoValidatorV1 extends AbstractJsonAssertingCondition {

	public static final Set<String> DOCUMENT_TYPE = SetUtils.createSet("APOLICE_INDIVIDUAL, BILHETE, CERTIFICADO, APOLICE_INDIVIDUAL_AUTOMOVEL, APOLICE_FROTA_AUTOMOVEL, CERTIFICADO_AUTOMOVEL");
	public static final Set<String> ISSUANCE_TYPE = SetUtils.createSet("EMISSAO_PROPRIA, COSSEGURO_ACEITO");
	public static final Set<String> IDENTIFICATION_TYPE = SetUtils.createSet("CPF, CNPJ, OUTROS");
	public static final Set<String> INTERMEDIARIES_TYPE = SetUtils.createSet("CORRETOR, REPRESENTANTE, ESTIPULANTE_AVERBADOR_INSTITUIDOR, CORRESPONDENTE, AGENTE_DE_MICROSSEGUROS, OUTROS");
	public static final Set<String> INSURED_OBJECTS_TYPE = SetUtils.createSet("CONTRATO, PROCESSO_ADMINISTRATIVO, PROCESSO_JUDICIAL, AUTOMOVEL, CONDUTOR, FROTA, PESSOA, OUTROS");
	public static final Set<String> GRACE_PERIODICITY = SetUtils.createSet("DIA, MES, ANO");
	public static final Set<String> GRACE_PERIOD_COUNTING_METHOD = SetUtils.createSet("DIAS_UTEIS, DIAS_CORRIDOS");
	public static final Set<String> CODE = SetUtils.createSet("IMOVEL_BASICA, IMOVEL_AMPLA, DANOS_ELETRICOS, DANOS_POR_AGUA, ALAGAMENTO, RESPONSABILIDADE_CIVIL_FAMILIAR, RESPONSABILIDADE_CIVIL_DANOS_MORAIS, ROUBO_SUBTRACAO_BENS, ROUBO_SUBTRACAO_BENS_FORA_LOCAL_SEGURADO, TACOS_GOLFE_HOLE_ONE, PEQUENAS_REFORMAS_OBRAS, GREVES_TUMULTOS_LOCKOUT, MICROEMPREENDEDOR, ESCRITORIO_RESIDENCIA, DANOS_EQUIPAMENTOS_ELETRONICOS, QUEBRA_VIDROS, IMPACTO_VEICULOS, VENDAVAL, PERDA_PAGAMENTO_ALUGUEL, BICICLETA, RESPONSABILIDADE_CIVIL_BICICLETA, RC_EMPREGADOR, DESMORONAMENTO, DESPESAS_EXTRAORDINARIAS, JOIAS_OBRAS_ARTE, TERREMOTO, IMPACTO_AERONAVES, PAISAGISMO, INCENDIO, QUEDA_RAIO, EXPLOSAO, COBERTURA_BASICA_AMPLA_COBERTURAS_PARA_QUAISQUER_EVENTOS_QUE_POSSAM_CAUSAR_DANOS_FISICOS_AO_IMOVEL_SEGURADO_EXCETO_OS_EXPRESSAMENTE_EXCLUIDOS, COBERTURA_BASICA_SIMPLES_COBERTURAS_DE_INCENDIO_QUEDA_DE_RAIO_DENTRO_DO_TERRENO_SEGURADO_E_EXPLOSAO_DE_QUALQUER_NATUREZA, ANUNCIOS_LUMINOSOS, DANOS_AO_JARDIM, DESPESAS_COM_ALUGUEL, EQUIPAMENTOS, FIDELIDADE_DE_EMPREGADOS, IMPACTO_DE_VEICULOS, VIDA_E_ACIDENTES_PESSOAIS_EMPREGADOS, LUCROS_CESSANTES, QUEBRA_DE_VIDROS_ESPELHOS_MARMORES_E_GRANITOS, RESPONSABILIDADE_CIVIL, ROUBO, VALORES, VAZAMENTO, TUMULTO, INCENDIO_QUEDA_DE_RAIO_E_EXPLOSAO, VENDAVAL_ATE_FUMACA, ALAGAMENTO_E_INUNDACAO, TUMULTOS_GREVES_LOCKOUT_E_ATOS_DOLOSOS, ROUBO_E_FURTO_QUALIFICADO, RECOMPOSICAO_DE_REGISTROS_E_DOCUMENTOS, DETERIORACAO_DE_MERCADORIAS_EM_AMBIENTES_FRIGORIFICADOS, DERRAME, VAZAMENTO_, QUEBRA_DE_MAQUINAS, LUCROS_CESSANTES_DESPESAS_FIXAS_LUCRO_LIQUIDO_LUCRO_BRUTO, PERDA_OU_PAGAMENTO_DE_ALUGUEL, PEQUENAS_OBRAS_DE_ENGENHARIA, DANOS_DE_CAUSA_EXTERNA, DANOS_DE_CAUSA_EXTERNA_E_ROUBO, SERVICOS_EMERGENCIAIS, SERVICOS_DE_CONVENIENCIA, GARANTIA_ESTENDIDA_ORIGINAL, GARANTIA_ESTENDIDA_AMPLIADA, GARANTIA_ESTENDIDA_REDUZIDA, COMPLEMENTACAO_DE_GARANTIA, LUCRO_BRUTO, LUCRO_LIQUIDO, DESPESAS_FIXAS, PERDA_DE_RECEITA_OU_INTERRUPCAO_DE_NEGOCIOS, OBRAS_CIVIS_CONSTRUCAO_E_INSTALACAO_E_MONTAGEM, AFRETAMENTOS_DE_AERONAVES, ARMAZENAGEM_FORA_DO_CANTEIRO_DE_OBRAS_OU_LOCAL_SEGURADO, DANOS_EM_CONSEQUENCIA_DE_ERRO_DE_PROJETO_RISCO_DO_FABRICANTE, DANOS_MORAIS, DESPESAS_COM_DESENTULHO_DO_LOCAL, DESPESAS_DE_SALVAMENTO_E_CONTENCAO_DE_SINISTROS, EQUIPAMENTOS_DE_ESCRITORIO_E_INFORMATICA, EQUIPAMENTOS_MOVEIS_ESTACIONARIOS_UTILIZADOS_NA_OBRA, FERRAMENTAS_DE_PEQUENO_E_MEDIO_PORTE, HONORARIOS_DE_PERITO, INCENDIO_APOS_O_TERMINO_DE_OBRAS_ATE_30_DIAS_EXCETO_PARA_REFORMAS_AMPLIACOES, MANUTENCAO_AMPLA_ATE_24_MESES, MANUTENCAO_SIMPLES_ATE_24_MESES, OBRAS_CONCLUIDAS, OBRAS_TEMPORARIAS, OBRAS_INSTALACOES_CONTRATADAS_ACEITAS_E_OU_COLOCADAS_EM_OPERACAO, PROPRIEDADES_CIRCUNVIZINHAS, RECOMPOSICAO_DE_DOCUMENTOS, RESPONSABILIDADE_CIVIL_EMPREGADOR, FUNDACAO, STANDS_DE_VENDA, TRANSPORTE_TERRESTRE, TUMULTOS_GREVES_E_LOCKOUT, DANOS_MATERIAIS_CAUSADOS_AO_COFRE_FORTE, DANOS_MATERIAIS_CAUSADOS_AOS_CAIXAS_ELETRONICOS_ATM, INFIDELIDADE_DE_FUNCIONARIOS, VALORES_NO_INTERIOR_DO_ESTABELECIMENTO_DENTRO_E_OU_FORA_DE_COFRE_FORTE, VALORES_NO_INTERIOR_DE_CAIXAS_ELETRONICOS_ATM, VALORES_EM_MAOS_DE_PORTADORES_EM_TRANSITO, ALAGAMENTO_INUNDACAO, ALUGUEL_PERDA_OU_PAGAMENTO, BAGAGEM, BASICA_INCENDIO_RAIO_EXPLOSAO, BASICA_DANOS_MATERIAIS, BASICA_DE_OBRAS_CIVIS_EM_CONSTRUCAO_E_INSTALACOES_E_MONTAGENS, BENS_DE_TERCEIROS_EM_PODER_DO_SEGURADO, CARGA_DESCARGA_ICAMENTO_E_DESCIDA, DANOS_NA_FABRICACAO, DERRAME_D_AGUA_OU_OUTRA_SUBSTANCIA_LIQUIDA_DE_INSTALACOES_DE_CHUVEIROS_AUTOMATICOS, DESPESAS_ADICIONAIS_OUTRAS_DESPESAS, DESPESAS_FIXA, DETERIORACAO_DE_MERCADORIAS_EM_AMBIENTES, FRIGORIFICADOS, EQUIPAMENTOS_ARRENDADOS, EQUIPAMENTOS_CEDIDOS_A_TERCEIROS, EQUIPAMENTOS_CINEMATOGRAFICOS_FOTOGRAFICOS_DE_AUDIO_E_VIDEO, EQUIPAMENTOS_DIVERSOS_OUTRAS_MODALIDADES, EQUIPAMENTOS_ELETRONICOS, EQUIPAMENTOS_ESTACIONARIOS, EQUIPAMENTOS_MOVEIS, EQUIPAMENTOS_PORTATEIS, HONORARIOS_DE_PERITOS, IMPACTO_DE_VEICULOS_E_QUEDA_DE_AERONAVES, IMPACTO_DE_VEICULOS_TERRESTRES, LINHAS_DE_TRANSMISSAO_E_DISTRIBUICAO, MOVIMENTACAO_INTERNA_DE_MERCADORIAS, PATIOS, ROUBO_DE_BENS_DE_HOSPEDES, ROUBO_DE_VALORES_EM_TRANSITO_EM_MAOS_DE_PORTADOR, ROUBO_E_FURTO_MEDIANTE_ARROMBAMENTO, ROUBO_E_OU_FURTO_QUALIFICADO_DE_VALORES_NO_INTERIOR_DO_ESTABELECIMENTO_DENTRO_E_OU_FORA_DE_COFRES_FORTES_OU_CAIXAS_FORTES, TERRORISMO_E_SABOTAGEM, VAZAMENTO_DE_TUBULACOES_E_TANQUES, VAZAMENTO_DE_TUBULACOES_HIDRAULICAS, VENDAVAL_FURACAO_CICLONE_TORNADO_GRANIZO_QUEDA_DE_AERONAVES_OU_QUAISQUER_OUTROS_ENGENHOS_AEREOS_OU_ESPACIAIS_IMPACTO_DE_VEICULOS_TERRESTRES_E_FUMACA, OUTRAS");
	public static final Set<String> FEATURE = SetUtils.createSet("MASSIFICADOS, MASSIFICADOS_MICROSEGUROS, GRANDES_RISCOS");
	public static final Set<String> TYPE = SetUtils.createSet("PARAMETRICO, INTERMITENTE, REGULAR_COMUM, CAPITAL_GLOBAL, PARAMETRICO_E_INTERMITENTE");
	public static final Set<String> DEDUCTIBLE_TYPE = SetUtils.createSet("REDUZIDA, NORMAL, MAJORADA, DEDUTIVEL, OUTROS");
	public static final Set<String> APPLICATION_TYPE = SetUtils.createSet("VALOR, PERCENTUAL, OUTROS");
	public static final Set<String> BASIC_COVERAGE_INDEX = SetUtils.createSet("SIMPLES, AMPLA");
	public static final Set<String> PROPERTY_TYPE = SetUtils.createSet("CASA, APARTAMENTO, CONDOMINIO_RESIDENCIAL, CONDOMINIO_COMERCIAL, CONDOMINIO_MISTOS");
	public static final Set<String> STRUCTURING_TYPE = SetUtils.createSet("CONDOMINIO_VERTICAL, CONDOMINIO_HORIZONTAL, MISTO");

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		assertField(body,
			new ObjectField
				.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.build());

		String linksPattern = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/insurance-patrimonial\\/v\\d+)(\\/insurance-patrimonial.*)?$";
		String requestUri = environment.getString("protected_resource_url");
		new OpenInsuranceLinksAndMetaValidator(this).assertMetaAndLinksWithPattern(body, requestUri, linksPattern);
		logFinalStatus();
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("documentType")
				.setEnums(DOCUMENT_TYPE)
				.build());

		assertField(data,
			new StringField
				.Builder("policyId")
				.setMaxLength(60)
				.build());

		assertField(data,
			new StringField
				.Builder("susepProcessNumber")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("groupCertificateId")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("issuanceType")
				.setEnums(ISSUANCE_TYPE)
				.build());

		assertField(data,
			new StringField
				.Builder("issuanceDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("termStartDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("termEndDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("leadInsurerCode")
				.setMaxLength(1024)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("leadInsurerPolicyId")
				.setMaxLength(1024)
				.setOptional()
				.build());

		assertField(data,
			new ObjectField
				.Builder("maxLMG")
				.setValidator(this::assertAmount)
				.build());

		assertField(data,
			new StringField
				.Builder("proposalId")
				.setMaxLength(60)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("insureds")
				.setValidator(this::assertPersonalInfo)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("beneficiaries")
				.setValidator(this::assertBeneficiaryInfo)
				.setOptional()
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("principals")
				.setValidator(this::assertPersonalInfo)
				.setOptional()
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("intermediaries")
				.setValidator(this::assertIntermediaries)
				.setOptional()
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("insuredObjects")
				.setValidator(this::assertInsuredObjects)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("coverages")
				.setValidator(this::assertCoverages)
				.setMinProperties(3)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("coinsuranceRetainedPercentage")
				.setPattern("^\\d{1,3}\\.\\d{1,9}$")
				.setOptional()
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("coinsurers")
				.setValidator(coinsurers -> {
					assertField(coinsurers,
						new StringField
							.Builder("identification")
							.setMaxLength(60)
							.build());

					assertField(coinsurers,
						new StringField
							.Builder("cededPercentage")
							.setPattern("^\\d{1,3}\\.\\d{1,9}$")
							.build());
				})
				.setOptional()
				.build());

		assertField(data,
			new ObjectField
				.Builder("branchInfo")
				.setValidator(branchInfo -> {
					assertField(branchInfo,
						new StringField
							.Builder("basicCoverageIndex")
							.setMaxLength(7)
							.setEnums(BASIC_COVERAGE_INDEX)
							.setOptional()
							.build());

					assertField(branchInfo,
						new ObjectArrayField
							.Builder("insuredObjects")
							.setValidator(this::assertSpecificInsuredObjects)
							.setOptional()
							.build());
				})
				.setOptional()
				.build());
	}

	private void assertPersonalInfo(JsonObject insureds) {
		assertField(insureds,
			new StringField
				.Builder("identification")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("identificationType")
				.setEnums(IDENTIFICATION_TYPE)
				.build());

		assertField(insureds,
			new StringField
				.Builder("name")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("postCode")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("email")
				.setMaxLength(256)
				.setOptional()
				.build());

		assertField(insureds,
			new StringField
				.Builder("city")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("state")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("country")
				.setMaxLength(3)
				.setPattern("^(\\w{3}){1}$")
				.build());

		assertField(insureds,
			new StringField
				.Builder("address")
				.setMaxLength(60)
				.build());
	}
	private void assertBeneficiaryInfo(JsonObject insureds) {
		assertField(insureds,
			new StringField
				.Builder("identification")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("identificationType")
				.setEnums(IDENTIFICATION_TYPE)
				.build());

		assertField(insureds,
			new StringField
				.Builder("name")
				.setMaxLength(60)
				.build());
	}

	private void assertIntermediaries(JsonObject intermediaries) {
		assertField(intermediaries,
			new StringField
				.Builder("type")
				.setEnums(INTERMEDIARIES_TYPE)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("identification")
				.setMaxLength(60)
				.setPattern("^\\d{1,60}$")
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("brokerId")
				.setMaxLength(100)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("identificationType")
				.setEnums(IDENTIFICATION_TYPE)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("name")
				.setMaxLength(60)
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("postCode")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("city")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("state")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("country")
				.setMaxLength(3)
				.setPattern("^(\\w{3}){1}$")
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("address")
				.setMaxLength(60)
				.setOptional()
				.build());
	}

	private void assertInsuredObjects(JsonObject insuredObjects) {
		assertField(insuredObjects,
			new StringField
				.Builder("identification")
				.setMaxLength(100)
				.build());

		assertField(insuredObjects,
			new StringField
				.Builder("type")
				.setEnums(INSURED_OBJECTS_TYPE)
				.build());

		assertField(insuredObjects,
			new StringField
				.Builder("typeAdditionalInfo")
				.setMaxLength(100)
				.setOptional()
				.build());

		assertField(insuredObjects,
			new StringField
				.Builder("description")
				.setMaxLength(1024)
				.build());

		assertField(insuredObjects,
			new ObjectField
				.Builder("amount")
				.setValidator(this::assertAmount)
				.setOptional()
				.build());

		assertField(insuredObjects,
			new ObjectArrayField
				.Builder("coverages")
				.setValidator(this::assertInsuredObjectsCoverages)
				.build());
	}

	private void assertSpecificInsuredObjects(JsonObject insuredObjects) {
		assertField(insuredObjects,
			new StringField
				.Builder("identification")
				.setMaxLength(100)
				.build());

		assertField(insuredObjects,
			new StringField
				.Builder("propertyType")
				.setMaxLength(22)
				.setEnums(PROPERTY_TYPE)
				.setOptional()
				.build());

		assertField(insuredObjects,
			new StringField
				.Builder("structuringType")
				.setMaxLength(21)
				.setEnums(STRUCTURING_TYPE)
				.setOptional()
				.build());

		assertField(insuredObjects,
			new StringField
				.Builder("postCode")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(insuredObjects,
			new StringField
				.Builder("businessActivity")
				.setMaxLength(7)
				.setPattern("^\\d{7}$")
				.setOptional()
				.build());
	}

	private void assertInsuredObjectsCoverages(JsonObject coverages) {
		assertField(coverages,
			new StringField
				.Builder("branch")
				.setMaxLength(4)
				.build());

		assertField(coverages,
			new StringField
				.Builder("code")
				.setEnums(CODE)
				.build());

		assertField(coverages,
			new StringField
				.Builder("description")
				.setMaxLength(500)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("internalCode")
				.setMaxLength(500)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("susepProcessNumber")
				.setMaxLength(50)
				.build());

		assertField(coverages,
			new ObjectField
				.Builder("LMI")
				.setValidator(this::assertAmount)
				.build());

		assertField(coverages,
			new BooleanField
				.Builder("isLMISublimit")
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("termStartDate")
				.setMaxLength(10)
				.build());

		assertField(coverages,
			new StringField
				.Builder("termEndDate")
				.setMaxLength(10)
				.build());

		assertField(coverages,
			new BooleanField
				.Builder("isMainCoverage")
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("feature")
				.setEnums(FEATURE)
				.build());

		assertField(coverages,
			new StringField
				.Builder("type")
				.setEnums(TYPE)
				.build());

		assertField(coverages,
			new IntField
				.Builder("gracePeriod")
				.setMaxLength(5)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("gracePeriodicity")
				.setEnums(GRACE_PERIODICITY)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("gracePeriodCountingMethod")
				.setEnums(GRACE_PERIOD_COUNTING_METHOD)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("gracePeriodStartDate")
				.setMaxLength(10)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("gracePeriodEndDate")
				.setMaxLength(10)
				.setOptional()
				.build());
	}

	private void assertCoverages(JsonObject coverages) {
		assertField(coverages,
			new StringField
				.Builder("branch")
				.setMaxLength(4)
				.build());

		assertField(coverages,
			new StringField
				.Builder("code")
				.setEnums(CODE)
				.build());

		assertField(coverages,
			new StringField
				.Builder("description")
				.setMaxLength(500)
				.setOptional()
				.build());

		assertField(coverages,
			new ObjectField
				.Builder("deductible")
				.setValidator(this::assertDeductible)
				.setOptional()
				.build());

		assertField(coverages,
			new ObjectField
				.Builder("POS")
				.setValidator(this::assertPOS)
				.setOptional()
				.build());
	}

	private void assertDeductible(JsonObject deductible) {
		assertField(deductible,
			new StringField
				.Builder("type")
				.setEnums(DEDUCTIBLE_TYPE)
				.build());

		assertField(deductible,
			new StringField
				.Builder("typeAdditionalInfo")
				.setMaxLength(500)
				.setOptional()
				.build());

		assertField(deductible,
			new ObjectField
				.Builder("amount")
				.setValidator(this::assertAmount)
				.build());

		assertField(deductible,
			new IntField
				.Builder("period")
				.setMaxLength(5)
				.build());

		assertField(deductible,
			new StringField
				.Builder("periodicity")
				.setEnums(GRACE_PERIODICITY)
				.build());

		assertField(deductible,
			new StringField
				.Builder("periodCountingMethod")
				.setOptional()
				.setEnums(GRACE_PERIOD_COUNTING_METHOD)
				.build());

		assertField(deductible,
			new StringField
				.Builder("periodStartDate")
				.build());

		assertField(deductible,
			new StringField
				.Builder("periodEndDate")
				.build());

		assertField(deductible,
			new StringField
				.Builder("description")
				.setMaxLength(60)
				.build());
	}

	private void assertPOS(JsonObject pOS) {
		assertField(pOS,
			new StringField
				.Builder("applicationType")
				.setEnums(APPLICATION_TYPE)
				.build());

		assertField(pOS,
			new StringField
				.Builder("description")
				.setMaxLength(60)
				.build());

		assertField(pOS,
			new ObjectField
				.Builder("minValue")
				.setValidator(this::assertAmount)
				.setOptional()
				.build());

		assertField(pOS,
			new ObjectField
				.Builder("maxValue")
				.setValidator(this::assertAmount)
				.setOptional()
				.build());

		assertField(pOS,
			new StringField
				.Builder("percentage")
				.setPattern("^\\d{1,3}\\.\\d{1,9}$")
				.setOptional()
				.build());
	}

	private void assertAmount(JsonObject minValue) {
		assertField(minValue,
				new StringField
						.Builder("amount")
						.setPattern("^\\d{1,16}\\.\\d{2}$")
						.build());

		assertField(minValue,
				new ObjectField
						.Builder("unit")
						.setValidator(this::assertUnit)
						.build());
	}

	private void assertUnit(JsonObject unit) {
		assertField(unit,
				new StringField
						.Builder("code")
						.setMaxLength(2)
						.build());

		assertField(unit,
				new StringField
						.Builder("description")
						.setPattern("^(\\w{3})$")
						.build());
	}
}
