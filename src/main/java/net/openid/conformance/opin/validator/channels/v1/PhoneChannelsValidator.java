package net.openid.conformance.opin.validator.channels.v1;

import com.google.common.collect.Sets;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.CommonFields;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.opin.validator.channels.v1.ChannelsCommonParts;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api source: swagger/openinsurance/channels/v1/swagger-channels.yaml
 * Api endpoint: /phone-channels
 * Api version: 1.3.0
 * Api git hash:
 */

@ApiName("Phone Channels")
public class PhoneChannelsValidator extends AbstractJsonAssertingCondition {

	public static final Set<String> IDENTIFICATION_TYPES = Sets.newHashSet("CENTRAL_TELEFONICA", "SAC", "OUVIDORIA");
	private final net.openid.conformance.opin.validator.channels.v1.ChannelsCommonParts parts;

	public PhoneChannelsValidator() {
		parts = new ChannelsCommonParts(this);
	}
	private static class Fields extends CommonFields {
	}

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		assertField(body, new ObjectField.Builder(ROOT_PATH).setValidator(
			data -> assertField(data, new ObjectField.Builder("brand").setValidator(
				brand -> {
					assertField(brand, Fields.name().build());
					assertField(brand,
						new ObjectArrayField.Builder("companies")
							.setMinItems(1)
							.setValidator(this::assertCompanies)
							.build());}
			).build())
		).build());

		String linksPattern = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/channels\\/v\\d+)(\\/(branches|electronic-channels|phone-channels).*)?$";
		new OpenInsuranceLinksAndMetaValidator(this).assertMetaAndLinksWithPattern(body, linksPattern);
		logFinalStatus();
		return environment;
	}

	private void assertCompanies(JsonObject companies) {
		assertField(companies, Fields.name().build());
		assertField(companies, Fields.cnpjNumber().setMinLength(14).build());

		assertField(companies,
			new ObjectArrayField
				.Builder("phoneChannels")
				.setValidator(this::assertPhoneChannels)
				.setMinItems(1)
				.build());
	}

	private void assertPhoneChannels(JsonObject phoneChannels) {
		assertField(phoneChannels,
			new ObjectField
				.Builder("identification")
				.setValidator(this::assertIdentification)
				.build());

		parts.assertCommonServices(phoneChannels);
		parts.assertAvailability(phoneChannels);
	}

	private void assertIdentification(JsonObject identification) {
		assertField(identification,
			new StringField
				.Builder("type")
				.setEnums(IDENTIFICATION_TYPES)
				.build());

		assertField(identification,
			new ObjectArrayField
				.Builder("phones")
				.setValidator(this::assertPhoneChannelsPhones)
				.setMinItems(1)
				.setOptional()
				.build());
	}

	private void assertPhoneChannelsPhones(JsonObject phones) {
		assertField(phones,
			new StringField
				.Builder("countryCallingCode")
				.setMaxLength(4)
				.setPattern("^\\d{1,4}$|^NA$")
				.build());

		assertField(phones,
			new StringField
				.Builder("areaCode")
				.setMaxLength(2)
				.setPattern("^\\d{2}$|^NA$")
				.build());

		assertField(phones,
			new StringField
				.Builder("number")
				.setMaxLength(13)
				.setPattern("^([0-9]{8,11})$|^NA$")
				.build());
	}
}
