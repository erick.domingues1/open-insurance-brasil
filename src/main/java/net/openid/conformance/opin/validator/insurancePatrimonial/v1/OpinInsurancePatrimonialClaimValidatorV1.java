package net.openid.conformance.opin.validator.insurancePatrimonial.v1;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api Source: swagger/openinsurance/insurancePatrimonial/v1/swagger-insurance-patrimonial.yaml
 * Api endpoint: /{policyId}/claim
 * Api version: 1.3.0
 */

@ApiName("Insurance Patrimonial Claim V1")
public class OpinInsurancePatrimonialClaimValidatorV1 extends AbstractJsonAssertingCondition {
	public static final Set<String> STATUS = SetUtils.createSet("ABERTO, ENCERRADO_COM_INDENIZACAO, ENCERRADO_SEM_INDENIZACAO, REABERTO, CANCELADO_POR_ERRO_OPERACIONAL, AVALIACAO_INICIAL");
	public static final Set<String> JUSTIFICATION = SetUtils.createSet("RISCO_EXCLUIDO, RISCO_AGRAVADO, SEM_DOCUMENTACAO, DOCUMENTACAO_INCOMPLETA, PRESCRICAO, FORA_COBERTURA, OUTROS");
	public static final Set<String> CODE = SetUtils.createSet("IMOVEL_BASICA, IMOVEL_AMPLA, DANOS_ELETRICOS, DANOS_POR_AGUA, ALAGAMENTO, RESPONSABILIDADE_CIVIL_FAMILIAR, RESPONSABILIDADE_CIVIL_DANOS_MORAIS, ROUBO_SUBTRACAO_BENS, ROUBO_SUBTRACAO_BENS_FORA_LOCAL_SEGURADO, TACOS_GOLFE_HOLE_ONE, PEQUENAS_REFORMAS_OBRAS, GREVES_TUMULTOS_LOCKOUT, MICROEMPREENDEDOR, ESCRITORIO_RESIDENCIA, DANOS_EQUIPAMENTOS_ELETRONICOS, QUEBRA_VIDROS, IMPACTO_VEICULOS, VENDAVAL, PERDA_PAGAMENTO_ALUGUEL, BICICLETA, RESPONSABILIDADE_CIVIL_BICICLETA, RC_EMPREGADOR, DESMORONAMENTO, DESPESAS_EXTRAORDINARIAS, JOIAS_OBRAS_ARTE, TERREMOTO, IMPACTO_AERONAVES, PAISAGISMO, INCENDIO, QUEDA_RAIO, EXPLOSAO, COBERTURA_BASICA_AMPLA_COBERTURAS_PARA_QUAISQUER_EVENTOS_QUE_POSSAM_CAUSAR_DANOS_FISICOS_AO_IMOVEL_SEGURADO_EXCETO_OS_EXPRESSAMENTE_EXCLUIDOS, COBERTURA_BASICA_SIMPLES_COBERTURAS_DE_INCENDIO_QUEDA_DE_RAIO_DENTRO_DO_TERRENO_SEGURADO_E_EXPLOSAO_DE_QUALQUER_NATUREZA, ANUNCIOS_LUMINOSOS, DANOS_AO_JARDIM, DESPESAS_COM_ALUGUEL, EQUIPAMENTOS, FIDELIDADE_DE_EMPREGADOS, IMPACTO_DE_VEICULOS, VIDA_E_ACIDENTES_PESSOAIS_EMPREGADOS, LUCROS_CESSANTES, QUEBRA_DE_VIDROS_ESPELHOS_MARMORES_E_GRANITOS, RESPONSABILIDADE_CIVIL, ROUBO, VALORES, VAZAMENTO, TUMULTO, INCENDIO_QUEDA_DE_RAIO_E_EXPLOSAO, VENDAVAL_ATE_FUMACA, ALAGAMENTO_E_INUNDACAO, TUMULTOS_GREVES_LOCKOUT_E_ATOS_DOLOSOS, ROUBO_E_FURTO_QUALIFICADO, RECOMPOSICAO_DE_REGISTROS_E_DOCUMENTOS, DETERIORACAO_DE_MERCADORIAS_EM_AMBIENTES_FRIGORIFICADOS, DERRAME, VAZAMENTO_, QUEBRA_DE_MAQUINAS, LUCROS_CESSANTES_DESPESAS_FIXAS_LUCRO_LIQUIDO_LUCRO_BRUTO, PERDA_OU_PAGAMENTO_DE_ALUGUEL, PEQUENAS_OBRAS_DE_ENGENHARIA, DANOS_DE_CAUSA_EXTERNA, DANOS_DE_CAUSA_EXTERNA_E_ROUBO, SERVICOS_EMERGENCIAIS, SERVICOS_DE_CONVENIENCIA, GARANTIA_ESTENDIDA_ORIGINAL, GARANTIA_ESTENDIDA_AMPLIADA, GARANTIA_ESTENDIDA_REDUZIDA, COMPLEMENTACAO_DE_GARANTIA, LUCRO_BRUTO, LUCRO_LIQUIDO, DESPESAS_FIXAS, PERDA_DE_RECEITA_OU_INTERRUPCAO_DE_NEGOCIOS, OBRAS_CIVIS_CONSTRUCAO_E_INSTALACAO_E_MONTAGEM, AFRETAMENTOS_DE_AERONAVES, ARMAZENAGEM_FORA_DO_CANTEIRO_DE_OBRAS_OU_LOCAL_SEGURADO, DANOS_EM_CONSEQUENCIA_DE_ERRO_DE_PROJETO_RISCO_DO_FABRICANTE, DANOS_MORAIS, DESPESAS_COM_DESENTULHO_DO_LOCAL, DESPESAS_DE_SALVAMENTO_E_CONTENCAO_DE_SINISTROS, EQUIPAMENTOS_DE_ESCRITORIO_E_INFORMATICA, EQUIPAMENTOS_MOVEIS_ESTACIONARIOS_UTILIZADOS_NA_OBRA, FERRAMENTAS_DE_PEQUENO_E_MEDIO_PORTE, HONORARIOS_DE_PERITO, INCENDIO_APOS_O_TERMINO_DE_OBRAS_ATE_30_DIAS_EXCETO_PARA_REFORMAS_AMPLIACOES, MANUTENCAO_AMPLA_ATE_24_MESES, MANUTENCAO_SIMPLES_ATE_24_MESES, OBRAS_CONCLUIDAS, OBRAS_TEMPORARIAS, OBRAS_INSTALACOES_CONTRATADAS_ACEITAS_E_OU_COLOCADAS_EM_OPERACAO, PROPRIEDADES_CIRCUNVIZINHAS, RECOMPOSICAO_DE_DOCUMENTOS, RESPONSABILIDADE_CIVIL_EMPREGADOR, FUNDACAO, STANDS_DE_VENDA, TRANSPORTE_TERRESTRE, TUMULTOS_GREVES_E_LOCKOUT, DANOS_MATERIAIS_CAUSADOS_AO_COFRE_FORTE, DANOS_MATERIAIS_CAUSADOS_AOS_CAIXAS_ELETRONICOS_ATM, INFIDELIDADE_DE_FUNCIONARIOS, VALORES_NO_INTERIOR_DO_ESTABELECIMENTO_DENTRO_E_OU_FORA_DE_COFRE_FORTE, VALORES_NO_INTERIOR_DE_CAIXAS_ELETRONICOS_ATM, VALORES_EM_MAOS_DE_PORTADORES_EM_TRANSITO, ALAGAMENTO_INUNDACAO, ALUGUEL_PERDA_OU_PAGAMENTO, BAGAGEM, BASICA_INCENDIO_RAIO_EXPLOSAO, BASICA_DANOS_MATERIAIS, BASICA_DE_OBRAS_CIVIS_EM_CONSTRUCAO_E_INSTALACOES_E_MONTAGENS, BENS_DE_TERCEIROS_EM_PODER_DO_SEGURADO, CARGA_DESCARGA_ICAMENTO_E_DESCIDA, DANOS_NA_FABRICACAO, DERRAME_D_AGUA_OU_OUTRA_SUBSTANCIA_LIQUIDA_DE_INSTALACOES_DE_CHUVEIROS_AUTOMATICOS, DESPESAS_ADICIONAIS_OUTRAS_DESPESAS, DESPESAS_FIXA, DETERIORACAO_DE_MERCADORIAS_EM_AMBIENTES, FRIGORIFICADOS, EQUIPAMENTOS_ARRENDADOS, EQUIPAMENTOS_CEDIDOS_A_TERCEIROS, EQUIPAMENTOS_CINEMATOGRAFICOS_FOTOGRAFICOS_DE_AUDIO_E_VIDEO, EQUIPAMENTOS_DIVERSOS_OUTRAS_MODALIDADES, EQUIPAMENTOS_ELETRONICOS, EQUIPAMENTOS_ESTACIONARIOS, EQUIPAMENTOS_MOVEIS, EQUIPAMENTOS_PORTATEIS, HONORARIOS_DE_PERITOS, IMPACTO_DE_VEICULOS_E_QUEDA_DE_AERONAVES, IMPACTO_DE_VEICULOS_TERRESTRES, LINHAS_DE_TRANSMISSAO_E_DISTRIBUICAO, MOVIMENTACAO_INTERNA_DE_MERCADORIAS, PATIOS, ROUBO_DE_BENS_DE_HOSPEDES, ROUBO_DE_VALORES_EM_TRANSITO_EM_MAOS_DE_PORTADOR, ROUBO_E_FURTO_MEDIANTE_ARROMBAMENTO, ROUBO_E_OU_FURTO_QUALIFICADO_DE_VALORES_NO_INTERIOR_DO_ESTABELECIMENTO_DENTRO_E_OU_FORA_DE_COFRES_FORTES_OU_CAIXAS_FORTES, TERRORISMO_E_SABOTAGEM, VAZAMENTO_DE_TUBULACOES_E_TANQUES, VAZAMENTO_DE_TUBULACOES_HIDRAULICAS, VENDAVAL_FURACAO_CICLONE_TORNADO_GRANIZO_QUEDA_DE_AERONAVES_OU_QUAISQUER_OUTROS_ENGENHOS_AEREOS_OU_ESPACIAIS_IMPACTO_DE_VEICULOS_TERRESTRES_E_FUMACA, OUTRAS");

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		assertField(body,
			new ObjectArrayField
				.Builder("data")
				.setValidator(this::assertInnerFields)
				.build());

		String linksPattern = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/insurance-patrimonial\\/v\\d+)(\\/insurance-patrimonial.*)?$";
		String requestUri = environment.getString("protected_resource_url");
		new OpenInsuranceLinksAndMetaValidator(this).assertMetaAndLinksWithPattern(body, requestUri, linksPattern);
		logFinalStatus();
		return environment;
	}

	private void assertInnerFields(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("identification")
				.setMaxLength(50)
				.build());

		assertField(data,
			new StringField
				.Builder("documentationDeliveryDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("status")
				.setEnums(STATUS)
				.build());

		assertField(data,
			new StringField
				.Builder("statusAlterationDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("occurrenceDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("warningDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("thirdPartyClaimDate")
				.setMaxLength(10)
				.setOptional()
				.build());

		assertField(data,
			new ObjectField
				.Builder("amount")
				.setValidator(this::assertAmount)
				.build());

		assertField(data,
			new StringField
				.Builder("denialJustification")
				.setEnums(JUSTIFICATION)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("denialJustificationDescription")
				.setMaxLength(100)
				.setOptional()
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("coverages")
				.setValidator(this::assertCoverages)
				.setOptional()
				.build());
	}

	private void assertCoverages(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("insuredObjectId")
				.setMaxLength(100)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("branch")
				.setMaxLength(4)
				.build());

		assertField(data,
			new StringField
				.Builder("code")
				.setEnums(CODE)
				.build());

		assertField(data,
			new StringField
				.Builder("description")
				.setMaxLength(500)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("warningDate")
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("thirdPartyClaimDate")
				.setOptional()
				.build());
	}

	private void assertAmount(JsonObject minValue) {
		assertField(minValue,
				new StringField
						.Builder("amount")
						.setPattern("^\\d{1,16}\\.\\d{2}$")
						.build());

		assertField(minValue,
				new ObjectField
						.Builder("unit")
						.setValidator(this::assertUnit)
						.build());
	}

	private void assertUnit(JsonObject unit) {
		assertField(unit,
				new StringField
						.Builder("code")
						.setMaxLength(2)
						.build());

		assertField(unit,
				new StringField
						.Builder("description")
						.setPattern("^(\\w{3})$")
						.build());
	}
}
