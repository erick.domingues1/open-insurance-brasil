package net.openid.conformance.opin.validator.productsNServices;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.openbanking_brasil.productsNServices.CommonValidatorParts;
import net.openid.conformance.openbanking_brasil.productsNServices.ProductNServicesCommonFields;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.*;

import java.util.Set;

/**
 * Api Source: swagger/openinsurance/swagger-productsnservices-lifepension.yaml
 * Api endpoint: /life-pension/
 * Api version: 1.3.0
 * Api Git Hash: a0cf93fb358df175adea537178f1980078014836
 */

@ApiName("ProductsNServices Life Pension")
public class GetLifePensionValidator extends AbstractJsonAssertingCondition {


	public static final Set<String> SEGMENT = SetUtils.createSet("SEGURO_PESSOAS, PREVIDENCIA");
	public static final Set<String> TYPE = SetUtils.createSet("PGBL, PRGP, PAGP, PRSA, PRI, PDR, VGBL, VRGP, VAGP, VRSA, VRI, VDR, DEMAIS_PRODUTOS_PREVIDENCIA");
	public static final Set<String> MODALITY = SetUtils.createSet("CONTRIBUICAO_VARIAVEL, BENEFICIO_DEFINIDO");
	public static final Set<String> PREMIUM_PAYMENT_METHOD = SetUtils.createSet("CARTAO_CREDITO, DEBITO_CONTA, DEBITO_CONTA_POUPANCA, BOLETO_BANCARIO, PIX, CARTAO_DEBITO, REGRA_PARCEIRO, CONSIGNACAO_FOLHA_PAGAMENTO, PONTOS_PROGRAMA_BENEFICIO, TED_DOC, OUTROS");
	public static final Set<String> TYPE_PERFORMANCE_FEE = SetUtils.createSet("DIRETAMENTE, INDIRETAMENTE, NAO_APLICA");
	public static final Set<String> INCOME_MODALITY = SetUtils.createSet("PAGAMENTO_UNICO, RENDA_PRAZO_CERTO, RENDA_TEMPORARIA, RENDA_TEMPORARIA_REVERSIVEL, RENDA_TEMPORARIA_MINIMO_GARANTIDO, RENDA_TEMPORARIA_REVERSIVEL_MININO_GARANTIDO, RENDA_VITALICIA, RENDA_VITALICIA_REVERSIVEL_BENEFICIARIO_INDICADO, RENDA_VITALICIA_CONJUGE_CONTINUIDADE_MENORES, RENDA_VITALICIA_MINIMO_GARANTIDO, RENDA_VITALICIA_PRAZO_MINIMO_GRANTIDO");
	public static final Set<String> BIOMETRIC_TABLE = SetUtils.createSet("AT_2000_MALE, AT_2000_FEMALE, AT_2000_MALE_FEMALE, AT_2000_MALE_SUAVIZADA_10, AT_2000_FEMALE_SUAVIZADA_10, AT_2000_MALE_FEMALE_SUAVIZADA_10, AT_2000_MALE_SUAVIZADA_15, AT_2000_FEMALE_SUAVIZADA_15, AT_2000_MALE_FEMALE_SUAVIZADA_15, AT_83_MALE, AT_83_FEMALE, AT_83_MALE_FEMALE, BR_EMSSB_MALE, BR_EMSSB_FEMALE, BR_EMSSB_MALE_FEMALE");
	public static final Set<String> CONTRACT_TYPE = SetUtils.createSet("COLETIVO_AVERBADO, COLETIVO_INSTITUIDO, INDIVIDUAL");
	public static final Set<String> TARGET_AUDIENCE = SetUtils.createSet("PESSOA_NATURAL, PESSOA_JURIDICA");
	public static final Set<String> UPDATE_INDEX = SetUtils.createSet("IPCA, IGP-M, INPC, NAO_SE_APLICA");

	private final CommonValidatorParts parts;
	private static class Fields extends ProductNServicesCommonFields { }

	public GetLifePensionValidator() {
		parts = new CommonValidatorParts(this);
	}

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);

		assertField(body,
				new DatetimeField
						.Builder("requestTime")
						.setPattern("[\\w\\W\\s]*")
						.setMaxLength(2048)
						.setOptional()
						.build());

		assertField(body,
				new ObjectField
						.Builder("data")
						.setValidator(data -> {
							assertField(data, new ObjectField
									.Builder("brand")
									.setValidator(brand -> {
										assertField(brand, Fields.name().build());
										assertField(brand, new ObjectArrayField
												.Builder("companies")
												.setValidator(this::assertCompanies)
												.build());
									}).build());
						})
						.build());

		assertField(body,
				new ObjectField
						.Builder("links")
						.setValidator(this::assertLinks)
						.build());

		assertField(body,
				new ObjectField
						.Builder("meta")
						.setValidator(this::assertMeta)
						.build());

		logFinalStatus();
		return environment;
	}

	private void assertCompanies(JsonObject companies) {
		assertField(companies, Fields.name().build());
		assertField(companies, Fields.cnpjNumber().build());

		assertField(companies,
				new ObjectArrayField
						.Builder("products")
						.setValidator(this::assertProducts)
						.build());
	}

	private void assertProducts(JsonObject products) {
		assertField(products, Fields.name().setMaxLength(80).build());
		assertField(products, Fields.code().setMaxLength(80).build());

		assertField(products,
				new StringField
						.Builder("segment")
						.setEnums(SEGMENT)
						.build());

		assertField(products,
				new StringField
						.Builder("type")
						.setEnums(TYPE)
						.setOptional()
						.build());

		assertField(products,
				new StringField
						.Builder("modality")
						.setEnums(MODALITY)
						.build());

		assertField(products,
				new StringField
						.Builder("optionalCoverage")
						.setMaxLength(1024)
						.build());

		assertField(products,
				new ObjectArrayField
						.Builder("productDetails")
						.setValidator(this::assertProductDetails)
						.setOptional()
						.build());

		assertField(products,
				new ObjectField
						.Builder("minimumRequirements")
						.setValidator(minimumRequirements -> {
							assertField(minimumRequirements,
									new StringArrayField
											.Builder("contractType")
											.setEnums(CONTRACT_TYPE)
											.build());

							assertField(minimumRequirements,
									new BooleanField
											.Builder("participantQualified")
											.build());

							assertField(minimumRequirements,
									new StringField
											.Builder("minRequirementsContract")
											.setMaxLength(1024)
											.build());
						})
						.build());

		assertField(products,
				new StringField
						.Builder("targetAudience")
						.setEnums(TARGET_AUDIENCE)
						.build());
	}

	private void assertProductDetails(JsonObject productDetails) {
		assertField(productDetails,
				new StringField
						.Builder("susepProcessNumber")
						.setMaxLength(30)
						.build());

		assertField(productDetails,
				new StringField
						.Builder("contractTermsConditions")
						.setMaxLength(1024)
						.build());

		assertField(productDetails,
				new ObjectField
						.Builder("defferalPeriod")
						.setValidator(this::assertDefferalPeriod)
						.build());

		assertField(productDetails,
				new ObjectField
						.Builder("grantPeriodBenefit")
						.setValidator(this::assertPeriodBenefit)
						.build());

		assertField(productDetails,
				new ObjectField
						.Builder("costs")
						.setValidator(this::assertCosts)
						.build());
	}

	private void assertCosts(JsonObject periodCosts) {
		assertField(periodCosts,
				new ObjectField
						.Builder("loadingAntecipated")
						.setValidator(loadingAntecipated -> {
							assertField(loadingAntecipated,
									new NumberField
											.Builder("minValue")
											.setMaxLength(7)
											.build());

							assertField(loadingAntecipated,
									new NumberField
											.Builder("maxValue")
											.setMaxLength(8)
											.build());
						})
						.build());

		assertField(periodCosts,
				new ObjectField
						.Builder("loadingLate")
						.setValidator(loadingLate -> {
							assertField(loadingLate,
									new NumberField
											.Builder("minValue")
											.setMaxLength(7)
											.build());

							assertField(loadingLate,
									new NumberField
											.Builder("maxValue")
											.setMaxLength(8)
											.build());
						})
						.build());
	}

	private void assertPeriodBenefit(JsonObject periodBenefit) {
		assertField(periodBenefit,
				new StringArrayField
						.Builder("incomeModality")
						.setEnums(INCOME_MODALITY)
						.build());

		assertField(periodBenefit,
				new StringArrayField
						.Builder("biometricTable")
						.setEnums(BIOMETRIC_TABLE)
						.setOptional()
						.build());

		assertField(periodBenefit,
				new NumberField
						.Builder("interestRate")
						.setMaxLength(6)
						.build());

		assertField(periodBenefit,
				new StringField
						.Builder("updateIndex")
						.setEnums(UPDATE_INDEX)
						.build());

		assertField(periodBenefit,
				new NumberField
						.Builder("reversalResultsFinancial")
						.setMaxLength(8)
						.build());

		assertField(periodBenefit,
				new ObjectArrayField
						.Builder("investmentFunds")
						.setValidator(this::assertInvestmentFunds)
						.setOptional()
						.build());
	}

	private void assertDefferalPeriod(JsonObject defferalPeriod) {
		assertField(defferalPeriod,
				new NumberField
						.Builder("interestRate")
						.setMaxLength(10)
						.build());

		assertField(defferalPeriod,
				new StringField
						.Builder("updateIndex")
						.setEnums(UPDATE_INDEX)
						.build());

		assertField(defferalPeriod,
				new StringField
						.Builder("otherMinimumPerformanceGarantees")
						.setMaxLength(12)
						.build());

		assertField(defferalPeriod,
				new NumberField
						.Builder("reversalFinancialResults")
						.setMaxLength(5)
						.build());

		assertField(defferalPeriod,
				new ObjectArrayField
						.Builder("minimumPremiumAmount")
						.setValidator(minimumPremiumAmount -> {
							assertField(minimumPremiumAmount,
									new NumberField
											.Builder("minimumPremiumAmountValue")
											.setMaxLength(13)
											.setOptional()
											.build());

							assertField(minimumPremiumAmount,
									new StringField
											.Builder("minimumPremiumAmountDescription")
											.setMaxLength(15)
											.build());
						})
						.build());

		assertField(defferalPeriod,
				new StringArrayField
						.Builder("premiumPaymentMethod")
						.setEnums(PREMIUM_PAYMENT_METHOD)
						.setOptional()
						.build());

		assertField(defferalPeriod,
				new BooleanField
						.Builder("permissionExtraordinaryContributions")
						.setOptional()
						.build());

		assertField(defferalPeriod,
				new BooleanField
						.Builder("permissonScheduledFinancialPayments")
						.build());

		assertField(defferalPeriod,
				new IntField
						.Builder("gracePeriodRedemption")
						.setMaxLength(4)
						.build());

		assertField(defferalPeriod,
				new IntField
						.Builder("gracePeriodBetweenRedemptionRequests")
						.setMaxLength(4)
						.build());

		assertField(defferalPeriod,
				new IntField
						.Builder("redemptionPaymentTerm")
						.setMaxLength(4)
						.build());

		assertField(defferalPeriod,
				new IntField
						.Builder("gracePeriodPortability")
						.setMaxLength(4)
						.build());

		assertField(defferalPeriod,
				new IntField
						.Builder("gracePeriodBetweenPortabilityRequests")
						.setMaxLength(4)
						.build());

		assertField(defferalPeriod,
				new IntField
						.Builder("portabilityPaymentTerm")
						.setMaxLength(4)
						.build());

		assertField(defferalPeriod,
				new ObjectArrayField
						.Builder("investmentFunds")
						.setValidator(this::assertInvestmentFunds)
						.setOptional()
						.build());
	}

	private void assertInvestmentFunds(JsonObject investmentFunds) {
		assertField(investmentFunds, Fields.cnpjNumber().setMaxLength(18).build());

		assertField(investmentFunds,
				new StringField
						.Builder("companyName")
						.setMaxLength(200)
						.build());

		assertField(investmentFunds,
				new NumberField
						.Builder("maximumAdministrationFee")
						.setMaxLength(5)
						.build());

		assertField(investmentFunds,
				new StringArrayField
						.Builder("typePerformanceFee")
						.setEnums(TYPE_PERFORMANCE_FEE)
						.build());

		assertField(investmentFunds,
				new NumberField
						.Builder("maximumPerformanceFee")
						.setMaxLength(5)
						.setOptional()
						.build());

		assertField(investmentFunds,
				new BooleanField
						.Builder("eligibilityRule")
						.setOptional()
						.build());

		assertField(investmentFunds,
				new NumberField
						.Builder("minimumContributionAmount")
						.setMaxLength(15)
						.setOptional()
						.build());

		assertField(investmentFunds,
				new NumberField
						.Builder("minimumContributionValue")
						.setMaxLength(15)
						.setOptional()
						.build());

		assertField(investmentFunds,
				new NumberField
						.Builder("minimumMathematicalProvisionAmount")
						.setMaxLength(5)
						.setOptional()
						.build());
	}

	public void assertLinks(JsonObject links) {
		assertField(links,
				new StringField
						.Builder("self")
						.setPattern("^(https:\\/\\/)(.*?)(\\/open-insurance\\/products-services\\/v\\d+)(\\/life-pension.*)?$")
						.build());

		assertField(links,
				new StringField
						.Builder("first")
						.setOptional()
						.setPattern("^(https:\\/\\/)(.*?)(\\/open-insurance\\/products-services\\/v\\d+)(\\/life-pension.*)?$")
						.build());

		assertField(links,
				new StringField
						.Builder("prev")
						.setOptional()
						.setPattern("^(https:\\/\\/)(.*?)(\\/open-insurance\\/products-services\\/v\\d+)(\\/life-pension.*)?$")
						.build());

		assertField(links,
				new StringField
						.Builder("next")
						.setOptional()
						.setPattern("^(https:\\/\\/)(.*?)(\\/open-insurance\\/products-services\\/v\\d+)(\\/life-pension.*)?$")
						.build());

		assertField(links,
				new StringField
						.Builder("last")
						.setOptional()
						.setPattern("^(https:\\/\\/)(.*?)(\\/open-insurance\\/products-services\\/v\\d+)(\\/life-pension.*)?$")
						.build());
	}

	private void assertMeta(JsonObject meta) {
		assertField(meta,
				new IntField
						.Builder("totalRecords")
						.build());

		assertField(meta,
				new IntField
						.Builder("totalPages")
						.build());
	}
}
