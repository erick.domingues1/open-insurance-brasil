package net.openid.conformance.opin.validator.claimNotification;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api Source: swagger/openinsurance/claimNotification/claim-notification.yaml
 * Api endpoint: /
 * Api version: 1.1.0
 */

@ApiName("Post Claim Notification Damage V1")
public class PostClaimNotificationDamageValidatorV1 extends AbstractJsonAssertingCondition {
    private static final Set<String> TYPE = SetUtils.createSet("APOLICE_INDIVIDUAL, BILHETE, CERTIFICADO");
    @Override
    @PreEnvironment(strings = "resource_endpoint_response")
    public Environment evaluate(Environment environment) {
        JsonElement body = bodyFrom(environment);
        assertField(body,
                new ObjectField
                        .Builder("data")
                        .setValidator(this::assertData)
                        .build());

        assertField(body,
                new ObjectField
                        .Builder("links")
                        .setValidator(this::assertLinks)
                        .build());

        String linksPattern = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/claim-notification\\/v\\d+)(\\/claim-notification.*)?$";
        String requestUri = environment.getString("protected_resource_url");
        new OpenInsuranceLinksAndMetaValidator(this).assertMetaAndLinksWithPattern(body, requestUri, linksPattern);
        logFinalStatus();
        return environment;
    }

    private void assertData(JsonObject data) {
        assertField(data,
                new StringField
                        .Builder("protocolNumber")
                        .setMaxLength(60)
                        .build());

        assertField(data,
                new StringField
                        .Builder("protocolDateTime")
                        .setMaxLength(2048)
                        .build());

        assertField(data,
                new StringField
                        .Builder("documentType")
                        .setEnums(TYPE)
                        .build());

        assertField(data,
                new StringField
                        .Builder("policyNumber")
                        .setMaxLength(60)
                        .setOptional()
                        .build());

        assertField(data,
                new StringField
                        .Builder("groupCertificateId")
                        .setMaxLength(60)
                        .setOptional()
                        .build());

        assertField(data,
                new StringField
                        .Builder("occurrenceDate")
                        .setMaxLength(10)
                        .build());

        assertField(data,
                new StringField
                        .Builder("occurrenceTime")
                        .setPattern("[0-2][0-3]:[0-5][0-9]:[0-5][0-9]")
                        .setOptional()
                        .build());

        assertField(data,
                new StringField
                        .Builder("occurrenceDescription")
                        .setMaxLength(10000)
                        .build());

        assertField(data,
                new ObjectField
                        .Builder("customData")
                        .setValidator(this::assertCustomData)
                        .setOptional()
                        .build());
    }

    private void assertCustomData(JsonObject customData) {
        assertField(customData,
                new ObjectArrayField
                        .Builder("customerIdentification")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("customerQualification")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("customerComplimentaryInfo")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("businessIdentification")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("businessQualification")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("businessComplimentaryInfo")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("generalQuoteInfo")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("riskLocationInfo")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("insuredObjects")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("beneficiaries")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("coverages")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("generalClaimInfo")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

    }

    private void assertCustomInfoData(JsonObject customInfoData) {
        assertField(customInfoData,
                new StringField
                        .Builder("fieldId")
                        .setPattern("^[a-zA-Z0-9][a-zA-Z0-9\\-]{0,99}$")
                        .setMaxLength(100)
                        .build());

        assertField(customInfoData,
                new StringField
                        .Builder("value")
                        .build());
    }



    private void assertLinks(JsonObject links) {
        assertField(links,
                new StringField
                        .Builder("redirect")
                        .build());
    }
}
