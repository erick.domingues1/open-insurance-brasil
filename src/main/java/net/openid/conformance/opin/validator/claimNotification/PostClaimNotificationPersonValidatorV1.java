package net.openid.conformance.opin.validator.claimNotification;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.BooleanField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api Source: swagger/openinsurance/claimNotification/claim-notification.yaml
 * Api endpoint: /
 * Api version: 1.1.0
 */

@ApiName("Post Claim Notification Person V1")
public class PostClaimNotificationPersonValidatorV1 extends AbstractJsonAssertingCondition {
    private static final Set<String> REQUESTOR_DOCUMENT_TYPE = SetUtils.createSet("CNPJ, CPF, RG_RE, PASSAPORTE, OUTROS");
    private static final Set<String> DOCUMENT_TYPE = SetUtils.createSet("APOLICE_INDIVIDUAL, BILHETE, CERTIFICADO");
    private static final Set<String> CLAIMANT = SetUtils.createSet("CONJUGE, FILHO, SEGURADO");
    private static final Set<String> AREA_CODE = SetUtils.createSet("11, 12, 13, 14, 15, 16, 17, 18, 19, 21, " +
            "22, 24, 27, 28, 31, 32, 33, 34, 35, 37, 38, 41, 42, 43, 44, 45, 46, 47, 48, 49, 51, 53, 54, 55, 61, 62, " +
            "63, 64, 65, 66, 67, 68, 69, 71, 73, 74, 75, 77, 79, 81, 82, 83, 84, 85, 86, 87, 88, 89, 91, 92, 93, 94, 95, " +
            "96, 97, 98, 99, NA");

    @Override
    @PreEnvironment(strings = "resource_endpoint_response")
    public Environment evaluate(Environment environment) {
        JsonElement body = bodyFrom(environment);
        assertField(body,
                new ObjectField
                        .Builder("data")
                        .setValidator(this::assertData)
                        .build());

        assertField(body,
                new ObjectField
                        .Builder("links")
                        .setValidator(this::assertLinks)
                        .build());

        String linksPattern = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/claim-notification\\/v\\d+)(\\/claim-notification.*)?$";
        String requestUri = environment.getString("protected_resource_url");
        new OpenInsuranceLinksAndMetaValidator(this).assertMetaAndLinksWithPattern(body, requestUri, linksPattern);
        logFinalStatus();
        return environment;
    }

    private void assertData(JsonObject data) {
        assertField(data,
                new StringField
                        .Builder("protocolNumber")
                        .setMaxLength(60)
                        .build());

        assertField(data,
                new StringField
                        .Builder("protocolDateTime")
                        .setMaxLength(2048)
                        .build());

        assertField(data,
                new BooleanField
                        .Builder("requestorIsInsured")
                        .build());

        assertField(data,
                new ObjectField
                        .Builder("requestor")
                        .setValidator(this::assertRequestor)
                        .setOptional()
                        .build());

        assertField(data,
                new StringField
                        .Builder("claimant")
                        .setEnums(CLAIMANT)
                        .setOptional()
                        .build());

        assertField(data,
                new StringField
                        .Builder("documentType")
                        .setEnums(DOCUMENT_TYPE)
                        .build());

        assertField(data,
                new StringField
                        .Builder("policyNumber")
                        .setMaxLength(60)
                        .setOptional()
                        .build());

        assertField(data,
                new StringField
                        .Builder("groupCertificateId")
                        .setMaxLength(60)
                        .setOptional()
                        .build());

        assertField(data,
                new StringField
                        .Builder("occurrenceDate")
                        .setMaxLength(10)
                        .build());

        assertField(data,
                new StringField
                        .Builder("occurrenceTime")
                        .setPattern("[0-2][0-3]:[0-5][0-9]:[0-5][0-9]")
                        .setOptional()
                        .build());

        assertField(data,
                new StringField
                        .Builder("occurrenceDescription")
                        .setMaxLength(10000)
                        .build());

        assertField(data,
                new ObjectField
                        .Builder("customData")
                        .setValidator(this::assertCustomData)
                        .setOptional()
                        .build());
    }

    private void assertRequestor(JsonObject requestor) {
        assertField(requestor,
                new StringField
                        .Builder("name")
                        .setMaxLength(70)
                        .setPattern("[\\w\\W\\s]*")
                        .build());

        assertField(requestor,
                new StringField
                        .Builder("documentType")
                        .setEnums(REQUESTOR_DOCUMENT_TYPE)
                        .build());

        assertField(requestor,
                new StringField
                        .Builder("documentNumber")
                        .setMaxLength(20)
                        .setPattern("[\\w\\W\\s]*")
                        .build());

        assertField(requestor,
                new ObjectArrayField
                        .Builder("phones")
                        .setValidator(phones -> {
                            assertField(phones,
                                    new StringField
                                            .Builder("countryCallingCode")
                                            .setMaxLength(4)
                                            .setPattern("^\\d{2,4}$|^NA$")
                                            .setOptional()
                                            .build());

                            assertField(phones,
                                    new StringField
                                            .Builder("areaCode")
                                            .setEnums(AREA_CODE)
                                            .setOptional()
                                            .build());

                            assertField(phones,
                                    new StringField
                                            .Builder("number")
                                            .setMaxLength(11)
                                            .setPattern("^([0-9]{8,11})|^NA$")
                                            .setOptional()
                                            .build());
                        })
                        .setMinItems(1)
                        .setOptional()
                        .build());

        assertField(requestor,
                new ObjectArrayField
                        .Builder("emails")
                        .setValidator(emails -> {
                            assertField(emails,
                                    new StringField
                                            .Builder("email")
                                            .setMaxLength(320)
                                            .setPattern("[\\w\\W\\s]*")
                                            .setOptional()
                                            .build());
                        })
                        .setMinItems(1)
                        .setOptional()
                        .build());
    }

    private void assertCustomData(JsonObject customData) {
        assertField(customData,
                new ObjectArrayField
                        .Builder("customerIdentification")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("customerQualification")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("customerComplimentaryInfo")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("businessIdentification")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("businessQualification")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("businessComplimentaryInfo")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("generalQuoteInfo")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("riskLocationInfo")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("insuredObjects")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("beneficiaries")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("coverages")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());

        assertField(customData,
                new ObjectArrayField
                        .Builder("generalClaimInfo")
                        .setValidator(this::assertCustomInfoData)
                        .setOptional()
                        .build());
    }

    private void assertCustomInfoData(JsonObject customInfoData) {
        assertField(customInfoData,
                new StringField
                        .Builder("fieldId")
                        .setPattern("^[a-zA-Z0-9][a-zA-Z0-9\\-]{0,99}$")
                        .setMaxLength(100)
                        .build());

        assertField(customInfoData,
                new StringField
                        .Builder("value")
                        .build());
    }

    private void assertLinks(JsonObject links) {
        assertField(links,
                new StringField
                        .Builder("redirect")
                        .build());
    }
}
