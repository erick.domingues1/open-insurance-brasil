package net.openid.conformance.opin.validator.financialFisk.v1;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.StringArrayField;
import net.openid.conformance.util.field.StringField;

/**
 * Api Source: swagger/openinsurance/financialFisk/v1/swagger-insurance-financial-risk.yaml
 * Api endpoint: /{policyId}/claim
 * Api version: 1.1.0
 */

@ApiName("Insurance Financial Risk List V1")
public class OpinInsuranceFinancialRiskListValidatorV1 extends AbstractJsonAssertingCondition {

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		assertField(body,
			new ObjectArrayField
				.Builder("data")
				.setValidator(this::assertInnerFields)
				.build());

		String linksPattern = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/insurance-financial-risk\\/v\\d+)(\\/insurance-financial-risk.*)?$";
		String requestUri = environment.getString("protected_resource_url");
		new OpenInsuranceLinksAndMetaValidator(this).assertMetaAndLinksWithPattern(body, requestUri, linksPattern);
		logFinalStatus();
		return environment;
	}

	private void assertInnerFields(JsonObject identification) {
		assertField(identification,
			new StringField
				.Builder("brand")
				.setMaxLength(80)
				.build());

		assertField(identification,
			new ObjectArrayField
				.Builder("companies")
				.setValidator(this::assertCompanies)
				.build());
	}

	private void assertCompanies(JsonObject products) {
		assertField(products,
			new StringField
				.Builder("companyName")
				.setMaxLength(200)
				.build());

		assertField(products,
			new StringField
				.Builder("cnpjNumber")
				.setMaxLength(14)
				.setPattern("^\\d{14}$")
				.build());

		assertField(products,
			new StringArrayField
				.Builder("policies")
				.setMaxLength(60)
				.build());
	}
}
