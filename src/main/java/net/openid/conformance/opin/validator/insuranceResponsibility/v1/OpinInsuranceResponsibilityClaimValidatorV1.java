package net.openid.conformance.opin.validator.insuranceResponsibility.v1;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.DoubleField;
import net.openid.conformance.util.field.ObjectArrayField;
import net.openid.conformance.util.field.ObjectField;
import net.openid.conformance.util.field.StringField;

import java.util.Set;

/**
 * Api Source: swagger/openinsurance/insuranceResponsibility/v1/swagger-insurance-responsibility.yaml
 * Api endpoint: /{policyId}/claim
 * Api version: 1.2.0
 */

@ApiName("Insurance Responsibility Claim V1")
public class OpinInsuranceResponsibilityClaimValidatorV1 extends AbstractJsonAssertingCondition {
	public static final Set<String> STATUS = SetUtils.createSet("ABERTO, ENCERRADO_COM_INDENIZACAO, ENCERRADO_SEM_INDENIZACAO, REABERTO, CANCELADO_POR_ERRO_OPERACIONAL, AVALIACAO_INICIAL");
	public static final Set<String> JUSTIFICATION = SetUtils.createSet("RISCO_EXCLUIDO, RISCO_AGRAVADO, SEM_DOCUMENTACAO, DOCUMENTACAO_INCOMPLETA, PRESCRICAO, FORA_COBERTURA, OUTROS");
	public static final Set<String> CODE = SetUtils.createSet("DANOS_CAUSADOS_A_TERCEIROS, INSTALACOES_FIXAS, TRANSPORTE_AMBIENTAL, OBRAS_E_PRESTACAO_DE_SERVICO, ALAGAMENTO_E_OU_INUNDACAO, ANUNCIOS_E_ANTENAS, ASSISTENCIAS_TECNICAS_E_MECANICAS, CONDOMINIOS_PROPRIETARIOS_E_LOCATARIOS_DE_IMOVEIS, CUSTOS_DE_DEFESA_DO_SEGURADO, DANOS_CAUSADOS_POR_FALHAS_DE_PROFISSIONAL_DA_AREA_MEDICA, DANOS_CAUSADOS_POR_FOGOS_DE_ARTIFICIO, DANOS_ESTETICOS, DANOS_MORAIS, DESPESAS_EMERGENCIAIS_DESPESAS_DE_CONTENCAO_E_DESPESAS_DE_SALVAMENTO_DE_SINISTRO, EMPREGADOR_EMPREGADOS, EMPRESAS_DE_SERVICOS, EQUIPAMENTOS_DE_TERCEIROS_OPERADOS_PELO_SEGURADO, ERRO_DE_PROJETO, EXCURSOES_EVENTOS_EXPOSICOES_E_ATIVIDADES, FAMILIAR, FINANCEIRO, FORO, INDUSTRIA_E_COMERCIO, LOCAIS_E_OU_ESTABELECIMENTOS_DE_QUALQUER_NATUREZA, OBRAS, OPERACOES_DE_QUALQUER_NATUREZA, POLUICAO, PRESTACAO_DE_SERVICOS, PRODUTOS, RECALL, RECLAMACOES_DECORRENTES_DO_FORNECIMENTO_DE_COMESTIVEIS_OU_BEBIDAS, SINDICO, TELEFERICOS_E_SIMILARES, TRANSPORTE_DE_BENS_OU_PESSOAS, VEICULOS_EMBARCACOES_BENS_E_MERCADORIAS, RESPONSABILIZACAO_CIVIL_VINCULADA_A_PRESTACAO_DE_SERVICOS_PROFISSIONAIS_OBJETO_DA_ATIVIDADE_DO_SEGURADO, RESPONSABILIDADE_CIVIL_PERANTE_TERCEIROS, PERDAS_DIRETAS_AO_SEGURADO, GERENCIAMENTO_DE_CRISE, OUTRAS");

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		assertField(body,
			new ObjectArrayField
				.Builder("data")
				.setValidator(this::assertInnerFields)
				.build());

		String linksPattern = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/insurance-responsibility\\/v\\d+)(\\/insurance-responsibility.*)?$";
		new OpenInsuranceLinksAndMetaValidator(this).assertMetaAndLinksWithPattern(body, linksPattern);
		logFinalStatus();
		return environment;
	}

	private void assertInnerFields(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("identification")
				.setMaxLength(50)
				.build());

		assertField(data,
			new StringField
				.Builder("documentationDeliveryDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("status")
				.setEnums(STATUS)
				.build());

		assertField(data,
			new StringField
				.Builder("statusAlterationDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("occurrenceDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("warningDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("thirdPartyClaimDate")
				.setMaxLength(10)
				.setOptional()
				.build());

		assertField(data,
			new ObjectField
				.Builder("amount")
				.setValidator(this::assertAmount)
				.build());

		assertField(data,
			new StringField
				.Builder("denialJustification")
				.setEnums(JUSTIFICATION)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("denialJustificationDescription")
				.setMaxLength(100)
				.setOptional()
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("coverages")
				.setValidator(this::assertCoverages)
				.setOptional()
				.build());
	}

	private void assertCoverages(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("insuredObjectId")
				.setMaxLength(100)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("branch")
				.setMaxLength(4)
				.build());

		assertField(data,
			new StringField
				.Builder("code")
				.setEnums(CODE)
				.build());

		assertField(data,
			new StringField
				.Builder("description")
				.setMaxLength(500)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("warningDate")
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("thirdPartyClaimDate")
				.setOptional()
				.build());
	}

	private void assertAmount(JsonObject minValue) {
		assertField(minValue,
				new StringField
						.Builder("amount")
						.setPattern("^\\d{1,16}\\.\\d{2}$")
						.build());

		assertField(minValue,
				new ObjectField
						.Builder("unit")
						.setValidator(this::assertUnit)
						.build());
	}

	private void assertUnit(JsonObject unit) {
		assertField(unit,
				new StringField
						.Builder("code")
						.setMaxLength(2)
						.build());

		assertField(unit,
				new StringField
						.Builder("description")
						.setPattern("^(\\w{3})$")
						.build());
	}
}
