package net.openid.conformance.opin.validator.insuranceTransport.v1;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.openid.conformance.condition.PreEnvironment;
import net.openid.conformance.condition.client.jsonAsserting.AbstractJsonAssertingCondition;
import net.openid.conformance.logging.ApiName;
import net.openid.conformance.opin.validator.OpenInsuranceLinksAndMetaValidator;
import net.openid.conformance.testmodule.Environment;
import net.openid.conformance.util.SetUtils;
import net.openid.conformance.util.field.*;

import java.util.Set;

/**
 * Api Source: swagger/openinsurance/insuranceTransport/v1/swagger-insurance-transport.yaml
 * Api endpoint: /{policyId}/policy-info
 * Api version: 1.1.0
 */

@ApiName("Insurance Transport PolicyInfo V1")
public class OpinInsuranceTransportPolicyInfoValidatorV1 extends AbstractJsonAssertingCondition {

	public static final Set<String> DOCUMENT_TYPE = SetUtils.createSet("APOLICE_INDIVIDUAL, BILHETE, CERTIFICADO, APOLICE_INDIVIDUAL_AUTOMOVEL, APOLICE_FROTA_AUTOMOVEL, CERTIFICADO_AUTOMOVEL");
	public static final Set<String> ISSUANCE_TYPE = SetUtils.createSet("EMISSAO_PROPRIA, COSSEGURO_ACEITO");
	public static final Set<String> IDENTIFICATION_TYPE = SetUtils.createSet("CPF, CNPJ, OUTROS");
	public static final Set<String> INTERMEDIARIES_TYPE = SetUtils.createSet("CORRETOR, REPRESENTANTE, ESTIPULANTE_AVERBADOR_INSTITUIDOR, CORRESPONDENTE, AGENTE_DE_MICROSSEGUROS, OUTROS");
	public static final Set<String> INSURED_OBJECTS_TYPE = SetUtils.createSet("CONTRATO, PROCESSO_ADMINISTRATIVO, PROCESSO_JUDICIAL, AUTOMOVEL, CONDUTOR, FROTA, PESSOA, OUTROS");
	public static final Set<String> GRACE_PERIODICITY = SetUtils.createSet("DIA, MES, ANO");
	public static final Set<String> GRACE_PERIOD_COUNTING_METHOD = SetUtils.createSet("DIAS_UTEIS, DIAS_CORRIDOS");
	public static final Set<String> CODE = SetUtils.createSet("ACIDENTES_PESSOAIS_COM_PASSAGEIROS, ACIDENTES_PESSOAIS_COM_TRIPULANTES, DANOS_CORPORAIS_PASSAGEIROS, DANOS_CORPORAIS_TERCEIROS, DANOS_CORPORAIS_TRIPULANTES, DANOS_ESTETICOS_PASSAGEIROS, DANOS_ESTETICOS_TERCEIROS, DANOS_MATERIAIS_PASSAGEIROS, DANOS_MATERIAIS_TERCEIROS, DANOS_MATERIAIS_TRIPULANTES, DANOS_MORAIS_PASSAGEIROS, DANOS_MORAIS_TERCEIROS, DANOS_MORAIS_TRIPULANTES, DESPESAS_COM_HONORARIOS, EMBARCADOR_AMPLA_A, EMBARCADOR_RESTRITA_B, EMBARCADOR_RESTRITA_C, RESPONSABILIDADE_CIVIL_DO_OPERADOR_DE_TRANSPORTES_MULTIMODAL_CARGA, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_AEREO_CARGA, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_AQUAVIARIO_CARGA, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_FERROVIARIO_CARGA, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_RODOVIARIO_CARGA, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_RODOVIARIO_EM_VIAGEM_INTERNACIONAL, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_RODOVIARIO_EM_VIAGEM_INTERNACIONAL_DANOS_CARGA_TRANSPORTADA, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_RODOVIARIO_POR_DESAPARECIMENTO_DE_CARGA, OUTRAS");
	public static final Set<String> COVERAGE_CODE = SetUtils.createSet("ACIDENTES_PESSOAIS_COM_PASSAGEIROS, ACIDENTES_PESSOAIS_COM_TRIPULANTES, DANOS_CORPORAIS_PASSAGEIROS, DANOS_CORPORAIS_TERCEIROS, DANOS_CORPORAIS_TRIPULANTES, DANOS_ESTETICOS_PASSAGEIROS, DANOS_ESTETICOS_TERCEIROS, DANOS_ESTETICOS_TRIPULANTES, DANOS_MATERIAIS_PASSAGEIROS, DANOS_MATERIAIS_TERCEIROS, DANOS_MATERIAIS_TRIPULANTES, DANOS_MORAIS_PASSAGEIROS, DANOS_MORAIS_TERCEIROS, DANOS_MORAIS_TRIPULANTES, DESPESAS_COM_HONORARIOS, EMBARCADOR_AMPLA_A_RISCOS_DE_PERDA_OU_DANO_MATERIAL_SOFRIDOS_PELO_OBJETO_SEGURADO_EM_CONSEQUENCIA_DE_QUAISQUER_CAUSAS_EXTERNAS_EXCETO_AS_PREVISTAS_NA_CLAUSULA_DE_PREJUIZOS_NAO_INDENIZAVEIS, EMBARCADOR_RESTRITA_B_COBERTURAS_ELENCADAS_NA_EMBARCADOR_RESTRITA_C_E_INCLUI_INUNDACAO_TRANSBORDAMENTO_DE_CURSOS_DAGUA_REPRESAS_LAGOS_OU_LAGOAS_DURANTE_A_VIAGEM_TERRESTRE_DESMORONAMENTO_OU_QUEDA_DE_PEDRAS_TERRAS_OBRAS_DE_ARTE_DE_QUALQUER_NATUREZA_OU_OUTROS_OBJETOS_DURANTE_A_VIAGEM_TERRESTRE_TERREMOTO_OU_ERUPCAO_VULCANICA_E, ENTRADA_DE_AGUA_DO_MAR_LAGO_OU_RIO_NA_EMBARCACAO_OU_NO_NAVIO, VEICULO_CONTAINER_FURGAO_LIFTVAN_OU_LOCAL_DE_ARMAZENAGEM, EMBARCADOR_RESTRITA_C_PERDAS_E_DANOS_MATERIAIS_CAUSADOS_AO_OBJETO_SEGURADO_EXCLUSIVAMENTE_POR_INCENDIO_RAIO_OU_EXPLOSAO_ENCALHE, NAUFRAGIO_OU_SOCOBRAMENTO_DO_NAVIO_OU_EMBARCACAO_CAPOTAGEM_COLISAO_TOMBAMENTO_OU_DESCARRILAMENTO_DE_VEICULO_TERRESTRE_ABALROAMENTO_COLISAO_OU_CONTATO_DO_NAVIO_OU_EMBARCACAO_COM_QUALQUER_OBJETO_EXTERNO_QUE_NAO_SEJA_AGUA_COLISAO_QUEDA_E_OU_ATERRISSAGEM_FORCADA_DA_AERONAVE_DEVIDAMENTE_COMPROVADA_DESCARGA_DA_CARGA_EM_PORTO_DE_ARRIBADA_CARGA_LANCADA_AO_MAR_PERDA_TOTAL_DE_QUALQUER_VOLUME_DURANTE_AS_OPERACOES_DE_CARGA_E_DESCARGA_DO_NAVIO_E, PERDA_TOTAL_DECORRENTE_DE_FORTUNA_DO_MAR_E_OU_DE_ARREBATAMENTO_PELO_MAR, RESPONSABILIDADE_CIVIL_DO_OPERADOR_DE_TRANSPORTES_MULTIMODAL_CARGA_RCOTM_C, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_AEREO_CARGA_RCTA_C, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_AQUAVIARIO_CARGA_RCA_C, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_FERROVIARIO_CARGA_RCTF_C, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_RODOVIARIO_CARGA_RCTR, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_RODOVIARIO_EM_VIAGEM_INTERNACIONAL_DANOS_CAUSADOS_A_PESSOAS_OU_COISAS_TRANSPORTADAS_OU_NAO_A_EXCECAO_DA_CARGA_TRANSPORTADA_CARTA_AZUL, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_RODOVIARIO_EM_VIAGEM_INTERNACIONAL_DANOS_A_CARGA_TRANSPORTADA_RCTR_VI_C_, RESPONSABILIDADE_CIVIL_DO_TRANSPORTADOR_RODOVIARIO_POR_DESAPARECIMENTO_DE_CARGA_RCF_DC, OUTRAS");
	public static final Set<String> FEATURE = SetUtils.createSet("MASSIFICADOS, MASSIFICADOS_MICROSEGUROS, GRANDES_RISCOS");
	public static final Set<String> TYPE = SetUtils.createSet("PARAMETRICO, INTERMITENTE, REGULAR_COMUM, CAPITAL_GLOBAL, PARAMETRICO_E_INTERMITENTE");
	public static final Set<String> DEDUCTIBLE_TYPE = SetUtils.createSet("REDUZIDA, NORMAL, MAJORADA, DEDUTIVEL, OUTROS");
	public static final Set<String> APPLICATION_TYPE = SetUtils.createSet("VALOR, PERCENTUAL, OUTROS");
	public static final Set<String> TRAVEL_TYPE = SetUtils.createSet("INTERNACIONAL_IMPORTACAO, INTERNACIONAL_EXPORTACAO, NACIONAL, INTERNACIONAL");
	public static final Set<String> TRANSPORT_TYPE = SetUtils.createSet("AEREO, MARITIMO, LACUSTRE_FLUVIAL, RODOVIARIO, FERROVIARIO, MULTIMODAL, DIVERSOS_MODAIS");
	private final OpenInsuranceLinksAndMetaValidator linksAndMetaValidator = new OpenInsuranceLinksAndMetaValidator(this);

	@Override
	@PreEnvironment(strings = "resource_endpoint_response")
	public Environment evaluate(Environment environment) {
		JsonElement body = bodyFrom(environment);
		assertField(body,
			new ObjectField
				.Builder(ROOT_PATH)
				.setValidator(this::assertData)
				.build());

		String linksPattern = "^(https:\\/\\/)(.*?)(\\/open-insurance\\/insurance-transport\\/v\\d+)(\\/insurance-transport.*)?$";
		new OpenInsuranceLinksAndMetaValidator(this).assertMetaAndLinksWithPattern(body, linksPattern);
		logFinalStatus();
		return environment;
	}

	private void assertData(JsonObject data) {
		assertField(data,
			new StringField
				.Builder("documentType")
				.setEnums(DOCUMENT_TYPE)
				.build());

		assertField(data,
			new StringField
				.Builder("policyId")
				.setMaxLength(60)
				.build());

		assertField(data,
			new StringField
				.Builder("susepProcessNumber")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("groupCertificateId")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("issuanceType")
				.setEnums(ISSUANCE_TYPE)
				.build());

		assertField(data,
			new StringField
				.Builder("issuanceDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("termStartDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("termEndDate")
				.setMaxLength(10)
				.build());

		assertField(data,
			new StringField
				.Builder("leadInsurerCode")
				.setMaxLength(1024)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("leadInsurerPolicyId")
				.setMaxLength(1024)
				.setOptional()
				.build());

		assertField(data,
			new ObjectField
				.Builder("maxLMG")
				.setValidator(this::assertAmount)
				.build());

		assertField(data,
			new StringField
				.Builder("proposalId")
				.setMaxLength(60)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("insureds")
				.setValidator(this::assertPersonalInfo)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("beneficiaries")
				.setValidator(this::assertBeneficiaryInfo)
				.setOptional()
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("principals")
				.setValidator(this::assertPersonalInfo)
				.setOptional()
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("intermediaries")
				.setValidator(this::assertIntermediaries)
				.setOptional()
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("insuredObjects")
				.setValidator(this::assertInsuredObjects)
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("coverages")
				.setValidator(this::assertCoverages)
				.setMinProperties(3)
				.setOptional()
				.build());

		assertField(data,
			new StringField
				.Builder("coinsuranceRetainedPercentage")
				.setPattern("^\\d{1,3}\\.\\d{1,9}$")
				.setOptional()
				.build());

		assertField(data,
			new ObjectArrayField
				.Builder("coinsurers")
				.setValidator(coinsurers -> {
					assertField(coinsurers,
						new StringField
							.Builder("identification")
							.setMaxLength(60)
							.build());

					assertField(coinsurers,
						new StringField
							.Builder("cededPercentage")
							.setPattern("^\\d{1,3}\\.\\d{1,9}$")
							.build());
				})
				.setOptional()
				.build());

		assertField(data,
			new ObjectField
				.Builder("branchInfo")
				.setValidator(branchInfo -> assertField(branchInfo,
					new ObjectArrayField
						.Builder("endorsements")
						.setValidator(this::assertEndorsements)
						.build()))
				.build());
	}

	private void assertPersonalInfo(JsonObject insureds) {
		assertField(insureds,
			new StringField
				.Builder("identification")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("identificationType")
				.setEnums(IDENTIFICATION_TYPE)
				.build());

		assertField(insureds,
			new StringField
				.Builder("name")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("postCode")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("email")
				.setMaxLength(256)
				.setOptional()
				.build());

		assertField(insureds,
			new StringField
				.Builder("city")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("state")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("country")
				.setMaxLength(3)
				.setPattern("^(\\w{3}){1}$")
				.build());

		assertField(insureds,
			new StringField
				.Builder("address")
				.setMaxLength(60)
				.build());
	}

	private void assertBeneficiaryInfo(JsonObject insureds) {
		assertField(insureds,
			new StringField
				.Builder("identification")
				.setMaxLength(60)
				.build());

		assertField(insureds,
			new StringField
				.Builder("identificationType")
				.setEnums(IDENTIFICATION_TYPE)
				.build());

		assertField(insureds,
			new StringField
				.Builder("name")
				.setMaxLength(60)
				.build());
	}

	private void assertIntermediaries(JsonObject intermediaries) {
		assertField(intermediaries,
			new StringField
				.Builder("type")
				.setEnums(INTERMEDIARIES_TYPE)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("identification")
				.setMaxLength(60)
				.setPattern("^\\d{1,60}$")
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("brokerId")
				.setMaxLength(100)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("identificationType")
				.setEnums(IDENTIFICATION_TYPE)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("name")
				.setMaxLength(60)
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("postCode")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("city")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("state")
				.setMaxLength(60)
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("country")
				.setMaxLength(3)
				.setPattern("^(\\w{3}){1}$")
				.setOptional()
				.build());

		assertField(intermediaries,
			new StringField
				.Builder("address")
				.setMaxLength(60)
				.setOptional()
				.build());
	}

	private void assertInsuredObjects(JsonObject insuredObjects) {
		assertField(insuredObjects,
			new StringField
				.Builder("identification")
				.setMaxLength(100)
				.build());

		assertField(insuredObjects,
			new StringField
				.Builder("type")
				.setEnums(INSURED_OBJECTS_TYPE)
				.build());

		assertField(insuredObjects,
			new StringField
				.Builder("typeAdditionalInfo")
				.setMaxLength(100)
				.setOptional()
				.build());

		assertField(insuredObjects,
			new StringField
				.Builder("description")
				.setMaxLength(1024)
				.build());

		assertField(insuredObjects,
			new ObjectField
				.Builder("amount")
				.setValidator(this::assertAmount)
				.setOptional()
				.build());

		assertField(insuredObjects,
			new ObjectArrayField
				.Builder("coverages")
				.setValidator(this::assertInsuredObjectsCoverages)
				.build());
	}

	private void assertInsuredObjectsCoverages(JsonObject coverages) {
		assertField(coverages,
			new StringField
				.Builder("branch")
				.setMaxLength(4)
				.build());

		assertField(coverages,
			new StringField
				.Builder("code")
				.setEnums(CODE)
				.build());

		assertField(coverages,
			new StringField
				.Builder("description")
				.setMaxLength(500)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("internalCode")
				.setMaxLength(500)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("susepProcessNumber")
				.setMaxLength(50)
				.build());

		assertField(coverages,
			new ObjectField
				.Builder("LMI")
				.setValidator(this::assertAmount)
				.build());

		assertField(coverages,
			new BooleanField
				.Builder("isLMISublimit")
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("termStartDate")
				.setMaxLength(10)
				.build());

		assertField(coverages,
			new StringField
				.Builder("termEndDate")
				.setMaxLength(10)
				.build());

		assertField(coverages,
			new BooleanField
				.Builder("isMainCoverage")
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("feature")
				.setEnums(FEATURE)
				.build());

		assertField(coverages,
			new StringField
				.Builder("type")
				.setEnums(TYPE)
				.build());

		assertField(coverages,
			new IntField
				.Builder("gracePeriod")
				.setMaxLength(5)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("gracePeriodicity")
				.setEnums(GRACE_PERIODICITY)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("gracePeriodCountingMethod")
				.setEnums(GRACE_PERIOD_COUNTING_METHOD)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("gracePeriodStartDate")
				.setMaxLength(10)
				.setOptional()
				.build());

		assertField(coverages,
			new StringField
				.Builder("gracePeriodEndDate")
				.setMaxLength(10)
				.setOptional()
				.build());
	}

	private void assertCoverages(JsonObject coverages) {
		assertField(coverages,
			new StringField
				.Builder("branch")
				.setMaxLength(4)
				.build());

		assertField(coverages,
			new StringField
				.Builder("code")
				.setEnums(COVERAGE_CODE)
				.build());

		assertField(coverages,
			new StringField
				.Builder("description")
				.setMaxLength(500)
				.setOptional()
				.build());

		assertField(coverages,
			new ObjectField
				.Builder("deductible")
				.setValidator(this::assertDeductible)
				.setOptional()
				.build());

		assertField(coverages,
			new ObjectField
				.Builder("POS")
				.setValidator(this::assertPOS)
				.setOptional()
				.build());
	}

	private void assertDeductible(JsonObject deductible) {
		assertField(deductible,
			new StringField
				.Builder("type")
				.setEnums(DEDUCTIBLE_TYPE)
				.build());

		assertField(deductible,
			new StringField
				.Builder("typeAdditionalInfo")
				.setMaxLength(500)
				.setOptional()
				.build());

		assertField(deductible,
			new ObjectField
				.Builder("amount")
				.setValidator(this::assertAmount)
				.build());

		assertField(deductible,
			new IntField
				.Builder("period")
				.setMaxLength(5)
				.build());

		assertField(deductible,
			new StringField
				.Builder("periodicity")
				.setEnums(GRACE_PERIODICITY)
				.build());

		assertField(deductible,
			new StringField
				.Builder("periodCountingMethod")
				.setEnums(GRACE_PERIOD_COUNTING_METHOD)
				.setOptional()
				.build());

		assertField(deductible,
			new StringField
				.Builder("periodStartDate")
				.build());

		assertField(deductible,
			new StringField
				.Builder("periodEndDate")
				.build());

		assertField(deductible,
			new StringField
				.Builder("description")
				.setMaxLength(60)
				.build());
	}

	private void assertPOS(JsonObject pOS) {
		assertField(pOS,
			new StringField
				.Builder("applicationType")
				.setEnums(APPLICATION_TYPE)
				.build());

		assertField(pOS,
			new StringField
				.Builder("description")
				.setMaxLength(60)
				.build());

		assertField(pOS,
			new ObjectField
				.Builder("minValue")
				.setValidator(this::assertAmount)
				.setOptional()
				.build());

		assertField(pOS,
			new ObjectField
				.Builder("maxValue")
				.setValidator(this::assertAmount)
				.setOptional()
				.build());

		assertField(pOS,
			new StringField
				.Builder("percentage")
				.setPattern("^\\d{1,3}\\.\\d{1,9}$")
				.setOptional()
				.build());
	}

	private void assertEndorsements(JsonObject endorsements) {
		assertField(endorsements,
			new StringField
				.Builder("travelType")
				.setEnums(TRAVEL_TYPE)
				.build());

		assertField(endorsements,
			new StringField
				.Builder("transportType")
				.setEnums(TRANSPORT_TYPE)
				.build());

		assertField(endorsements,
			new IntField
				.Builder("shipmentsNumber")
				.setMaxLength(100)
				.build());

		assertField(endorsements,
			new StringField
				.Builder("branch")
				.setMaxLength(4)
				.setOptional()
				.build());

		assertField(endorsements,
			new ObjectField
				.Builder("shipmentsPremium")
				.setValidator(this::assertAmount)
				.build());

		assertField(endorsements,
			new StringField
				.Builder("shipmentsPremiumBRL")
				.setPattern("^\\d{1,16}\\.\\d{2}$")
				.build());

		assertField(endorsements,
			new ObjectField
				.Builder("shipmentsInsuredsAmount")
				.setValidator(this::assertAmount)
				.build());

		assertField(endorsements,
			new ObjectField
				.Builder("minInsuredAmount")
				.setValidator(this::assertAmount)
				.build());

		assertField(endorsements,
			new ObjectField
				.Builder("maxInsuredAmount")
				.setValidator(this::assertAmount)
				.build());
	}

	private void assertAmount(JsonObject minValue) {
		assertField(minValue,
				new StringField
						.Builder("amount")
						.setPattern("^\\d{1,16}\\.\\d{2}$")
						.build());

		assertField(minValue,
				new ObjectField
						.Builder("unit")
						.setValidator(this::assertUnit)
						.build());
	}

	private void assertUnit(JsonObject unit) {
		assertField(unit,
				new StringField
						.Builder("code")
						.setMaxLength(2)
						.build());

		assertField(unit,
				new StringField
						.Builder("description")
						.setPattern("^(\\w{3})$")
						.build());
	}
}
