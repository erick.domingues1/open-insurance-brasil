package net.openid.raidiam.opin;

import net.openid.conformance.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpinApplication {

    private static final Logger LOG = LoggerFactory.getLogger(OpinApplication.class);

    public static void main(String[] args) {
        LOG.info("Bootstrapping open insurance conformance suite.");
        Application.main(args);
    }

}
