package net.openid.conformance.opin.claimNotification;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.claimNotification.PostClaimNotificationDamageValidatorV1;
import net.openid.conformance.opin.validator.claimNotification.PostClaimNotificationPersonValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class ClaimNotificationValidatorsTest extends AbstractJsonResponseConditionUnitTest {

    private String PROTECTED_RESOURCE_URL = "https://api.seguro.com.br/open-insurance/claim-notification/v1/claim-notification/";

    @Test
    @UseResurce("opinResponses/claimNotification/PostClaimNotificationDamageResponse.json")
    public void validateStructureDamage() {
        environment.putString("protected_resource_url", PROTECTED_RESOURCE_URL + "request/damage/111");
        run(new PostClaimNotificationDamageValidatorV1());
    }

    @Test
    @UseResurce("opinResponses/claimNotification/PostClaimNotificationPersonResponse.json")
    public void validateStructurePerson() {
        environment.putString("protected_resource_url", PROTECTED_RESOURCE_URL + "request/person/111");
        run(new PostClaimNotificationPersonValidatorV1());
    }

}