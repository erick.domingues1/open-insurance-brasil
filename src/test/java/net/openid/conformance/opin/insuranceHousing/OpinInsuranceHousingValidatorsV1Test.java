package net.openid.conformance.opin.insuranceHousing;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.insuranceHousing.v1.OpinInsuranceHousingClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceHousing.v1.OpinInsuranceHousingListValidatorV1;
import net.openid.conformance.opin.validator.insuranceHousing.v1.OpinInsuranceHousingPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceHousing.v1.OpinInsuranceHousingPremiumValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class OpinInsuranceHousingValidatorsV1Test extends AbstractJsonResponseConditionUnitTest {

	private String RESOURCE_URL = "https://api.organizacao.com.br/open-insurance/insurance-housing/v1/insurance-housing";

	@Test
	@UseResurce("opinResponses/insuranceHousing/OpinInsuranceHousingPolicyInfoValidatorV1OK.json")
	public void validateStructurePolicyInfo() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceHousingPolicyInfoValidatorV1());
	}


	@Test
	@UseResurce("opinResponses/insuranceHousing/OpinInsuranceHousingPremiumValidatorV1OK.json")
	public void validateStructurePremium() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceHousingPremiumValidatorV1());
	}

	@Test
	@UseResurce("opinResponses/insuranceHousing/OpinInsuranceHousingClaimValidatorV1OK.json")
	public void validateStructureClaim() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceHousingClaimValidatorV1());
	}

	@Test
	@UseResurce("opinResponses/insuranceHousing/OpinInsuranceHousingListValidatorV1OK.json")
	public void validateStructureList() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceHousingListValidatorV1());
	}

}

