package net.openid.conformance.opin.insuranceTransport;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportListValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceTransport.v1.OpinInsuranceTransportPremiumValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class OpinInsuranceTransportValidatorsV1Test extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insuranceTransport/OpinInsuranceTransportPolicyInfoValidatorV1OK.json")
	public void validateStructurePolicyInfo() {
		run(new OpinInsuranceTransportPolicyInfoValidatorV1());
	}


	@Test
	@UseResurce("opinResponses/insuranceTransport/OpinInsuranceTransportPremiumValidatorV1OK.json")
	public void validateStructurePremium() {
		run(new OpinInsuranceTransportPremiumValidatorV1());
	}

	@Test
	@UseResurce("opinResponses/insuranceTransport/OpinInsuranceTransportClaimValidatorV1OK.json")
	public void validateStructureClaim() {
		run(new OpinInsuranceTransportClaimValidatorV1());
	}

	@Test
	@UseResurce("opinResponses/insuranceTransport/OpinInsuranceTransportListValidatorV1OK.json")
	public void validateStructureList() {
		run(new OpinInsuranceTransportListValidatorV1());
	}

}

