package net.openid.conformance.opin.channels;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.channels.v1.BranchesValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class BranchesValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/channels/GetBranchesResponse.json")
	public void validateStructure() {
		run(new BranchesValidator());
	}
}
