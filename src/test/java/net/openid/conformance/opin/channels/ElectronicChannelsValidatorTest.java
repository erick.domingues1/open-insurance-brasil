package net.openid.conformance.opin.channels;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.channels.v1.ElectronicChannelsValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@UseResurce("opinResponses/channels/ElectronicChannelsResponse.json")
public class ElectronicChannelsValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		run(new ElectronicChannelsValidator());
	}
}
