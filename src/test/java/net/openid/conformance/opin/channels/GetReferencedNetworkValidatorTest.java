package net.openid.conformance.opin.channels;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.channels.GetReferencedNetworkValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@UseResurce("opinResponses/productsServices/referencedNetworkStructure.json")
public class GetReferencedNetworkValidatorTest extends AbstractJsonResponseConditionUnitTest {
	@Test
	public void validateStructure() {
		run(new GetReferencedNetworkValidator());
	}
}
