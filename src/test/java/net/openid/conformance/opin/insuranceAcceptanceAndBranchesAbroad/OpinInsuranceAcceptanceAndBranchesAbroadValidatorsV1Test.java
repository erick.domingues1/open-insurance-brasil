package net.openid.conformance.opin.insuranceAcceptanceAndBranchesAbroad;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadListValidatorV1;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceAcceptanceAndBranchesAbroad.v1.OpinInsuranceAcceptanceAndBranchesAbroadPremiumValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class OpinInsuranceAcceptanceAndBranchesAbroadValidatorsV1Test extends AbstractJsonResponseConditionUnitTest {

	private String RESOURCE_URL = "https://api.organizacao.com.br/open-insurance/insurance-acceptance-and-branches-abroad/v1/insurance-acceptance-and-branches-abroad";

	@Test
	@UseResurce("opinResponses/insuranceAcceptanceAndBranchesAbroad/OpinInsuranceAcceptanceAndBranchesAbroadPolicyInfoValidatorV1OK.json")
	public void validateStructurePolicyInfo() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceAcceptanceAndBranchesAbroadPolicyInfoValidatorV1());
	}


	@Test
	@UseResurce("opinResponses/insuranceAcceptanceAndBranchesAbroad/OpinInsuranceAcceptanceAndBranchesAbroadPremiumValidatorV1OK.json")
	public void validateStructurePremium() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceAcceptanceAndBranchesAbroadPremiumValidatorV1());
	}

	@Test
	@UseResurce("opinResponses/insuranceAcceptanceAndBranchesAbroad/OpinInsuranceAcceptanceAndBranchesAbroadClaimValidatorV1OK.json")
	public void validateStructureClaim() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceAcceptanceAndBranchesAbroadClaimValidatorV1());
	}


	@Test
	@UseResurce("opinResponses/insuranceAcceptanceAndBranchesAbroad/OpinInsuranceAcceptanceAndBranchesAbroadListValidatorV1OK.json")
	public void validateStructureList() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceAcceptanceAndBranchesAbroadListValidatorV1());
	}
}

