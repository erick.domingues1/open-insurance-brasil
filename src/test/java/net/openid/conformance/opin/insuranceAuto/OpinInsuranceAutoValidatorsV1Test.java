package net.openid.conformance.opin.insuranceAuto;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoClaimValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoListValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insuranceAuto.v1.OpinInsuranceAutoPremiumValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class OpinInsuranceAutoValidatorsV1Test extends AbstractJsonResponseConditionUnitTest {

	private String RESOURCE_URL = "https://api.organizacao.com.br/open-insurance/insurance-auto/v1/insurance-auto";

	@Test
	@UseResurce("opinResponses/insuranceAuto/OpinInsuranceAutoPolicyInfoValidatorV1OK.json")
	public void validateStructurePolicyInfo() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceAutoPolicyInfoValidatorV1());
	}


	@Test
	@UseResurce("opinResponses/insuranceAuto/OpinInsuranceAutoPremiumValidatorV1OK.json")
	public void validateStructurePremium() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceAutoPremiumValidatorV1());
	}

	@Test
	@UseResurce("opinResponses/insuranceAuto/OpinInsuranceAutoClaimValidatorV1OK.json")
	public void validateStructureClaim() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceAutoClaimValidatorV1());
	}


	@Test
	@UseResurce("opinResponses/insuranceAuto/OpinInsuranceAutoListValidatorV1OK.json")
	public void validateStructureList() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceAutoListValidatorV1());
	}
}

