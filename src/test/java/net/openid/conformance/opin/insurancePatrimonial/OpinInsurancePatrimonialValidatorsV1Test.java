package net.openid.conformance.opin.insurancePatrimonial;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialClaimValidatorV1;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialListValidatorV1;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.insurancePatrimonial.v1.OpinInsurancePatrimonialPremiumValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class OpinInsurancePatrimonialValidatorsV1Test extends AbstractJsonResponseConditionUnitTest {

	private String RESOURCE_URL = "https://api.organizacao.com.br/open-insurance/insurance-patrimonial/v1/insurance-patrimonial";

	@Test
	@UseResurce("opinResponses/insurancePatrimonial/OpinInsurancePatrimonialPolicyInfoValidatorV1OK.json")
	public void validateStructurePolicyInfo() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsurancePatrimonialPolicyInfoValidatorV1());
	}


	@Test
	@UseResurce("opinResponses/insurancePatrimonial/OpinInsurancePatrimonialPremiumValidatorV1OK.json")
	public void validateStructurePremium() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsurancePatrimonialPremiumValidatorV1());
	}

	@Test
	@UseResurce("opinResponses/insurancePatrimonial/OpinInsurancePatrimonialClaimValidatorV1OK.json")
	public void validateStructureClaim() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsurancePatrimonialClaimValidatorV1());
	}


	@Test
	@UseResurce("opinResponses/insurancePatrimonial/OpinInsurancePatrimonialListValidatorV1OK.json")
	public void validateStructureList() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsurancePatrimonialListValidatorV1());
	}
}

