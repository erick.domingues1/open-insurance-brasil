package net.openid.conformance.opin.insurancePatrimonial;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.PolicyIDAllSelector;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PolicyIDAllSelectorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insurancePatrimonial/branches/OpinInsurancePatrimonialPoliciesOK.json")
	public void validateStructurePolicyInfo() {
		run(new PolicyIDAllSelector());
	}
}

