package net.openid.conformance.opin.insurancePatrimonial;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.openbanking_brasil.testmodules.support.PolicyIDSelector;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PolicyIDSelectorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/insurancePatrimonial/OpinInsurancePatrimonialListValidatorV1OK.json")
	public void validateStructurePolicyInfo() {
		run(new PolicyIDSelector());
	}
}

