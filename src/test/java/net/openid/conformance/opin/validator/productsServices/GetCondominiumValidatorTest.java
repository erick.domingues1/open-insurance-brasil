package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


@UseResurce("opinResponses/productsServices/condominiumStructure.json")
public class GetCondominiumValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		run(new GetCondominiumValidator());
	}
}
