package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetGeneralLiabilityValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


@UseResurce("opinResponses/productsServices/generalLiabilityStructure.json")
public class GetGeneralLiabilityValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		run(new GetGeneralLiabilityValidator());
	}
}
