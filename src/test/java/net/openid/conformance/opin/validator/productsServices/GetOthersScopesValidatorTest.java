package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetOthersScopesValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetOthersScopesValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/productsServices/othersScopesStructure.json")
	public void validateStructure() {
		run(new GetOthersScopesValidator());
	}
}
