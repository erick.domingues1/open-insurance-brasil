package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetEngineeringValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


@UseResurce("opinResponses/productsServices/engineeringStructure.json")
public class GetEngineeringValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		run(new GetEngineeringValidator());
	}
}
