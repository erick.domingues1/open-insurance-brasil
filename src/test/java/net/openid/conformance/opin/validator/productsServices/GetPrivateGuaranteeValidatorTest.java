package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetPrivateGuaranteeValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@UseResurce("opinResponses/productsServices/privateGuaranteeStructure.json")
public class GetPrivateGuaranteeValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		run(new GetPrivateGuaranteeValidator());
	}
}
