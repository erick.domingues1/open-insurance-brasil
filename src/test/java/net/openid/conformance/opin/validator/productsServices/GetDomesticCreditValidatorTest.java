package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetDomesticCreditValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@UseResurce("opinResponses/productsServices/domesticCreditStructure.json")
public class GetDomesticCreditValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		run(new GetDomesticCreditValidator());
	}
}
