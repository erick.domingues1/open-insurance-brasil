package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetStopLossValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@UseResurce("opinResponses/productsServices/stopLossStructure.json")
public class GetStopLossValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		run(new GetStopLossValidator());
	}
}
