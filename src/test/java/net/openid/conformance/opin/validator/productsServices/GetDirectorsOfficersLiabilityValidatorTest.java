package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetDirectorsOfficersLiabilityValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


@UseResurce("opinResponses/productsServices/directorsOfficersLiabilityStructure.json")
public class GetDirectorsOfficersLiabilityValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		run(new GetDirectorsOfficersLiabilityValidator());
	}
}
