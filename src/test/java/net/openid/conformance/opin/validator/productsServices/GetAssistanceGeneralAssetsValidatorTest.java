package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetAssistanceGeneralAssetsValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetAssistanceGeneralAssetsValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/productsServices/assistanceGeneralAssetsStructure.json")
	public void validateStructure() {
		GetAssistanceGeneralAssetsValidator condition = new GetAssistanceGeneralAssetsValidator();
		run(condition);
	}
}
