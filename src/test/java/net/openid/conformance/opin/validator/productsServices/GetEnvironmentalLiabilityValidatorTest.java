package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetEnvironmentalLiabilityValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetEnvironmentalLiabilityValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/productsServices/environmentalLiabilityStructure.json")
	public void validateStructure() {
		GetEnvironmentalLiabilityValidator condition = new GetEnvironmentalLiabilityValidator();
		run(condition);
	}
}
