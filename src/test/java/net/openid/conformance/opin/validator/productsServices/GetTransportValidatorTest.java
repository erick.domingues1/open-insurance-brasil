package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetTransportValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class GetTransportValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/productsServices/transportStructure.json")
	public void validateStructure() {
		run(new GetTransportValidator());
	}
}
