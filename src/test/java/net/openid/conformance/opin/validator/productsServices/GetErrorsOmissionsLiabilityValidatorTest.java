package net.openid.conformance.opin.validator.productsServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsServices.GetErrorsOmissionsLiabilityValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


@UseResurce("opinResponses/productsServices/errorsOmissionsLiabilityStructure.json")
public class GetErrorsOmissionsLiabilityValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void validateStructure() {
		run(new GetErrorsOmissionsLiabilityValidator());
	}
}
