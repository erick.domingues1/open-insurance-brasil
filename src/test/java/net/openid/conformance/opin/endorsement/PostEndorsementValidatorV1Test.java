package net.openid.conformance.opin.endorsement;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.endorsement.PostEndorsementValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class PostEndorsementValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

    @Test
    @UseResurce("opinResponses/endorsement/PostEndorsementResponse.json")
    public void validateStructure() {
        environment.putString("protected_resource_url", "https://api.seguro.com.br/open-insurance/endorsement/v1/endorsement/request/111");
        run(new PostEndorsementValidatorV1());
    }


}
