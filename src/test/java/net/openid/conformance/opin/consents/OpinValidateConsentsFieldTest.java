package net.openid.conformance.opin.consents;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.testmodule.support.OpinValidateConsentsField;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;


public class OpinValidateConsentsFieldTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("jsonEnvironmentObjects/OkConfigConsents.json")
	public void validateStructureOk()
	{
		environment.putObject("config", jsonObject);
		run(new OpinValidateConsentsField());
	}

	@Test
	@UseResurce("jsonEnvironmentObjects/WrongConfigConsents.json")
	public void validateStructureWrong()
	{
		environment.putObject("config", jsonObject);
		run(new OpinValidateConsentsField());
	}
}
