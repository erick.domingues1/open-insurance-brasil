package net.openid.conformance.opin.financialRisk;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskClaimValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskListValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskPolicyInfoValidatorV1;
import net.openid.conformance.opin.validator.financialFisk.v1.OpinInsuranceFinancialRiskPremiumValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class OpinInsuranceFinancialRiskValidatorsV1Test extends AbstractJsonResponseConditionUnitTest {

	private String RESOURCE_URL = "https://api.organizacao.com.br/open-insurance/insurance-financial-risk/v1/insurance-financial-risk";

	@Test
	@UseResurce("opinResponses/financialRisk/OpinInsuranceFinancialRiskPolicyInfoValidatorV1OK.json")
	public void validateStructurePolicyInfo() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceFinancialRiskPolicyInfoValidatorV1());
	}


	@Test
	@UseResurce("opinResponses/financialRisk/OpinInsuranceFinancialRiskPremiumValidatorV1OK.json")
	public void validateStructurePremium() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceFinancialRiskPremiumValidatorV1());
	}

	@Test
	@UseResurce("opinResponses/financialRisk/OpinInsuranceFinancialRiskClaimValidatorV1OK.json")
	public void validateStructureClaim() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceFinancialRiskClaimValidatorV1());
	}


	@Test
	@UseResurce("opinResponses/financialRisk/OpinInsuranceFinancialRiskListValidatorV1OK.json")
	public void validateStructureList() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceFinancialRiskListValidatorV1());
	}
}

