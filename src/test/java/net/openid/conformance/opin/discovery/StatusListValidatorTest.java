package net.openid.conformance.opin.discovery;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.discovery.StatusListValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@UseResurce("opinResponses/discovery/StatusListValidatorResponse.json")
public class StatusListValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void evaluate() {
		run(new StatusListValidator());
	}
}
