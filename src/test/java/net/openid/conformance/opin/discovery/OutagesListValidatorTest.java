package net.openid.conformance.opin.discovery;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.discovery.OutagesListValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

@UseResurce("opinResponses/discovery/OutagesListValidatorResponse.json")
public class OutagesListValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	public void evaluate() {
		run(new OutagesListValidator());
	}
}
