package net.openid.conformance.opin.productsNServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsNServices.GetHomeInsuranceValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class HomeInsuranceValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/homeInsurance/GetHomeInsuranceResponse.json")
	public void validateStructure() {
		GetHomeInsuranceValidator condition = new GetHomeInsuranceValidator();
		run(condition);
	}
}
