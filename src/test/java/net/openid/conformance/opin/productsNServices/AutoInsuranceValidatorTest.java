package net.openid.conformance.opin.productsNServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.opin.validator.productsNServices.GetAutoInsuranceValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

public class AutoInsuranceValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/autoInsurance/GetAutoInsurance.json")
	public void validateStructure() {
		GetAutoInsuranceValidator condition = new GetAutoInsuranceValidator();
		run(condition);
	}
}
