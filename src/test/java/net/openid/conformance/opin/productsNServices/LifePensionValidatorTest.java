package net.openid.conformance.opin.productsNServices;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.opin.validator.productsNServices.GetLifePensionValidator;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class LifePensionValidatorTest extends AbstractJsonResponseConditionUnitTest {

	@Test
	@UseResurce("opinResponses/lifePension/GetLifePensionResponse.json")
	public void validateStructure() {
		run(new GetLifePensionValidator());
	}

	@Test
	@UseResurce("opinResponses/lifePension/GetLifePensionResponse_maxLengthError.json")
	public void validateStructureWithWrongMaxLength() {
		GetLifePensionValidator condition = new GetLifePensionValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createFieldValueIsMoreThanMaxLengthMessage("name",
			condition.getApiName())));
	}

	@Test
	@UseResurce("opinResponses/lifePension/GetLifePensionResponse(MissingMandatoryField_segment)Error.json")
	public void validateStructureMissingMandatoryFieldInvestimentFundsError() {
		GetLifePensionValidator condition = new GetLifePensionValidator();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils.createElementNotFoundMessage("segment",
			condition.getApiName())));
	}

	@Test
	@UseResurce("opinResponses/lifePension/GetLifePensionResponse(MissingMandatoryField_investimentFunds)OK.json")
	public void validateStructureMissingMandatoryFieldInvestimentFundsOK() {
		run(new GetLifePensionValidator());
	}
}
