package net.openid.conformance.opin.rural;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralPremiumValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class OpinInsuranceRuralPremiumValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	private String RESOURCE_URL = "https://api.organizacao.com.br/open-insurance/insurance-rural/v1/insurance-rural";

	@Test
	@UseResurce("opinResponses/rural/premium/premiumResponseStructure.json")
	public void validateStructure() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceRuralPremiumValidatorV1());
	}

	@Test
	@UseResurce("opinResponses/rural/premium/premiumResponseStructure(FieldNotFound).json")
	public void validateFieldNotFound() {
		OpinInsuranceRuralPremiumValidatorV1 condition = new OpinInsuranceRuralPremiumValidatorV1();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils
			.createElementNotFoundMessage("paymentsQuantity", condition.getApiName())));
	}

	@Test
	@UseResurce("opinResponses/rural/premium/premiumResponseStructure(IncorrectRegexp).json")
	public void validateWrongRegexp() {
		OpinInsuranceRuralPremiumValidatorV1 condition = new OpinInsuranceRuralPremiumValidatorV1();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils
			.createFieldValueNotMatchPatternMessage("description", condition.getApiName())));
	}
}
