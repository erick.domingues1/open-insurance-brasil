package net.openid.conformance.opin.rural;

import net.openid.conformance.apis.AbstractJsonResponseConditionUnitTest;
import net.openid.conformance.condition.ConditionError;
import net.openid.conformance.condition.client.jsonAsserting.ErrorMessagesUtils;
import net.openid.conformance.opin.validator.rural.v1.OpinInsuranceRuralClaimListValidatorV1;
import net.openid.conformance.util.UseResurce;
import org.junit.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class OpinInsuranceRuralClaimListValidatorV1Test extends AbstractJsonResponseConditionUnitTest {

	private String RESOURCE_URL = "https://api.organizacao.com.br/open-insurance/insurance-rural/v1/insurance-rural";

	@Test
	@UseResurce("opinResponses/rural/claim/claimListStructure.json")
	public void validateStructure() {
		environment.putString("protected_resource_url", RESOURCE_URL);
		run(new OpinInsuranceRuralClaimListValidatorV1());
	}

	@Test
	@UseResurce("opinResponses/rural/claim/claimListStructure(FieldNotFound).json")
	public void validateFieldNotFound() {
		OpinInsuranceRuralClaimListValidatorV1 condition = new OpinInsuranceRuralClaimListValidatorV1();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils
			.createElementNotFoundMessage("identification", condition.getApiName())));
	}

	@Test
	@UseResurce("opinResponses/rural/claim/claimListStructure(WrongRegexp).json")
	public void validateWrongRegexp() {
		OpinInsuranceRuralClaimListValidatorV1 condition = new OpinInsuranceRuralClaimListValidatorV1();
		ConditionError error = runAndFail(condition);
		assertThat(error.getMessage(), containsString(ErrorMessagesUtils
			.createFieldValueNotMatchPatternMessage("description", condition.getApiName())));
	}
}
